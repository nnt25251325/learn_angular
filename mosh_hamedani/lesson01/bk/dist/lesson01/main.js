(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_products_products_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/products/products.component */ "./src/app/components/products/products.component.ts");
/* harmony import */ var _components_check_out_check_out_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/check-out/check-out.component */ "./src/app/components/check-out/check-out.component.ts");
/* harmony import */ var _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/shopping-cart/shopping-cart.component */ "./src/app/components/shopping-cart/shopping-cart.component.ts");
/* harmony import */ var _components_order_success_order_success_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/order-success/order-success.component */ "./src/app/components/order-success/order-success.component.ts");
/* harmony import */ var _components_my_orders_my_orders_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/my-orders/my-orders.component */ "./src/app/components/my-orders/my-orders.component.ts");
/* harmony import */ var _components_admin_admin_products_admin_products_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/admin/admin-products/admin-products.component */ "./src/app/components/admin/admin-products/admin-products.component.ts");
/* harmony import */ var _components_admin_admin_orders_admin_orders_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/admin/admin-orders/admin-orders.component */ "./src/app/components/admin/admin-orders/admin-orders.component.ts");
/* harmony import */ var _components_admin_product_form_product_form_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/admin/product-form/product-form.component */ "./src/app/components/admin/product-form/product-form.component.ts");
/* harmony import */ var _shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/guards/auth.guard */ "./src/app/shared/guards/auth.guard.ts");
/* harmony import */ var _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/guards/admin-auth.guard */ "./src/app/shared/guards/admin-auth.guard.ts");















var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'products', component: _components_products_products_component__WEBPACK_IMPORTED_MODULE_5__["ProductsComponent"] },
    { path: 'shopping-cart', component: _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_7__["ShoppingCartComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'check-out', component: _components_check_out_check_out_component__WEBPACK_IMPORTED_MODULE_6__["CheckOutComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"]] },
    { path: 'order-success/:id', component: _components_order_success_order_success_component__WEBPACK_IMPORTED_MODULE_8__["OrderSuccessComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"]] },
    { path: 'my/orders', component: _components_my_orders_my_orders_component__WEBPACK_IMPORTED_MODULE_9__["MyOrdersComponent"], canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"]] },
    {
        path: 'admin/products/new',
        component: _components_admin_product_form_product_form_component__WEBPACK_IMPORTED_MODULE_12__["ProductFormComponent"],
        canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"], _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AdminAuthGuard"]]
    },
    {
        path: 'admin/products/:id',
        component: _components_admin_product_form_product_form_component__WEBPACK_IMPORTED_MODULE_12__["ProductFormComponent"],
        canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"], _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AdminAuthGuard"]]
    },
    {
        path: 'admin/products',
        component: _components_admin_admin_products_admin_products_component__WEBPACK_IMPORTED_MODULE_10__["AdminProductsComponent"],
        canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"], _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AdminAuthGuard"]]
    },
    {
        path: 'admin/orders',
        component: _components_admin_admin_orders_admin_orders_component__WEBPACK_IMPORTED_MODULE_11__["AdminOrdersComponent"],
        canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_13__["AuthGuard"], _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_14__["AdminAuthGuard"]]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\r\n<div id=\"contents\">\r\n\t<div class=\"container\">\r\n\t\t<router-outlet></router-outlet>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/services/user.service */ "./src/app/shared/services/user.service.ts");





var AppComponent = /** @class */ (function () {
    function AppComponent(authService, userService, router) {
        var _this = this;
        this.authService = authService;
        this.userService = userService;
        this.router = router;
        this.authService.user$.subscribe(function (user) {
            if (user) {
                _this.userService.saveUser(user);
                var returnUrl = localStorage.getItem('returnUrl');
                if (returnUrl) {
                    localStorage.removeItem('returnUrl');
                    _this.router.navigateByUrl(returnUrl);
                }
            }
        });
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular7_data_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular7-data-table */ "./node_modules/angular7-data-table/esm5/angular7-data-table.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_products_products_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/products/products.component */ "./src/app/components/products/products.component.ts");
/* harmony import */ var _components_products_product_filter_product_filter_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/products/product-filter/product-filter.component */ "./src/app/components/products/product-filter/product-filter.component.ts");
/* harmony import */ var _components_product_card_product_card_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/product-card/product-card.component */ "./src/app/components/product-card/product-card.component.ts");
/* harmony import */ var _components_product_quantity_product_quantity_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/product-quantity/product-quantity.component */ "./src/app/components/product-quantity/product-quantity.component.ts");
/* harmony import */ var _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/shopping-cart/shopping-cart.component */ "./src/app/components/shopping-cart/shopping-cart.component.ts");
/* harmony import */ var _components_check_out_check_out_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/check-out/check-out.component */ "./src/app/components/check-out/check-out.component.ts");
/* harmony import */ var _components_shipping_form_shipping_form_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/shipping-form/shipping-form.component */ "./src/app/components/shipping-form/shipping-form.component.ts");
/* harmony import */ var _components_shopping_cart_summary_shopping_cart_summary_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/shopping-cart-summary/shopping-cart-summary.component */ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.ts");
/* harmony import */ var _components_order_success_order_success_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/order-success/order-success.component */ "./src/app/components/order-success/order-success.component.ts");
/* harmony import */ var _components_my_orders_my_orders_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/my-orders/my-orders.component */ "./src/app/components/my-orders/my-orders.component.ts");
/* harmony import */ var _components_admin_admin_products_admin_products_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/admin/admin-products/admin-products.component */ "./src/app/components/admin/admin-products/admin-products.component.ts");
/* harmony import */ var _components_admin_admin_orders_admin_orders_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/admin/admin-orders/admin-orders.component */ "./src/app/components/admin/admin-orders/admin-orders.component.ts");
/* harmony import */ var _components_admin_product_form_product_form_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/admin/product-form/product-form.component */ "./src/app/components/admin/product-form/product-form.component.ts");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./shared/services/user.service */ "./src/app/shared/services/user.service.ts");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./shared/services/category.service */ "./src/app/shared/services/category.service.ts");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./shared/services/product.service */ "./src/app/shared/services/product.service.ts");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");
/* harmony import */ var _shared_services_order_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./shared/guards/auth.guard */ "./src/app/shared/guards/auth.guard.ts");
/* harmony import */ var _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./shared/guards/admin-auth.guard */ "./src/app/shared/guards/admin-auth.guard.ts");



































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
                _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__["NavbarComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
                _components_products_products_component__WEBPACK_IMPORTED_MODULE_14__["ProductsComponent"],
                _components_products_product_filter_product_filter_component__WEBPACK_IMPORTED_MODULE_15__["ProductFilterComponent"],
                _components_product_card_product_card_component__WEBPACK_IMPORTED_MODULE_16__["ProductCardComponent"],
                _components_product_quantity_product_quantity_component__WEBPACK_IMPORTED_MODULE_17__["ProductQuantityComponent"],
                _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_18__["ShoppingCartComponent"],
                _components_check_out_check_out_component__WEBPACK_IMPORTED_MODULE_19__["CheckOutComponent"],
                _components_shipping_form_shipping_form_component__WEBPACK_IMPORTED_MODULE_20__["ShippingFormComponent"],
                _components_shopping_cart_summary_shopping_cart_summary_component__WEBPACK_IMPORTED_MODULE_21__["ShoppingCartSummaryComponent"],
                _components_order_success_order_success_component__WEBPACK_IMPORTED_MODULE_22__["OrderSuccessComponent"],
                _components_my_orders_my_orders_component__WEBPACK_IMPORTED_MODULE_23__["MyOrdersComponent"],
                _components_admin_admin_products_admin_products_component__WEBPACK_IMPORTED_MODULE_24__["AdminProductsComponent"],
                _components_admin_admin_orders_admin_orders_component__WEBPACK_IMPORTED_MODULE_25__["AdminOrdersComponent"],
                _components_admin_product_form_product_form_component__WEBPACK_IMPORTED_MODULE_26__["ProductFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                angular7_data_table__WEBPACK_IMPORTED_MODULE_4__["DataTableModule"].forRoot(),
                _angular_fire__WEBPACK_IMPORTED_MODULE_5__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].firebase),
                _angular_fire_database__WEBPACK_IMPORTED_MODULE_6__["AngularFireDatabaseModule"],
                _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__["AngularFireAuthModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"]
            ],
            providers: [
                _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_27__["AuthService"],
                _shared_services_user_service__WEBPACK_IMPORTED_MODULE_28__["UserService"],
                _shared_services_category_service__WEBPACK_IMPORTED_MODULE_29__["CategoryService"],
                _shared_services_product_service__WEBPACK_IMPORTED_MODULE_30__["ProductService"],
                _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_31__["ShoppingCartService"],
                _shared_services_order_service__WEBPACK_IMPORTED_MODULE_32__["OrderService"],
                _shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_33__["AuthGuard"],
                _shared_guards_admin_auth_guard__WEBPACK_IMPORTED_MODULE_34__["AdminAuthGuard"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/admin/admin-orders/admin-orders.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/admin-orders/admin-orders.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 class=\"mb-3\">Orders</h3>\r\n\r\n<table class=\"table\">\r\n\t<thead>\r\n\t\t<tr>\r\n\t\t\t<th>Customer</th>\r\n\t\t\t<th>Date</th>\r\n\t\t\t<th>Actions</th>\r\n\t\t</tr>\r\n\t</thead>\r\n\t<tbody>\r\n\t\t<tr *ngFor=\"let order of orders$ | async\">\r\n\t\t\t<td>{{ order.shipping.name }}</td>\r\n\t\t\t<td>{{ order.datePlaced | date:'medium'}}</td>\r\n\t\t\t<td><a href=\"#\">View</a></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/components/admin/admin-orders/admin-orders.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/admin-orders/admin-orders.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vYWRtaW4tb3JkZXJzL2FkbWluLW9yZGVycy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/admin/admin-orders/admin-orders.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/admin/admin-orders/admin-orders.component.ts ***!
  \*************************************************************************/
/*! exports provided: AdminOrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminOrdersComponent", function() { return AdminOrdersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../shared/services/order.service */ "./src/app/shared/services/order.service.ts");




var AdminOrdersComponent = /** @class */ (function () {
    function AdminOrdersComponent(orderService) {
        this.orderService = orderService;
        this.orders$ = this.orderService.getOrders().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
            return changes.map(function (c) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val())); });
        }));
        this.orders$.subscribe(function (x) { return console.log(x); }); //test
    }
    AdminOrdersComponent.prototype.ngOnInit = function () {
    };
    AdminOrdersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-orders',
            template: __webpack_require__(/*! ./admin-orders.component.html */ "./src/app/components/admin/admin-orders/admin-orders.component.html"),
            styles: [__webpack_require__(/*! ./admin-orders.component.scss */ "./src/app/components/admin/admin-orders/admin-orders.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"]])
    ], AdminOrdersComponent);
    return AdminOrdersComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/admin-products/admin-products.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/admin-products/admin-products.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"mb-20\"><button type=\"button\" class=\"btn btn-primary\" [routerLink]=\"['/admin/products/new']\">New Product</button></p>\r\n\r\n<div class=\"row mb-20\">\r\n\t<div class=\"col-md-6\">\r\n\t\t<div class=\"search-wrp\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\tplaceholder=\"Search...\"\r\n\t\t\t\t[ngClass]=\"search.value ? 'has-value' : ''\"\r\n\t\t\t\t#search\r\n\t\t\t\t(keyup)=\"filterTitle(search.value)\"\r\n\t\t\t>\r\n\t\t\t<button class=\"btn btn-default btn-clear\" type=\"button\" (click)=\"filterTitle(''); search.value = '';\"><i class=\"fa fa-close\"></i></button>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<data-table\r\n\t[items]=\"items\"\r\n\t[itemCount]=\"itemCount\"\r\n\t[pageLimits]=\"pageLimits\"\r\n\t(reload)=\"reloadItems($event)\"\r\n\tclass=\"tbl-products\">\r\n\t<data-table-column\r\n\t\t[property]=\"'imageUrl'\"\r\n\t\t[header]=\"'Image'\"\r\n\t\t[resizable]=\"true\">\r\n\t\t<ng-template #dataTableCell let-item=\"item\">\r\n\t\t\t<img [src]=\"item.imageUrl\" [alt]=\"item.title\" width=\"70\">\r\n\t\t</ng-template>\r\n\t</data-table-column>\r\n\t<data-table-column\r\n\t\t[property]=\"'title'\"\r\n\t\t[header]=\"'Title'\"\r\n\t\t[sortable]=\"true\"\r\n\t\t[resizable]=\"true\">\r\n\t</data-table-column>\r\n\t<data-table-column\r\n\t\t[property]=\"'category'\"\r\n\t\t[header]=\"'Category'\"\r\n\t\t[sortable]=\"true\"\r\n\t\t[resizable]=\"true\">\r\n\t</data-table-column>\r\n\t<data-table-column\r\n\t\t[property]=\"'price'\"\r\n\t\t[header]=\"'Price'\"\r\n\t\t[sortable]=\"true\"\r\n\t\t[resizable]=\"true\">\r\n\t\t<ng-template #dataTableCell let-item=\"item\">\r\n\t\t\t{{ item.price | currency:'USD' }}\r\n\t\t</ng-template>\r\n\t</data-table-column>\r\n\t<data-table-column\r\n\t\t[property]=\"'key'\"\r\n\t\t[header]=\"'Actions'\"\r\n\t\t[resizable]=\"true\">\r\n\t\t<ng-template #dataTableCell let-item=\"item\">\r\n\t\t\t<button type=\"button\" class=\"btn btn-sm btn-success\" [routerLink]=\"['/admin/products', item.key]\">Edit</button>\r\n\t\t\t&nbsp;\r\n\t\t\t<button type=\"button\" class=\"btn btn-sm btn-danger\" (click)=\"deleteProduct(item.key)\">Delete</button>\r\n\t\t</ng-template>\r\n\t</data-table-column>\r\n</data-table>\r\n"

/***/ }),

/***/ "./src/app/components/admin/admin-products/admin-products.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/admin-products/admin-products.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search-wrp {\n  position: relative; }\n  .search-wrp .form-control {\n    padding-right: 30px; }\n  .search-wrp .form-control.has-value + .btn-clear {\n      display: inline-block; }\n  .search-wrp .btn-clear {\n    display: none;\n    vertical-align: middle;\n    position: absolute;\n    right: 3px;\n    top: 50%;\n    -webkit-transform: translate(0, -50%);\n            transform: translate(0, -50%);\n    background: transparent;\n    border: 0;\n    outline: 0;\n    text-align: center;\n    padding: 1px 5px;\n    margin: 0;\n    z-index: 99; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9hZG1pbi1wcm9kdWN0cy9EOlxceGFtcHA1NVxcaHRkb2NzXFxsZWFybl9hbmd1bGFyXFxtb3NoX2hhbWVkYW5pXFxsZXNzb24wMS9zcmNcXGFwcFxcY29tcG9uZW50c1xcYWRtaW5cXGFkbWluLXByb2R1Y3RzXFxhZG1pbi1wcm9kdWN0cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtCQUFrQixFQUFBO0VBRG5CO0lBR0UsbUJBQW1CLEVBQUE7RUFIckI7TUFLRyxxQkFBcUIsRUFBQTtFQUx4QjtJQVNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixRQUFRO0lBQ1IscUNBQTZCO1lBQTdCLDZCQUE2QjtJQUM3Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLFNBQVM7SUFDVCxXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2FkbWluLXByb2R1Y3RzL2FkbWluLXByb2R1Y3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlYXJjaC13cnAge1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHQuZm9ybS1jb250cm9sIHtcclxuXHRcdHBhZGRpbmctcmlnaHQ6IDMwcHg7XHJcblx0XHQmLmhhcy12YWx1ZSArIC5idG4tY2xlYXIge1xyXG5cdFx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5idG4tY2xlYXIge1xyXG5cdFx0ZGlzcGxheTogbm9uZTtcclxuXHRcdHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRyaWdodDogM3B4O1xyXG5cdFx0dG9wOiA1MCU7XHJcblx0XHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcclxuXHRcdGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG5cdFx0Ym9yZGVyOiAwO1xyXG5cdFx0b3V0bGluZTogMDtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRcdHBhZGRpbmc6IDFweCA1cHg7XHJcblx0XHRtYXJnaW46IDA7XHJcblx0XHR6LWluZGV4OiA5OTtcclxuXHR9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/admin/admin-products/admin-products.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/admin/admin-products/admin-products.component.ts ***!
  \*****************************************************************************/
/*! exports provided: AdminProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminProductsComponent", function() { return AdminProductsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var angular7_data_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular7-data-table */ "./node_modules/angular7-data-table/esm5/angular7-data-table.js");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared/services/product.service */ "./src/app/shared/services/product.service.ts");





var AdminProductsComponent = /** @class */ (function () {
    function AdminProductsComponent(productService) {
        var _this = this;
        this.productService = productService;
        this.items = [];
        this.itemCount = 0;
        this.pageLimits = [10, 20, 40, 80, 100];
        this.productService.getAllProducts().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
            return changes.map(function (c) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val())); });
        })).subscribe(function (products) {
            _this.products = products;
            _this.initializeTable(products);
        });
    }
    AdminProductsComponent.prototype.initializeTable = function (products) {
        var _this = this;
        this.tableResource = new angular7_data_table__WEBPACK_IMPORTED_MODULE_3__["DataTableResource"](products);
        this.reloadItems({ offset: 0, limit: 10 });
        this.tableResource.count().then(function (count) { return _this.itemCount = count; });
    };
    AdminProductsComponent.prototype.ngOnInit = function () {
    };
    AdminProductsComponent.prototype.reloadItems = function (params) {
        var _this = this;
        if (this.tableResource) {
            this.tableResource.query(params).then(function (items) { return _this.items = items; });
        }
    };
    AdminProductsComponent.prototype.deleteProduct = function (id) {
        if (confirm('Are you sure want to delete this product?')) {
            this.productService.deleteProduct(id)
                .catch(function (err) {
                console.log(err);
            });
        }
    };
    AdminProductsComponent.prototype.filterTitle = function (keyword) {
        var filteredProducts = this.products.filter(function (p) {
            return p.title.toLowerCase().indexOf(keyword.trim().toLowerCase()) != -1;
        });
        this.initializeTable(filteredProducts);
    };
    AdminProductsComponent.prototype.ngOnDestroy = function () {
        if (this.subscription)
            this.subscription.unsubscribe();
    };
    AdminProductsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-products',
            template: __webpack_require__(/*! ./admin-products.component.html */ "./src/app/components/admin/admin-products/admin-products.component.html"),
            styles: [__webpack_require__(/*! ./admin-products.component.scss */ "./src/app/components/admin/admin-products/admin-products.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"]])
    ], AdminProductsComponent);
    return AdminProductsComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/product-form/product-form.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/product-form/product-form.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mb-3\">\r\n\t<div class=\"col-sm-6\">\r\n\t\t<form class=\"form\" [formGroup]=\"formProduct\" (ngSubmit)=\"onSaveProduct()\">\r\n\t\t\t<h3 class=\"text-center\">New Product</h3>\r\n\t\t\t<div class=\"form-group\">\r\n\t\t\t\t<label>Title</label>\r\n\t\t\t\t<input\r\n\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\tname=\"title\"\r\n\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\tformControlName=\"title\"\r\n\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.title.errors }\"\r\n\t\t\t\t>\r\n\t\t\t\t<span *ngIf=\"isSubmitted && f.title.errors?.required\" class=\"badge badge-danger\">Title is require.</span>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"form-group\">\r\n\t\t\t\t<label>Price</label>\r\n\t\t\t\t<div class=\"input-group\">\r\n\t\t\t\t\t<div class=\"input-group-prepend\"><span class=\"input-group-text\">$</span></div>\r\n\t\t\t\t\t<input\r\n\t\t\t\t\t\ttype=\"number\"\r\n\t\t\t\t\t\tname=\"price\"\r\n\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\tformControlName=\"price\"\r\n\t\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.price.errors }\"\r\n\t\t\t\t\t>\r\n\t\t\t\t</div>\r\n\t\t\t\t<span *ngIf=\"isSubmitted && f.price.errors?.required\" class=\"badge badge-danger\">Price is require.</span>\r\n\t\t\t\t<span *ngIf=\"isSubmitted && f.price.errors?.min\" class=\"badge badge-danger\">Price must be 0 or higher.</span>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"form-group\">\r\n\t\t\t\t<label>Category</label>\r\n\t\t\t\t<select\r\n\t\t\t\t\tname=\"category\"\r\n\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\tformControlName=\"category\"\r\n\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.category.errors }\"\r\n\t\t\t\t>\r\n\t\t\t\t\t<option></option>\r\n\t\t\t\t\t<option *ngFor=\"let c of categories$ | async\" [value]=\"c.payload.key\">{{ c.payload.val().name }}</option>\r\n\t\t\t\t</select>\r\n\t\t\t\t<span *ngIf=\"isSubmitted && f.category.errors?.required\" class=\"badge badge-danger\">Category is require.</span>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"form-group\">\r\n\t\t\t\t<label>Image Url</label>\r\n\t\t\t\t<input\r\n\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\tname=\"imageUrl\"\r\n\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\tformControlName=\"imageUrl\"\r\n\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.imageUrl.errors }\"\r\n\t\t\t\t>\r\n\t\t\t\t<span *ngIf=\"isSubmitted && f.imageUrl.errors?.required\" class=\"badge badge-danger\">Image Url is require.</span>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"form-group text-center\">\r\n\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">\r\n\t\t\t\t\tSave\r\n\t\t\t\t\t<span *ngIf=\"isLoading\">&nbsp;<img src=\"data:image/gif;base64,R0lGODlhFAAUAPAAAP////7+/iH5BAkKAAAAIf4aQ3JlYXRlZCB3aXRoIGFqYXhsb2FkLmluZm8AIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAFAAUAAACJoSPqcudAZ2CUSIaJGX1dc9Z4kgeH5eZqXOW7ttsS2uss0hrNpwUACH5BAkKAAAALAAAAAAUABQAAAImhI+py50RHINRvgrgbFR1/1lHKJamJqGGymHmC5ckwtKO28Z1nBQAIfkECQoAAAAsAAAAABQAFAAAAiaEj6nL7Y9CgErSySSOdmnKgeLzGV2jbWdWiusIs1vyHnN13XnsFAAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAStLJbNWb+g8aHDBmXLlIWMiy6rZGb9XMDgrhrVEAACH5BAkKAAAALAAAAAAUABQAAAInhI+py+2PQoBK0sls1Zv6D2aYwwGlGKaqcbKtO0bRKMVLbT94WC8FACH5BAkKAAAALAAAAAAUABQAAAIlhI+py+2PQoBK0sls1Zv6D2aYwwGlGKbkCUljwpqx8TZueINzAQAh+QQJCgAAACwAAAAAFAAUAAACJ4SPqcvtj0KAStLJbNWb+n9I2CeO2Vg+HLCCriG68dLCdXiZ3v0eBQAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAYB4JpVWYd79o1NhspPh8qPOR7juuSRuxFH2bsFIAADs=\" /></span>\r\n\t\t\t\t</button>\r\n\t\t\t\t&nbsp;\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" (click)=\"reset()\">Reset</button>\r\n\t\t\t</div>\r\n\t\t</form>\r\n\t</div>\r\n\r\n\t<div class=\"col-sm-6\" *ngIf=\"product.title\">\r\n\t\t<h3 class=\"text-center\">Preview</h3>\r\n\t\t<app-product-card [product]=\"product\" [show-actions]=\"false\"></app-product-card>\r\n\t</div>\r\n</div>\r\n\r\n<p class=\"text-right\">\r\n\t<button type=\"button\" class=\"btn btn-danger\" *ngIf=\"id\" (click)=\"deleteProduct()\">Delete</button>\r\n</p>\r\n\r\n<hr>\r\n<p class=\"text-center\"><button type=\"button\" class=\"btn btn-secondary\" [routerLink]=\"['/admin/products']\">Back To Product List</button></p>\r\n"

/***/ }),

/***/ "./src/app/components/admin/product-form/product-form.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/product-form/product-form.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vcHJvZHVjdC1mb3JtL3Byb2R1Y3QtZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/admin/product-form/product-form.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/admin/product-form/product-form.component.ts ***!
  \*************************************************************************/
/*! exports provided: ProductFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductFormComponent", function() { return ProductFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../shared/services/category.service */ "./src/app/shared/services/category.service.ts");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../../shared/services/product.service */ "./src/app/shared/services/product.service.ts");







var ProductFormComponent = /** @class */ (function () {
    function ProductFormComponent(formBuilder, route, categoryService, productService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.route = route;
        this.categoryService = categoryService;
        this.productService = productService;
        this.product = {
            title: '',
            price: 0,
            category: '',
            imageUrl: ''
        };
        this.isSubmitted = false;
        this.isLoading = false;
        this.categories$ = this.categoryService.getAllCategories().snapshotChanges();
        this.id = this.route.snapshot.paramMap.get('id');
        if (this.id) {
            this.productService.getProduct(this.id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1)).subscribe(function (product) {
                _this.formProduct.setValue(product);
            });
        }
    }
    ProductFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formProduct = this.formBuilder.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            price: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(0)]],
            category: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            imageUrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.subscription = this.formProduct.valueChanges.subscribe(function (product) {
            _this.product = product;
        });
    };
    Object.defineProperty(ProductFormComponent.prototype, "f", {
        get: function () {
            return this.formProduct.controls;
        },
        enumerable: true,
        configurable: true
    });
    ProductFormComponent.prototype.onSaveProduct = function () {
        var _this = this;
        this.isSubmitted = true;
        if (this.formProduct.invalid)
            return false;
        this.isLoading = true;
        if (this.id) {
            this.productService.updateProduct(this.id, this.product)
                .catch(function (err) {
                console.log(err);
                _this.isLoading = false;
            });
        }
        else {
            this.productService.createProduct(this.product)
                .catch(function (err) {
                console.log(err);
                _this.isLoading = false;
            });
        }
    };
    ProductFormComponent.prototype.reset = function () {
        this.formProduct.reset();
        this.isSubmitted = false;
        this.isLoading = false;
    };
    ProductFormComponent.prototype.deleteProduct = function () {
        if (confirm('Are you sure want to delete this product?')) {
            this.productService.deleteProduct(this.id)
                .catch(function (err) {
                console.log(err);
            });
        }
    };
    ProductFormComponent.prototype.ngOnDestroy = function () {
        if (this.subscription)
            this.subscription.unsubscribe();
    };
    ProductFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-form',
            template: __webpack_require__(/*! ./product-form.component.html */ "./src/app/components/admin/product-form/product-form.component.html"),
            styles: [__webpack_require__(/*! ./product-form.component.scss */ "./src/app/components/admin/product-form/product-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _shared_services_category_service__WEBPACK_IMPORTED_MODULE_5__["CategoryService"],
            _shared_services_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"]])
    ], ProductFormComponent);
    return ProductFormComponent;
}());



/***/ }),

/***/ "./src/app/components/check-out/check-out.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/check-out/check-out.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 class=\"mb-3\">Shipping</h3>\r\n\r\n<div class=\"row\" *ngIf=\"cart$ | async as cart\">\r\n\t<div class=\"col-lg-6\">\r\n\t\t<app-shipping-form [cart]=\"cart\"></app-shipping-form>\r\n\t</div>\r\n\t<div class=\"col-lg-6\">\r\n\t\t<app-shopping-cart-summary [cart]=\"cart\"></app-shopping-cart-summary>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/check-out/check-out.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/check-out/check-out.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2hlY2stb3V0L2NoZWNrLW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/check-out/check-out.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/check-out/check-out.component.ts ***!
  \*************************************************************/
/*! exports provided: CheckOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckOutComponent", function() { return CheckOutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");



var CheckOutComponent = /** @class */ (function () {
    function CheckOutComponent(shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }
    CheckOutComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.shoppingCartService.getCart()];
                    case 1:
                        _a.cart$ = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CheckOutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-check-out',
            template: __webpack_require__(/*! ./check-out.component.html */ "./src/app/components/check-out/check-out.component.html"),
            styles: [__webpack_require__(/*! ./check-out.component.scss */ "./src/app/components/check-out/check-out.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], CheckOutComponent);
    return CheckOutComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-products></app-products>\r\n"

/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"text-center\">\r\n\t<button\r\n\t\ttype=\"button\"\r\n\t\tclass=\"btn btn-primary\"\r\n\t\t(click)=\"login()\"\r\n\t>\r\n\t\tLogin With Google\r\n\t</button>\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");



var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService) {
        this.authService = authService;
    }
    LoginComponent.prototype.login = function () {
        this.authService.loginWithGoogle();
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/my-orders/my-orders.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/my-orders/my-orders.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 class=\"mb-3\">Your Orders</h3>\r\n\r\n<table class=\"table\">\r\n\t<thead>\r\n\t\t<tr>\r\n\t\t\t<th>Customer</th>\r\n\t\t\t<th>Date</th>\r\n\t\t\t<th>Actions</th>\r\n\t\t</tr>\r\n\t</thead>\r\n\t<tbody>\r\n\t\t<tr *ngFor=\"let order of orders$ | async\">\r\n\t\t\t<td>{{ order.shipping.name }}</td>\r\n\t\t\t<td>{{ order.datePlaced | date:'medium'}}</td>\r\n\t\t\t<td><a href=\"#\">View</a></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/components/my-orders/my-orders.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/my-orders/my-orders.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbXktb3JkZXJzL215LW9yZGVycy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/my-orders/my-orders.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/my-orders/my-orders.component.ts ***!
  \*************************************************************/
/*! exports provided: MyOrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyOrdersComponent", function() { return MyOrdersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../shared/services/order.service */ "./src/app/shared/services/order.service.ts");





var MyOrdersComponent = /** @class */ (function () {
    function MyOrdersComponent(authService, orderService) {
        var _this = this;
        this.authService = authService;
        this.orderService = orderService;
        this.orders$ = this.authService.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (user) {
            return _this.orderService.getOrdersByUser(user.uid).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
                return changes.map(function (c) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val())); });
            }));
        }));
        this.orders$.subscribe(function (x) { return console.log(x); }); //test
    }
    MyOrdersComponent.prototype.ngOnInit = function () {
    };
    MyOrdersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-orders',
            template: __webpack_require__(/*! ./my-orders.component.html */ "./src/app/components/my-orders/my-orders.component.html"),
            styles: [__webpack_require__(/*! ./my-orders.component.scss */ "./src/app/components/my-orders/my-orders.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"]])
    ], MyOrdersComponent);
    return MyOrdersComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md navbar-light bg-light fixed-top\">\r\n\t<div class=\"container\">\r\n\t\t<a class=\"navbar-brand h1\" href=\"./\">SHOP</a>\r\n\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n\t\t\t<span class=\"navbar-toggler-icon\"></span>\r\n\t\t</button>\r\n\t\t<div class=\"collapse navbar-collapse\" id=\"navbarNavDropdown\">\r\n\t\t\t<ul class=\"navbar-nav mr-auto\">\r\n\t\t\t\t<li class=\"nav-item\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\r\n\t\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/']\">Home</a>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li class=\"nav-item\" routerLinkActive=\"active\">\r\n\t\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/shopping-cart']\">\r\n\t\t\t\t\t\tShopping Cart\r\n\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\tclass=\"badge badge-pill badge-danger\"\r\n\t\t\t\t\t\t\t*ngIf=\"cart$ | async as cart\">\r\n\t\t\t\t\t\t\t{{ cart.totalItemsCount }}\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t\t<ul class=\"navbar-nav\" *ngIf=\"!appUser\">\r\n\t\t\t\t<li class=\"nav-item\" routerLinkActive=\"active\">\r\n\t\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/login']\">Login</a>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t\t<div class=\"dropdown\" *ngIf=\"appUser\">\r\n\t\t\t\t<a class=\"nav-link dropdown-toggle\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n\t\t\t\t\t{{ appUser?.name }}\r\n\t\t\t\t</a>\r\n\t\t\t\t<div class=\"dropdown-menu\">\r\n\t\t\t\t\t<a class=\"dropdown-item\" [routerLink]=\"['/products']\"></a>\r\n\t\t\t\t\t<a class=\"dropdown-item\" [routerLink]=\"['/my/orders']\">My Orders</a>\r\n\t\t\t\t\t<ng-container *ngIf=\"appUser?.isAdmin\">\r\n\t\t\t\t\t\t<a class=\"dropdown-item\" [routerLink]=\"['/admin/orders']\">Manage Orders</a>\r\n\t\t\t\t\t\t<a class=\"dropdown-item\" [routerLink]=\"['/admin/products']\">Manage Products</a>\r\n\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t<a class=\"dropdown-item\" (click)=\"logout()\">Logout</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nav-item .nav-link {\n  font-weight: 500; }\n\n.nav-item.active .nav-link,\n.nav-item .nav-link:hover {\n  color: #007bff; }\n\n.dropdown .nav-link {\n  font-weight: 500;\n  color: #007bff; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcbW9zaF9oYW1lZGFuaVxcbGVzc29uMDEvc3JjXFxhcHBcXGNvbXBvbmVudHNcXG5hdmJhclxcbmF2YmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsZ0JBQWdCLEVBQUE7O0FBRmxCOztFQU1FLGNBQWMsRUFBQTs7QUFHaEI7RUFDQyxnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5hdi1pdGVtIHtcclxuXHQubmF2LWxpbmsge1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHR9XHJcblx0Ji5hY3RpdmUgLm5hdi1saW5rLFxyXG5cdC5uYXYtbGluazpob3ZlciB7XHJcblx0XHRjb2xvcjogIzAwN2JmZjtcclxuXHR9XHJcbn1cclxuLmRyb3Bkb3duIC5uYXYtbGluayB7XHJcblx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHRjb2xvcjogIzAwN2JmZjtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, shoppingCartService) {
        this.authService = authService;
        this.shoppingCartService = shoppingCartService;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.authService.appUser$.subscribe(function (user) {
                            if (user) {
                                console.log(user);
                                _this.appUser = user;
                            }
                        });
                        _a = this;
                        return [4 /*yield*/, this.shoppingCartService.getCart()];
                    case 1:
                        _a.cart$ = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NavbarComponent.prototype.logout = function () {
        this.authService.logout();
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/components/navbar/navbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_3__["ShoppingCartService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/order-success/order-success.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/order-success/order-success.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"alert alert-success\" role=\"alert\">\r\n\t<strong>Order Success!</strong>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/order-success/order-success.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/order-success/order-success.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvb3JkZXItc3VjY2Vzcy9vcmRlci1zdWNjZXNzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/order-success/order-success.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/order-success/order-success.component.ts ***!
  \*********************************************************************/
/*! exports provided: OrderSuccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderSuccessComponent", function() { return OrderSuccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var OrderSuccessComponent = /** @class */ (function () {
    function OrderSuccessComponent() {
    }
    OrderSuccessComponent.prototype.ngOnInit = function () {
    };
    OrderSuccessComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-order-success',
            template: __webpack_require__(/*! ./order-success.component.html */ "./src/app/components/order-success/order-success.component.html"),
            styles: [__webpack_require__(/*! ./order-success.component.scss */ "./src/app/components/order-success/order-success.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], OrderSuccessComponent);
    return OrderSuccessComponent;
}());



/***/ }),

/***/ "./src/app/components/product-card/product-card.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/product-card/product-card.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n\t<img class=\"card-img-top\" *ngIf=\"product.imageUrl\" [src]=\"product.imageUrl\" [alt]=\"product.title\">\n\t<div class=\"card-body\">\n\t\t<h4 class=\"card-title\">{{ product.title }}</h4>\n\t\t<p class=\"card-category\"><strong>{{ product.category }}</strong></p>\n\t\t<p class=\"card-price mb-0\"><strong>{{ product.price | currency:'USD' }}</strong></p>\n\t</div>\n\t<div class=\"card-footer p-0\" *ngIf=\"showActions && shoppingCart\">\n\t\t<button\n\t\t\ttype=\"button\"\n\t\t\tclass=\"btn btn-block btn-primary\"\n\t\t\t*ngIf=\"shoppingCart.getQuantity(product) === 0; else updateQuantity\"\n\t\t\t(click)=\"addToCart()\">\n\t\t\tAdd To Card\n\t\t</button>\n\t\t<ng-template #updateQuantity>\n\t\t\t<app-product-quantity [product]=\"product\" [shopping-cart]=\"shoppingCart\"></app-product-quantity>\n\t\t</ng-template>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/product-card/product-card.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/product-card/product-card.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZHVjdC1jYXJkL3Byb2R1Y3QtY2FyZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/product-card/product-card.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/product-card/product-card.component.ts ***!
  \*******************************************************************/
/*! exports provided: ProductCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCardComponent", function() { return ProductCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");
/* harmony import */ var _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../shared/models/shopping-cart.models */ "./src/app/shared/models/shopping-cart.models.ts");




var ProductCardComponent = /** @class */ (function () {
    function ProductCardComponent(shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
        this.showActions = true;
    }
    ProductCardComponent.prototype.addToCart = function () {
        this.shoppingCartService.addToCart(this.product);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProductCardComponent.prototype, "product", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('shopping-cart'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_3__["ShoppingCart"])
    ], ProductCardComponent.prototype, "shoppingCart", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('show-actions'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], ProductCardComponent.prototype, "showActions", void 0);
    ProductCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-card',
            template: __webpack_require__(/*! ./product-card.component.html */ "./src/app/components/product-card/product-card.component.html"),
            styles: [__webpack_require__(/*! ./product-card.component.scss */ "./src/app/components/product-card/product-card.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], ProductCardComponent);
    return ProductCardComponent;
}());



/***/ }),

/***/ "./src/app/components/product-quantity/product-quantity.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/product-quantity/product-quantity.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row no-gutters product-quantity\">\r\n\t<div class=\"col-2\">\r\n\t\t<button\r\n\t\t\ttype=\"button\"\r\n\t\t\tclass=\"btn btn-block btn-secondary\"\r\n\t\t\t(click)=\"removeFromCart()\">\r\n\t\t\t-\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"col text-center\" style=\"padding: 6px;\">\r\n\t\t{{ shoppingCart.getQuantity(product) }} in cart\r\n\t</div>\r\n\t<div class=\"col-2\">\r\n\t\t<button\r\n\t\t\ttype=\"button\"\r\n\t\t\tclass=\"btn btn-block btn-secondary\"\r\n\t\t\t(click)=\"addToCart()\">\r\n\t\t\t+\r\n\t\t</button>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/product-quantity/product-quantity.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/product-quantity/product-quantity.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZHVjdC1xdWFudGl0eS9wcm9kdWN0LXF1YW50aXR5LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/product-quantity/product-quantity.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/product-quantity/product-quantity.component.ts ***!
  \***************************************************************************/
/*! exports provided: ProductQuantityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductQuantityComponent", function() { return ProductQuantityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");
/* harmony import */ var _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../shared/models/shopping-cart.models */ "./src/app/shared/models/shopping-cart.models.ts");




var ProductQuantityComponent = /** @class */ (function () {
    function ProductQuantityComponent(shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }
    ProductQuantityComponent.prototype.ngOnInit = function () {
    };
    ProductQuantityComponent.prototype.addToCart = function () {
        this.shoppingCartService.addToCart(this.product);
    };
    ProductQuantityComponent.prototype.removeFromCart = function () {
        this.shoppingCartService.removeFromCart(this.product);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProductQuantityComponent.prototype, "product", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('shopping-cart'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_3__["ShoppingCart"])
    ], ProductQuantityComponent.prototype, "shoppingCart", void 0);
    ProductQuantityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-quantity',
            template: __webpack_require__(/*! ./product-quantity.component.html */ "./src/app/components/product-quantity/product-quantity.component.html"),
            styles: [__webpack_require__(/*! ./product-quantity.component.scss */ "./src/app/components/product-quantity/product-quantity.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], ProductQuantityComponent);
    return ProductQuantityComponent;
}());



/***/ }),

/***/ "./src/app/components/products/product-filter/product-filter.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/components/products/product-filter/product-filter.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar-category sticky-top\">\n\t<h3 class=\"st-sidebar\" *ngIf=\"categories$ | async\">&#8226; Category</h3>\n\t<div class=\"list-group category-list\">\n\t\t<a\n\t\t\t*ngIf=\"categories$ | async\"\n\t\t\t[routerLink]=\"['/']\"\n\t\t\tclass=\"list-group-item\"\n\t\t\t[ngClass]=\"{'active': !category}\">\n\t\t\tAll Categories\n\t\t</a>\n\t\t<a\n\t\t\t*ngFor=\"let c of categories$ | async\"\n\t\t\t[routerLink]=\"['/']\"\n\t\t\t[queryParams]=\"{ category: c.key }\"\n\t\t\tclass=\"list-group-item\"\n\t\t\t[ngClass]=\"{'active': category === c.key}\">\n\t\t\t{{ c.name }}\n\t\t</a>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/products/product-filter/product-filter.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/products/product-filter/product-filter.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sticky-top {\n  top: 90px; }\n\n.sidebar-category .category-list a {\n  text-decoration: none; }\n\n.sidebar-category .category-list a:not(.active):hover {\n    background: #f1f1f1; }\n\n.sidebar-category .category-list a:not(.active) {\n    color: #212529; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wcm9kdWN0cy9wcm9kdWN0LWZpbHRlci9EOlxceGFtcHA1NVxcaHRkb2NzXFxsZWFybl9hbmd1bGFyXFxtb3NoX2hhbWVkYW5pXFxsZXNzb24wMS9zcmNcXGFwcFxcY29tcG9uZW50c1xccHJvZHVjdHNcXHByb2R1Y3QtZmlsdGVyXFxwcm9kdWN0LWZpbHRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFNBQVMsRUFBQTs7QUFFVjtFQUdHLHFCQUFxQixFQUFBOztBQUh4QjtJQUtJLG1CQUFtQixFQUFBOztBQUx2QjtJQVFJLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZHVjdHMvcHJvZHVjdC1maWx0ZXIvcHJvZHVjdC1maWx0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RpY2t5LXRvcCB7XHJcblx0dG9wOiA5MHB4O1xyXG59XHJcbi5zaWRlYmFyLWNhdGVnb3J5IHtcclxuXHQuY2F0ZWdvcnktbGlzdCB7XHJcblx0XHRhIHtcclxuXHRcdFx0dGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cdFx0XHQmOm5vdCguYWN0aXZlKTpob3ZlciB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogI2YxZjFmMTtcclxuXHRcdFx0fVxyXG5cdFx0XHQmOm5vdCguYWN0aXZlKSB7XHJcblx0XHRcdFx0Y29sb3I6ICMyMTI1Mjk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/products/product-filter/product-filter.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/products/product-filter/product-filter.component.ts ***!
  \********************************************************************************/
/*! exports provided: ProductFilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductFilterComponent", function() { return ProductFilterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../shared/services/category.service */ "./src/app/shared/services/category.service.ts");




var ProductFilterComponent = /** @class */ (function () {
    function ProductFilterComponent(categoryService) {
        this.categoryService = categoryService;
        this.categories$ = this.categoryService.getAllCategories().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
            return changes.map(function (c) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val())); });
        }));
    }
    ProductFilterComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ProductFilterComponent.prototype, "category", void 0);
    ProductFilterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-filter',
            template: __webpack_require__(/*! ./product-filter.component.html */ "./src/app/components/products/product-filter/product-filter.component.html"),
            styles: [__webpack_require__(/*! ./product-filter.component.scss */ "./src/app/components/products/product-filter/product-filter.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"]])
    ], ProductFilterComponent);
    return ProductFilterComponent;
}());



/***/ }),

/***/ "./src/app/components/products/products.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/products/products.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n\t<div class=\"col-md-3\">\r\n\t\t<app-product-filter [category]=\"category\"></app-product-filter>\r\n\t</div>\r\n\t<div class=\"col-md-9\">\r\n\t\t<p *ngIf=\"filteredProducts.length > 0\">Total: {{ filteredProducts.length }}</p>\r\n\t\t<div class=\"row\" *ngIf=\"cart$ | async as cart\">\r\n\t\t\t<div class=\"col-sm-6 col-xl-4 mb-4\" *ngFor=\"let product of filteredProducts\">\r\n\t\t\t\t<app-product-card [product]=\"product\" [shopping-cart]=\"cart\"></app-product-card>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/products/products.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/products/products.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZHVjdHMvcHJvZHVjdHMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/products/products.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/products/products.component.ts ***!
  \***********************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../shared/services/product.service */ "./src/app/shared/services/product.service.ts");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");






var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(productService, shoppingCartService, route) {
        this.productService = productService;
        this.shoppingCartService = shoppingCartService;
        this.route = route;
        this.products = [];
        this.filteredProducts = [];
    }
    ProductsComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.shoppingCartService.getCart()];
                    case 1:
                        _a.cart$ = _b.sent();
                        this.cart$.subscribe(function (x) { return console.log(x); }); //test
                        this.populateProducts();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductsComponent.prototype.populateProducts = function () {
        var _this = this;
        this.productService.getAllProducts().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (c) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val())); });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(function (products) {
            console.log(products);
            _this.products = products;
            return _this.route.queryParamMap;
        })).subscribe(function (params) {
            _this.category = params.get('category');
            _this.applyFilter();
        });
    };
    ProductsComponent.prototype.applyFilter = function () {
        var _this = this;
        this.filteredProducts =
            (this.category ? this.products.filter(function (p) { return p.category === _this.category; }) : this.products);
    };
    ProductsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! ./products.component.html */ "./src/app/components/products/products.component.html"),
            styles: [__webpack_require__(/*! ./products.component.scss */ "./src/app/components/products/products.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"],
            _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_5__["ShoppingCartService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/components/shipping-form/shipping-form.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/shipping-form/shipping-form.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form\" [formGroup]=\"formShipping\" (ngSubmit)=\"onSubmitShipping()\">\n\t<div class=\"form-group\">\n\t\t<label>Name</label>\n\t\t<input\n\t\t\ttype=\"text\"\n\t\t\tname=\"name\"\n\t\t\tclass=\"form-control\"\n\t\t\tplaceholder=\"name\"\n\t\t\tformControlName=\"name\"\n\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.name.errors }\"\n\t\t>\n\t\t<span *ngIf=\"isSubmitted && f.name.errors?.required\" class=\"badge badge-danger\">Name is require.</span>\n\t</div>\n\n\t<div class=\"form-group\">\n\t\t<label>Address</label>\n\t\t<input\n\t\t\ttype=\"text\"\n\t\t\tname=\"addressLine1\"\n\t\t\tclass=\"form-control\"\n\t\t\tplaceholder=\"line1\"\n\t\t\tformControlName=\"addressLine1\"\n\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.addressLine1.errors }\"\n\t\t>\n\t\t<span *ngIf=\"isSubmitted && f.addressLine1.errors?.required\" class=\"badge badge-danger\">Address Line 1 is require.</span>\n\t</div>\n\n\t<div class=\"form-group\">\n\t\t<input\n\t\t\ttype=\"text\"\n\t\t\tname=\"addressLine2\"\n\t\t\tclass=\"form-control\"\n\t\t\tplaceholder=\"line2\"\n\t\t\tformControlName=\"addressLine2\"\n\t\t>\n\t</div>\n\n\t<div class=\"form-group\">\n\t\t<label>City</label>\n\t\t<input\n\t\t\ttype=\"text\"\n\t\t\tname=\"city\"\n\t\t\tclass=\"form-control\"\n\t\t\tplaceholder=\"city\"\n\t\t\tformControlName=\"city\"\n\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.city.errors }\"\n\t\t>\n\t\t<span *ngIf=\"isSubmitted && f.city.errors?.required\" class=\"badge badge-danger\">City is require.</span>\n\t</div>\n\n\t<div class=\"form-group text-center\">\n\t\t<button type=\"submit\" class=\"btn btn-primary\">\n\t\t\tPlace Order\n\t\t\t<span *ngIf=\"isLoading\">&nbsp;<img src=\"data:image/gif;base64,R0lGODlhFAAUAPAAAP////7+/iH5BAkKAAAAIf4aQ3JlYXRlZCB3aXRoIGFqYXhsb2FkLmluZm8AIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAFAAUAAACJoSPqcudAZ2CUSIaJGX1dc9Z4kgeH5eZqXOW7ttsS2uss0hrNpwUACH5BAkKAAAALAAAAAAUABQAAAImhI+py50RHINRvgrgbFR1/1lHKJamJqGGymHmC5ckwtKO28Z1nBQAIfkECQoAAAAsAAAAABQAFAAAAiaEj6nL7Y9CgErSySSOdmnKgeLzGV2jbWdWiusIs1vyHnN13XnsFAAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAStLJbNWb+g8aHDBmXLlIWMiy6rZGb9XMDgrhrVEAACH5BAkKAAAALAAAAAAUABQAAAInhI+py+2PQoBK0sls1Zv6D2aYwwGlGKaqcbKtO0bRKMVLbT94WC8FACH5BAkKAAAALAAAAAAUABQAAAIlhI+py+2PQoBK0sls1Zv6D2aYwwGlGKbkCUljwpqx8TZueINzAQAh+QQJCgAAACwAAAAAFAAUAAACJ4SPqcvtj0KAStLJbNWb+n9I2CeO2Vg+HLCCriG68dLCdXiZ3v0eBQAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAYB4JpVWYd79o1NhspPh8qPOR7juuSRuxFH2bsFIAADs=\" /></span>\n\t\t</button>\n\t</div>\n</form>\n"

/***/ }),

/***/ "./src/app/components/shipping-form/shipping-form.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/shipping-form/shipping-form.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hpcHBpbmctZm9ybS9zaGlwcGluZy1mb3JtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/shipping-form/shipping-form.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/shipping-form/shipping-form.component.ts ***!
  \*********************************************************************/
/*! exports provided: ShippingFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingFormComponent", function() { return ShippingFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../shared/models/shopping-cart.models */ "./src/app/shared/models/shopping-cart.models.ts");
/* harmony import */ var _shared_models_order_models__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../shared/models/order.models */ "./src/app/shared/models/order.models.ts");







var ShippingFormComponent = /** @class */ (function () {
    function ShippingFormComponent(formBuilder, authService, orderService) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.orderService = orderService;
        this.isSubmitted = false;
        this.isLoading = false;
    }
    ShippingFormComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.formShipping = this.formBuilder.group({
                    name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                    addressLine1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                    addressLine2: [''],
                    city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                });
                this.userSubscription = this.authService.user$.subscribe(function (user) { return _this.userId = user.uid; });
                return [2 /*return*/];
            });
        });
    };
    Object.defineProperty(ShippingFormComponent.prototype, "f", {
        get: function () {
            return this.formShipping.controls;
        },
        enumerable: true,
        configurable: true
    });
    ShippingFormComponent.prototype.onSubmitShipping = function () {
        var _this = this;
        this.isSubmitted = true;
        if (this.formShipping.invalid)
            return false;
        this.isLoading = true;
        var order = new _shared_models_order_models__WEBPACK_IMPORTED_MODULE_6__["Order"](this.userId, this.formShipping.value, this.cart);
        console.log(order);
        this.orderService.placeOrder(order)
            .catch(function (err) {
            console.log(err);
            _this.isLoading = false;
        });
    };
    ShippingFormComponent.prototype.ngOnDestroy = function () {
        if (this.userSubscription)
            this.userSubscription.unsubscribe();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_5__["ShoppingCart"])
    ], ShippingFormComponent.prototype, "cart", void 0);
    ShippingFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shipping-form',
            template: __webpack_require__(/*! ./shipping-form.component.html */ "./src/app/components/shipping-form/shipping-form.component.html"),
            styles: [__webpack_require__(/*! ./shipping-form.component.scss */ "./src/app/components/shipping-form/shipping-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"]])
    ], ShippingFormComponent);
    return ShippingFormComponent;
}());



/***/ }),

/***/ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/shopping-cart-summary/shopping-cart-summary.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n\t<div class=\"card-body\">\n\t\t<h5 class=\"card-title\">Order Summary</h5>\n\t\t<p class=\"card-text\">You have <strong>{{ cart.totalItemsCount }}</strong> items in your shopping cart.</p>\n\t\t<ul class=\"list-group list-group-flush\">\n\t\t\t<li class=\"list-group-item\" *ngFor=\"let item of cart.items\">\n\t\t\t\t{{ item.quantity }} x {{ item.title }}\n\t\t\t\t<span class=\"float-right font-weight-bold\">\n\t\t\t\t\t{{ item.totalPrice | currency:'USD' }}\n\t\t\t\t</span>\n\t\t\t</li>\n\t\t\t<li class=\"list-group-item font-weight-bold\">\n\t\t\t\tTotal\n\t\t\t\t<span class=\"float-right\">\n\t\t\t\t\t{{ cart.totalPrice | currency:'USD' }}\n\t\t\t\t</span>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/components/shopping-cart-summary/shopping-cart-summary.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hvcHBpbmctY2FydC1zdW1tYXJ5L3Nob3BwaW5nLWNhcnQtc3VtbWFyeS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/shopping-cart-summary/shopping-cart-summary.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ShoppingCartSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartSummaryComponent", function() { return ShoppingCartSummaryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/models/shopping-cart.models */ "./src/app/shared/models/shopping-cart.models.ts");



var ShoppingCartSummaryComponent = /** @class */ (function () {
    function ShoppingCartSummaryComponent() {
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_2__["ShoppingCart"])
    ], ShoppingCartSummaryComponent.prototype, "cart", void 0);
    ShoppingCartSummaryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shopping-cart-summary',
            template: __webpack_require__(/*! ./shopping-cart-summary.component.html */ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.html"),
            styles: [__webpack_require__(/*! ./shopping-cart-summary.component.scss */ "./src/app/components/shopping-cart-summary/shopping-cart-summary.component.scss")]
        })
    ], ShoppingCartSummaryComponent);
    return ShoppingCartSummaryComponent;
}());



/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 class=\"mb-3\">Shopping Cart</h3>\r\n\r\n<div *ngIf=\"cart$ | async as cart\">\r\n\t<p>\r\n\t\tYou have <strong>{{ cart.totalItemsCount }}</strong> items in your shopping cart\r\n\t\t&nbsp;\r\n\t\t<button\r\n\t\t\ttype=\"button\"\r\n\t\t\tclass=\"btn btn-sm btn-light\"\r\n\t\t\t*ngIf=\"cart.totalItemsCount\"\r\n\t\t\t(click)=\"clearCart()\">\r\n\t\t\tClear Shopping Cart\r\n\t\t</button>\r\n\t</p>\r\n\t<div class=\"table-responsive\">\r\n\t\t<table class=\"table\">\r\n\t\t\t<thead>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<th class=\"text-center\" style=\"width: 80px\">Order</th>\r\n\t\t\t\t\t<th class=\"text-center\" style=\"width: 100px\">Image</th>\r\n\t\t\t\t\t<th>Product</th>\r\n\t\t\t\t\t<th class=\"text-center\" style=\"width: 230px\">Quantity</th>\r\n\t\t\t\t\t<th class=\"text-right\" style=\"width: 130px\">Price</th>\r\n\t\t\t\t</tr>\r\n\t\t\t</thead>\r\n\t\t\t<tbody>\r\n\t\t\t\t<tr *ngFor=\"let item of cart.items; let i = index\">\r\n\t\t\t\t\t<td class=\"text-center\">{{ i + 1 }}</td>\r\n\t\t\t\t\t<td class=\"text-center\"><img [src]=\"item.imageUrl\" [alt]=\"item.title\" width=\"70\"></td>\r\n\t\t\t\t\t<td>{{ item.title }}</td>\r\n\t\t\t\t\t<td class=\"text-center\">\r\n\t\t\t\t\t\t<app-product-quantity [product]=\"item\" [shopping-cart]=\"cart\"></app-product-quantity>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td class=\"text-right\">{{ item.totalPrice | currency:'USD' }}</td>\r\n\t\t\t\t</tr>\r\n\t\t\t</tbody>\r\n\t\t\t<tfoot>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td></td>\r\n\t\t\t\t\t<td></td>\r\n\t\t\t\t\t<td></td>\r\n\t\t\t\t\t<td></td>\r\n\t\t\t\t\t<td class=\"text-right\"><strong>{{ cart.totalPrice | currency:'USD' }}</strong></td>\r\n\t\t\t\t</tr>\r\n\t\t\t</tfoot>\r\n\t\t</table>\r\n\t</div>\r\n\t<a\r\n\t\tclass=\"btn btn-primary\"\r\n\t\t*ngIf=\"cart.items.length\"\r\n\t\t[routerLink]=\"['/check-out']\">\r\n\t\tCheck Out\r\n\t</a>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media only screen and (max-width: 991px) {\n  table {\n    min-width: 690px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zaG9wcGluZy1jYXJ0L0Q6XFx4YW1wcDU1XFxodGRvY3NcXGxlYXJuX2FuZ3VsYXJcXG1vc2hfaGFtZWRhbmlcXGxlc3NvbjAxL3NyY1xcYXBwXFxjb21wb25lbnRzXFxzaG9wcGluZy1jYXJ0XFxzaG9wcGluZy1jYXJ0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNDO0VBREQ7SUFFRSxnQkFBZ0IsRUFBQSxFQUVqQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hvcHBpbmctY2FydC9zaG9wcGluZy1jYXJ0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG5cdEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpIHtcclxuXHRcdG1pbi13aWR0aDogNjkwcHg7XHJcblx0fVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.ts ***!
  \*********************************************************************/
/*! exports provided: ShoppingCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartComponent", function() { return ShoppingCartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared/services/shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");



var ShoppingCartComponent = /** @class */ (function () {
    function ShoppingCartComponent(shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }
    ShoppingCartComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.shoppingCartService.getCart()];
                    case 1:
                        _a.cart$ = _b.sent();
                        this.cart$.subscribe(function (x) { return console.log(x); }); //test
                        return [2 /*return*/];
                }
            });
        });
    };
    ShoppingCartComponent.prototype.clearCart = function () {
        if (confirm('Do you want to clear the cart?')) {
            this.shoppingCartService.clearCart();
        }
    };
    ShoppingCartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shopping-cart',
            template: __webpack_require__(/*! ./shopping-cart.component.html */ "./src/app/components/shopping-cart/shopping-cart.component.html"),
            styles: [__webpack_require__(/*! ./shopping-cart.component.scss */ "./src/app/components/shopping-cart/shopping-cart.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], ShoppingCartComponent);
    return ShoppingCartComponent;
}());



/***/ }),

/***/ "./src/app/shared/guards/admin-auth.guard.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/guards/admin-auth.guard.ts ***!
  \***************************************************/
/*! exports provided: AdminAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminAuthGuard", function() { return AdminAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../services/auth.service */ "./src/app/shared/services/auth.service.ts");




var AdminAuthGuard = /** @class */ (function () {
    function AdminAuthGuard(authService) {
        this.authService = authService;
    }
    AdminAuthGuard.prototype.canActivate = function (next, state) {
        return this.authService.appUser$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (appUser) { return appUser.isAdmin; }));
        // return this.authService.user$.pipe(
        // 	switchMap( (user: firebase.User) => this.userService.getUser(user.uid).valueChanges() ),
        // 	map( (appUser: AppUser) => appUser.isAdmin )
        // );
    };
    AdminAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], AdminAuthGuard);
    return AdminAuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/guards/auth.guard.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/guards/auth.guard.ts ***!
  \*********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../services/auth.service */ "./src/app/shared/services/auth.service.ts");





var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return this.authService.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (user) {
            if (user) {
                console.log('canActivate: Logged In');
                return true;
            }
            console.log('canActivate: Not Logged In');
            _this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }));
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/models/order.models.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/models/order.models.ts ***!
  \***********************************************/
/*! exports provided: Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
var Order = /** @class */ (function () {
    function Order(userId, shipping, shoppingCart) {
        this.userId = userId;
        this.shipping = shipping;
        this.datePlaced = new Date().getTime();
        this.items = shoppingCart.items.map(function (i) {
            return {
                product: {
                    title: i.title,
                    price: i.price,
                    category: i.category,
                    imageUrl: i.imageUrl
                },
                quanlity: i.quantity,
                totalPrice: i.totalPrice
            };
        });
    }
    return Order;
}());



/***/ }),

/***/ "./src/app/shared/models/shopping-cart-item.models.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/models/shopping-cart-item.models.ts ***!
  \************************************************************/
/*! exports provided: ShoppingCartItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartItem", function() { return ShoppingCartItem; });
var ShoppingCartItem = /** @class */ (function () {
    function ShoppingCartItem(init) {
        Object.assign(this, init);
    }
    Object.defineProperty(ShoppingCartItem.prototype, "totalPrice", {
        get: function () {
            return this.price * this.quantity;
        },
        enumerable: true,
        configurable: true
    });
    return ShoppingCartItem;
}());



/***/ }),

/***/ "./src/app/shared/models/shopping-cart.models.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/models/shopping-cart.models.ts ***!
  \*******************************************************/
/*! exports provided: ShoppingCart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCart", function() { return ShoppingCart; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shopping_cart_item_models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shopping-cart-item.models */ "./src/app/shared/models/shopping-cart-item.models.ts");


var ShoppingCart = /** @class */ (function () {
    function ShoppingCart(itemsMap) {
        this.itemsMap = itemsMap;
        this.items = [];
        this.itemsMap = this.itemsMap || {};
        for (var key in itemsMap) {
            this.items.push(new _shopping_cart_item_models__WEBPACK_IMPORTED_MODULE_1__["ShoppingCartItem"](tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: key }, itemsMap[key])));
        }
    }
    Object.defineProperty(ShoppingCart.prototype, "totalItemsCount", {
        get: function () {
            var count = 0;
            for (var key in this.itemsMap) {
                count += this.itemsMap[key].quantity;
            }
            return count;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ShoppingCart.prototype, "totalPrice", {
        get: function () {
            var sum = 0;
            for (var key in this.items) {
                sum += this.items[key].totalPrice;
            }
            return sum;
        },
        enumerable: true,
        configurable: true
    });
    ShoppingCart.prototype.getQuantity = function (product) {
        var item = this.itemsMap[product.key];
        return item ? item.quantity : 0;
    };
    return ShoppingCart;
}());



/***/ }),

/***/ "./src/app/shared/services/auth.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/auth.service.ts ***!
  \*************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../services/user.service */ "./src/app/shared/services/user.service.ts");








var AuthService = /** @class */ (function () {
    function AuthService(afAuth, route, userService) {
        this.afAuth = afAuth;
        this.route = route;
        this.userService = userService;
        this.user$ = this.afAuth.authState;
        this.user$.subscribe(function (x) { return console.log(x); }); //test
    }
    AuthService.prototype.loginWithGoogle = function () {
        console.log('CALL: loginWithGoogle()');
        var returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
        localStorage.setItem('returnUrl', returnUrl);
        return this.afAuth.auth.signInWithRedirect(new firebase__WEBPACK_IMPORTED_MODULE_4__["auth"].GoogleAuthProvider());
    };
    AuthService.prototype.logout = function () {
        console.log('CALL: logout()');
        return this.afAuth.auth.signOut();
    };
    Object.defineProperty(AuthService.prototype, "appUser$", {
        get: function () {
            var _this = this;
            console.log('CALL: get appUser$');
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (user) {
                if (user)
                    return _this.userService.getUser(user.uid).valueChanges();
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["defaultIfEmpty"])(null));
            }));
        },
        enumerable: true,
        configurable: true
    });
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/shared/services/category.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/services/category.service.ts ***!
  \*****************************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");



var CategoryService = /** @class */ (function () {
    function CategoryService(db) {
        this.db = db;
    }
    CategoryService.prototype.getAllCategories = function () {
        console.log('CALL: getAllCategories()');
        return this.db.list('/categories', function (result) { return result.orderByChild('name'); });
    };
    CategoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
    ], CategoryService);
    return CategoryService;
}());



/***/ }),

/***/ "./src/app/shared/services/order.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/order.service.ts ***!
  \**************************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _shopping_cart_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shopping-cart.service */ "./src/app/shared/services/shopping-cart.service.ts");





var OrderService = /** @class */ (function () {
    function OrderService(db, router, shoppingCartService) {
        this.db = db;
        this.router = router;
        this.shoppingCartService = shoppingCartService;
    }
    OrderService.prototype.placeOrder = function (order) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var result;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('CALL: placeOrder(order)');
                        return [4 /*yield*/, this.db.list('/orders').push(order)];
                    case 1:
                        result = _a.sent();
                        this.shoppingCartService.clearCart();
                        this.router.navigate(['/order-success', result.key]);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OrderService.prototype.getOrders = function () {
        return this.db.list('/orders');
    };
    OrderService.prototype.getOrdersByUser = function (userId) {
        return this.db.list('/orders', function (result) { return result.orderByChild('userId').equalTo(userId); });
    };
    OrderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _shopping_cart_service__WEBPACK_IMPORTED_MODULE_4__["ShoppingCartService"]])
    ], OrderService);
    return OrderService;
}());



/***/ }),

/***/ "./src/app/shared/services/product.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/product.service.ts ***!
  \****************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");




var ProductService = /** @class */ (function () {
    function ProductService(db, router) {
        this.db = db;
        this.router = router;
    }
    ProductService.prototype.getAllProducts = function () {
        console.log('CALL: getAllProducts()');
        return this.db.list('/products');
    };
    ProductService.prototype.getProduct = function (id) {
        console.log('CALL: getProduct(id)');
        return this.db.object('/products/' + id);
    };
    ProductService.prototype.createProduct = function (product) {
        var _this = this;
        console.log('CALL: createProduct()');
        return this.db.list('/products').push(product)
            .then(function (result) {
            console.log(result);
            _this.router.navigate(['/admin/products']);
        });
    };
    ProductService.prototype.updateProduct = function (id, product) {
        var _this = this;
        console.log('CALL: updateProduct(product)');
        return this.db.object('/products/' + id).update(product)
            .then(function () {
            _this.router.navigate(['/admin/products']);
        });
    };
    ProductService.prototype.deleteProduct = function (id) {
        var _this = this;
        console.log('CALL: deleteProduct(id)');
        return this.db.object('/products/' + id).remove()
            .then(function () {
            _this.router.navigate(['/admin/products']);
        });
    };
    ProductService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ProductService);
    return ProductService;
}());



/***/ }),

/***/ "./src/app/shared/services/shopping-cart.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/services/shopping-cart.service.ts ***!
  \**********************************************************/
/*! exports provided: ShoppingCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartService", function() { return ShoppingCartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../shared/models/shopping-cart.models */ "./src/app/shared/models/shopping-cart.models.ts");





var ShoppingCartService = /** @class */ (function () {
    function ShoppingCartService(db) {
        this.db = db;
    }
    ShoppingCartService.prototype.getCart = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var cartId;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('CALL: getCart()');
                        return [4 /*yield*/, this.getOrCreateCartId()];
                    case 1:
                        cartId = _a.sent();
                        return [2 /*return*/, this.db.object('/shopping-carts/' + cartId).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (change) { return new _shared_models_shopping_cart_models__WEBPACK_IMPORTED_MODULE_4__["ShoppingCart"](change['items']); }))];
                }
            });
        });
    };
    ShoppingCartService.prototype.addToCart = function (product) {
        console.log('CALL: addToCart(product)');
        this.updateItem(product, 1);
    };
    ShoppingCartService.prototype.removeFromCart = function (product) {
        console.log('CALL: removeFromCart(product)');
        this.updateItem(product, -1);
    };
    ShoppingCartService.prototype.clearCart = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var cartId;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getOrCreateCartId()];
                    case 1:
                        cartId = _a.sent();
                        this.db.list('/shopping-carts/' + cartId + '/items').remove();
                        return [2 /*return*/];
                }
            });
        });
    };
    ShoppingCartService.prototype.getOrCreateCartId = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var cartId, result;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        cartId = localStorage.getItem('cartId');
                        if (!cartId) return [3 /*break*/, 1];
                        return [2 /*return*/, cartId];
                    case 1: return [4 /*yield*/, this.createCart()];
                    case 2:
                        result = _a.sent();
                        localStorage.setItem('cartId', result.key);
                        return [2 /*return*/, result.key];
                }
            });
        });
    };
    ShoppingCartService.prototype.createCart = function () {
        return this.db.list('/shopping-carts').push({
            dataCreated: new Date().getTime()
        });
    };
    ShoppingCartService.prototype.getCartItem = function (cartId, productId) {
        return this.db.object('/shopping-carts/' + cartId + '/items/' + productId);
    };
    ShoppingCartService.prototype.updateItem = function (product, change) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var cartId, item$;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getOrCreateCartId()];
                    case 1:
                        cartId = _a.sent();
                        item$ = this.getCartItem(cartId, product.key);
                        item$.valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1)).subscribe(function (item) {
                            var quantity = (item ? item.quantity : 0) + change;
                            if (quantity === 0) {
                                item$.remove();
                            }
                            else {
                                item$.update({
                                    title: product.title,
                                    price: product.price,
                                    category: product.category,
                                    imageUrl: product.imageUrl,
                                    quantity: quantity
                                });
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ShoppingCartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
    ], ShoppingCartService);
    return ShoppingCartService;
}());



/***/ }),

/***/ "./src/app/shared/services/user.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/user.service.ts ***!
  \*************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");



var UserService = /** @class */ (function () {
    function UserService(db) {
        this.db = db;
    }
    UserService.prototype.saveUser = function (user) {
        console.log('CALL: saveUser()');
        this.db.object('/users/' + user.uid).update({
            name: user.displayName,
            email: user.email
        });
    };
    UserService.prototype.getUser = function (uid) {
        console.log('CALL: getUser(uid)');
        return this.db.object('/users/' + uid);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCda8TUSkuhylXNrj-hXhCOvCKG-Jus_Sw",
        authDomain: "oshop-lesson.firebaseapp.com",
        databaseURL: "https://oshop-lesson.firebaseio.com",
        projectId: "oshop-lesson",
        storageBucket: "oshop-lesson.appspot.com",
        messagingSenderId: "1046051959306"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\xampp55\htdocs\learn_angular\mosh_hamedani\lesson01\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map