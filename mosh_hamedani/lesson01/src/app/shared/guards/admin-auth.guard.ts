import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './../services/auth.service';
import { AppUser } from './../models/app-user.models';

@Injectable({
	providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {
	constructor(
		private authService: AuthService
	) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.authService.appUser$.pipe(
			map( (appUser: AppUser) => appUser.isAdmin )
		);
		// return this.authService.user$.pipe(
		// 	switchMap( (user: firebase.User) => this.userService.getUser(user.uid).valueChanges() ),
		// 	map( (appUser: AppUser) => appUser.isAdmin )
		// );
	}

}
