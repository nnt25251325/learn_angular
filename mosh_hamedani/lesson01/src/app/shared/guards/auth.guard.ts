import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { AuthService } from './../services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(
		private authService: AuthService,
		private router: Router
	) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.authService.user$.pipe(
			map((user: firebase.User) => {
				if (user) {
					console.log('canActivate: Logged In');
					return true;
				}

				console.log('canActivate: Not Logged In');
				this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
				return false;
			})
		);
	}

}
