import { ShoppingCart } from './shopping-cart.models';

export class Order {

	key?: string;
	datePlaced: number;
	items: any[];

	constructor(
		public userId: string,
		public shipping: any,
		shoppingCart: ShoppingCart,
	) {
		this.datePlaced = new Date().getTime();

		this.items = shoppingCart.items.map(i => {
			return {
				product: {
					title: i.title,
					price: i.price,
					category: i.category,
					imageUrl: i.imageUrl
				},
				quanlity: i.quantity,
				totalPrice: i.totalPrice
			}
		})
	}

}
