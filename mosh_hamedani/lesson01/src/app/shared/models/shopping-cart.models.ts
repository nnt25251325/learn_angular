import { ShoppingCartItem } from './shopping-cart-item.models';
import { Product } from './product.models';

export class ShoppingCart {

	items: ShoppingCartItem[] = [];

	constructor(
		private itemsMap: { [key: string]: ShoppingCartItem }
	) {
		this.itemsMap = this.itemsMap || {};
		for (let key in itemsMap) {
			this.items.push( new ShoppingCartItem({ key: key, ...itemsMap[key] }) );
		}
	}

	get totalItemsCount(): number {
		let count = 0;
		for (let key in this.itemsMap) {
			count += this.itemsMap[key].quantity;
		}
		return count;
	}

	get totalPrice(): number {
		let sum = 0;
		for (let key in this.items) {
			sum += this.items[key].totalPrice;
		}
		return sum;
	}

	getQuantity(product: Product) {
		let item = this.itemsMap[product.key];
		return item ? item.quantity : 0;
	}

}
