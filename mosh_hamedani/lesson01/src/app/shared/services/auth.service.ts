import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { Observable, of } from 'rxjs';
import { switchMap, defaultIfEmpty } from 'rxjs/operators';
import { UserService } from './../services/user.service';
import { AppUser } from './../models/app-user.models';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	user$: Observable<firebase.User>;

	constructor(
		private afAuth: AngularFireAuth,
		private route: ActivatedRoute,
		private userService: UserService
	) {
		this.user$ = this.afAuth.authState;
		this.user$.subscribe(x => console.log(x)); //test
	}

	loginWithGoogle(): Promise<void> {
		console.log('CALL: loginWithGoogle()');
		let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
		localStorage.setItem('returnUrl', returnUrl);
		return this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
	}

	logout(): Promise<void> {
		console.log('CALL: logout()');
		return this.afAuth.auth.signOut();
	}

	get appUser$(): Observable<AppUser> {
		console.log('CALL: get appUser$');
		return this.user$.pipe(
			switchMap( (user: firebase.User) => {
				if (user) return this.userService.getUser(user.uid).valueChanges();
				return of().pipe(defaultIfEmpty(null));
			})
		);
	}

}
