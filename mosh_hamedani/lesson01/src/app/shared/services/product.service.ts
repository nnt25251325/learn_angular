import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Product } from './../models/product.models';

@Injectable({
	providedIn: 'root'
})
export class ProductService {

	constructor(
		private db: AngularFireDatabase,
		private router: Router
	) { }

	getAllProducts(): AngularFireList<any> {
		console.log('CALL: getAllProducts()');
		return this.db.list('/products');
	}

	getProduct(id: string): AngularFireObject<any> {
		console.log('CALL: getProduct(id)');
		return this.db.object('/products/' + id);
	}

	createProduct(product: Product): Promise<void> {
		console.log('CALL: createProduct()');
		return this.db.list('/products').push(product)
			.then(result => {
				console.log(result);
				this.router.navigate(['/admin/products']);
			});
	}

	updateProduct(id: string, product: Product): Promise<void> {
		console.log('CALL: updateProduct(product)');
		return this.db.object('/products/' + id).update(product)
			.then(() => {
				this.router.navigate(['/admin/products']);
			});
	}

	deleteProduct(id: string): Promise<void> {
		console.log('CALL: deleteProduct(id)');
		return this.db.object('/products/' + id).remove()
			.then(() => {
				this.router.navigate(['/admin/products']);
			});
	}

}
