import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { AppUser } from './../models/app-user.models';

@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(
		private db: AngularFireDatabase
	) { }

	saveUser(user: firebase.User): void {
		console.log('CALL: saveUser()');
		this.db.object('/users/' + user.uid).update({
			name: user.displayName,
			email: user.email
		});
	}

	getUser(uid: string): AngularFireObject<AppUser> {
		console.log('CALL: getUser(uid)');
		return this.db.object('/users/' + uid);
	}

}
