import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import * as firebase from 'firebase';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable({
	providedIn: 'root'
})
export class OrderService {

	constructor(
		private db: AngularFireDatabase,
		private router: Router,
		private shoppingCartService: ShoppingCartService
	) { }

	async placeOrder(order): Promise<firebase.database.Reference> {
		console.log('CALL: placeOrder(order)');
		let result = await this.db.list('/orders').push(order);
		this.shoppingCartService.clearCart();
		this.router.navigate(['/order-success', result.key]);
		return result;
	}

	getOrders(): AngularFireList<any> {
		return this.db.list('/orders');
	}

	getOrdersByUser(userId: string): AngularFireList<any> {
		return this.db.list('/orders', result => result.orderByChild('userId').equalTo(userId));
	}

}
