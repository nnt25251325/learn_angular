import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
	providedIn: 'root'
})
export class CategoryService {

	constructor(
		private db: AngularFireDatabase
	) { }

	getAllCategories(): AngularFireList<any> {
		console.log('CALL: getAllCategories()');
		return this.db.list('/categories', result => result.orderByChild('name'));
	}

}
