import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Product } from './../models/product.models';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Injectable({
	providedIn: 'root'
})
export class ShoppingCartService {

	constructor(
		private db: AngularFireDatabase
	) { }

	async getCart(): Promise<Observable<ShoppingCart>> {
		console.log('CALL: getCart()');
		let cartId = await this.getOrCreateCartId();
		return this.db.object('/shopping-carts/' + cartId).valueChanges().pipe(
			map(change => new ShoppingCart(change['items']))
		);
		// return this.db.object('/shopping-carts/' + cartId).snapshotChanges().pipe(
		// 	map(change => new ShoppingCart(change.payload.val()['items']))
		// );
	}

	addToCart(product: Product): void {
		console.log('CALL: addToCart(product)');
		this.updateItem(product, 1);
	}

	removeFromCart(product: Product): void {
		console.log('CALL: removeFromCart(product)');
		this.updateItem(product, -1);
	}

	async clearCart(): Promise<void> {
		let cartId = await this.getOrCreateCartId();
		this.db.list('/shopping-carts/' + cartId + '/items').remove();
	}

	private async getOrCreateCartId(): Promise<string> {
		let cartId = localStorage.getItem('cartId');
		if (cartId) {
			return cartId;
		} else {
			let result = await this.createCart();
			localStorage.setItem('cartId', result.key);
			return result.key;
		}
	}

	private createCart(): firebase.database.ThenableReference {
		return this.db.list('/shopping-carts').push({
			dataCreated: new Date().getTime()
		});
	}

	private getCartItem(cartId: string, productId: string): AngularFireObject<any> {
		return this.db.object('/shopping-carts/' + cartId + '/items/' + productId);
	}

	private async updateItem(product: Product, change: number): Promise<void> {
		let cartId = await this.getOrCreateCartId();
		let item$ = this.getCartItem(cartId, product.key);
		item$.valueChanges().pipe(take(1)).subscribe((item: any) => {
			let quantity = (item ? item.quantity : 0) + change;
			if (quantity === 0) {
				item$.remove();
			} else {
				item$.update({
					title: product.title,
					price: product.price,
					category: product.category,
					imageUrl: product.imageUrl,
					quantity: quantity
				});
			}
		});
	}

}
