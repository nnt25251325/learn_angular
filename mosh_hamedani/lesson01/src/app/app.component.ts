import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './shared/services/auth.service';
import { UserService } from './shared/services/user.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	constructor(
		private authService: AuthService,
		private userService: UserService,
		private router: Router
	) {
		this.authService.user$.subscribe(user => {
			if (user) {
				this.userService.saveUser(user);
				let returnUrl = localStorage.getItem('returnUrl');
				if (returnUrl) {
					localStorage.removeItem('returnUrl');
					this.router.navigateByUrl(returnUrl);
				}
			}
		});
	}

}
