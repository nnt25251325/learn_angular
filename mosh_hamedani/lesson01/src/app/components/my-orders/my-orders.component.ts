import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import * as firebase from 'firebase';
import { AuthService } from './../../shared/services/auth.service';
import { OrderService } from './../../shared/services/order.service';
import { Order } from './../../shared/models/order.models';

@Component({
	selector: 'app-my-orders',
	templateUrl: './my-orders.component.html',
	styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {

	orders$: Observable<Order[]>;

	constructor(
		private authService: AuthService,
		private orderService: OrderService
	) {
		this.orders$ = this.authService.user$.pipe(
			switchMap((user: firebase.User) => {
				return this.orderService.getOrdersByUser(user.uid).snapshotChanges().pipe(
					map(changes =>
						changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
					)
				)
			})
		);

		this.orders$.subscribe(x => console.log(x)); //test
	}

	ngOnInit() {
	}

}
