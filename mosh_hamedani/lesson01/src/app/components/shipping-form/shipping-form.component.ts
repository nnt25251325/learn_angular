import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from './../../shared/services/auth.service';
import { OrderService } from './../../shared/services/order.service';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';
import { Order } from './../../shared/models/order.models';

@Component({
	selector: 'app-shipping-form',
	templateUrl: './shipping-form.component.html',
	styleUrls: ['./shipping-form.component.scss']
})
export class ShippingFormComponent implements OnInit, OnDestroy {

	@Input() cart: ShoppingCart;

	formShipping: FormGroup;
	userId: string;

	isSubmitted: boolean = false;
	isLoading: boolean = false;

	userSubscription: Subscription;

	constructor(
		private formBuilder: FormBuilder,
		private authService: AuthService,
		private orderService: OrderService
	) { }

	async ngOnInit() {
		this.formShipping = this.formBuilder.group({
			name: ['', Validators.required],
			addressLine1: ['', Validators.required],
			addressLine2: [''],
			city: ['', Validators.required],
		});

		this.userSubscription = this.authService.user$.subscribe(user => this.userId = user.uid );
	}

	get f() {
		return this.formShipping.controls;
	}

	onSubmitShipping(): any {
		this.isSubmitted = true;

		if (this.formShipping.invalid) return false;

		this.isLoading = true;

		let order = new Order(this.userId, this.formShipping.value, this.cart);
		console.log(order);
		this.orderService.placeOrder(order)
			.catch(err => {
				console.log(err);
				this.isLoading = false;
			});
	}

	ngOnDestroy() {
		if (this.userSubscription) this.userSubscription.unsubscribe();
	}

}
