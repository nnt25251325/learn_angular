import { Component, Input } from '@angular/core';
import { ShoppingCartService } from './../../shared/services/shopping-cart.service';
import { Product } from './../../shared/models/product.models';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Component({
	selector: 'app-product-card',
	templateUrl: './product-card.component.html',
	styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent {

	@Input() product: Product;
	@Input('shopping-cart') shoppingCart: ShoppingCart;
	@Input('show-actions') showActions: boolean = true;

	constructor(
		private shoppingCartService: ShoppingCartService
	) { }

	addToCart():void {
		this.shoppingCartService.addToCart(this.product);
	}

}
