import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrderService } from './../../../shared/services/order.service';
import { Order } from './../../../shared/models/order.models';

@Component({
	selector: 'app-admin-orders',
	templateUrl: './admin-orders.component.html',
	styleUrls: ['./admin-orders.component.scss']
})
export class AdminOrdersComponent implements OnInit {

	orders$: Observable<Order[]>;

	constructor(
		private orderService: OrderService
	) {
		this.orders$ = this.orderService.getOrders().snapshotChanges().pipe(
			map(changes =>
				changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
			)
		);

		this.orders$.subscribe(x => console.log(x)); //test
	}

	ngOnInit() {
	}

}
