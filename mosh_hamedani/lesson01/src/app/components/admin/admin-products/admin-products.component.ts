import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataTableResource } from 'angular7-data-table';
import { ProductService } from './../../../shared/services/product.service';
import { Product } from './../../../shared/models/product.models';

@Component({
	selector: 'app-admin-products',
	templateUrl: './admin-products.component.html',
	styleUrls: ['./admin-products.component.scss']
})
export class AdminProductsComponent implements OnInit, OnDestroy {

	subscription: Subscription;

	products: Product[];
	tableResource: DataTableResource<any>;
	items: Product[] = [];
	itemCount: number = 0;
	pageLimits: number[] = [10, 20, 40, 80, 100];

	constructor(
		private productService: ProductService
	) {
		this.productService.getAllProducts().snapshotChanges().pipe(
			map(changes =>
				changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
			)
		).subscribe((products: Product[]) => {
			this.products = products;
			this.initializeTable(products);
		});
	}

	private initializeTable(products: Product[]): void {
		this.tableResource = new DataTableResource(products);
		this.reloadItems({ offset: 0, limit: 10 });
		this.tableResource.count().then(count => this.itemCount = count );
	}

	ngOnInit() {
	}

	reloadItems(params: any): void {
		if (this.tableResource) {
			this.tableResource.query(params).then(items => this.items = items );
		}
	}

	deleteProduct(id: string): void {
		if (confirm('Are you sure want to delete this product?')) {
			this.productService.deleteProduct(id)
				.catch(err => {
					console.log(err);
				});
		}
	}

	filterTitle(keyword: string): void {
		let filteredProducts = this.products.filter(p => {
			return p.title.toLowerCase().indexOf(keyword.trim().toLowerCase()) != -1
		});
		this.initializeTable(filteredProducts);
	}

	ngOnDestroy() {
		if (this.subscription) this.subscription.unsubscribe();
	}

}
