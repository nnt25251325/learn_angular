import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { CategoryService } from './../../../shared/services/category.service';
import { ProductService } from './../../../shared/services/product.service';
import { Product } from './../../../shared/models/product.models';

@Component({
	selector: 'app-product-form',
	templateUrl: './product-form.component.html',
	styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy {

	subscription: Subscription;

	formProduct: FormGroup;
	categories$: Observable<any[]>;
	product: Product = {
		title: '',
		price: 0,
		category: '',
		imageUrl: ''
	};
	id: string;

	isSubmitted: boolean = false;
	isLoading: boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private categoryService: CategoryService,
		private productService: ProductService
	) {
		this.categories$ = this.categoryService.getAllCategories().snapshotChanges();

		this.id = this.route.snapshot.paramMap.get('id');
		if (this.id) {
			this.productService.getProduct(this.id).valueChanges().pipe(take(1)).subscribe((product: Product) => {
				this.formProduct.setValue(product);
			});
		}
	}

	ngOnInit() {
		this.formProduct = this.formBuilder.group({
			title: ['', Validators.required],
			price: ['', [Validators.required, Validators.min(0)] ],
			category: ['', Validators.required],
			imageUrl: ['', Validators.required]
		});

		this.subscription = this.formProduct.valueChanges.subscribe((product: Product) => {
			this.product = product;
		});
	}

	get f() {
		return this.formProduct.controls;
	}

	onSaveProduct(): any {
		this.isSubmitted = true;

		if (this.formProduct.invalid) return false;

		this.isLoading = true;

		if (this.id) {
			this.productService.updateProduct(this.id, this.product)
				.catch(err => {
					console.log(err);
					this.isLoading = false;
				});
		} else {
			this.productService.createProduct(this.product)
				.catch(err => {
					console.log(err);
					this.isLoading = false;
				});
		}
	}

	reset(): void {
		this.formProduct.reset();
		this.isSubmitted = false;
		this.isLoading = false;
	}

	deleteProduct(): void {
		if (confirm('Are you sure want to delete this product?')) {
			this.productService.deleteProduct(this.id)
				.catch(err => {
					console.log(err);
				});
		}
	}

	ngOnDestroy() {
		if (this.subscription) this.subscription.unsubscribe();
	}

}
