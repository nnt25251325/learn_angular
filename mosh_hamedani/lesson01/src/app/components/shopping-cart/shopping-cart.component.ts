import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ShoppingCartService } from './../../shared/services/shopping-cart.service';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Component({
	selector: 'app-shopping-cart',
	templateUrl: './shopping-cart.component.html',
	styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {

	cart$: Observable<ShoppingCart>;

	constructor(
		private shoppingCartService: ShoppingCartService
	) { }

	async ngOnInit() {
		this.cart$ = await this.shoppingCartService.getCart();
		this.cart$.subscribe(x => console.log(x)); //test
	}

	clearCart() {
		if (confirm('Do you want to clear the cart?')) {
			this.shoppingCartService.clearCart();
		}
	}

}
