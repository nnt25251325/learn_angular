import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryService } from './../../../shared/services/category.service';

@Component({
	selector: 'app-product-filter',
	templateUrl: './product-filter.component.html',
	styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit {

	categories$: Observable<any[]>;
	@Input() category: string;

	constructor(
		private categoryService: CategoryService
	) {
		this.categories$ = this.categoryService.getAllCategories().snapshotChanges().pipe(
			map(changes =>
				changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
			)
		);
	}

	ngOnInit() {
	}

}
