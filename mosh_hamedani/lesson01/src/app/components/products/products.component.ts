import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ProductService } from './../../shared/services/product.service';
import { ShoppingCartService } from './../../shared/services/shopping-cart.service';
import { Product } from './../../shared/models/product.models';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

	products: Product[] = [];
	filteredProducts: Product[] = [];
	category: string;
	cart$: Observable<ShoppingCart>;

	constructor(
		private productService: ProductService,
		private shoppingCartService: ShoppingCartService,
		private route: ActivatedRoute
	) { }

	async ngOnInit() {
		this.cart$ = await this.shoppingCartService.getCart();
		this.cart$.subscribe(x => console.log(x)); //test
		this.populateProducts();
	}

	private populateProducts() {
		this.productService.getAllProducts().snapshotChanges().pipe(
			map(changes =>
				changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
			),
			switchMap(products => {
				console.log(products);
				this.products = products;
				return this.route.queryParamMap;
			})
		).subscribe((params: ParamMap) => {
			this.category = params.get('category');
			this.applyFilter();
		});
	}

	private applyFilter() {
		this.filteredProducts =
			( this.category ? this.products.filter(p => p.category === this.category) : this.products );
	}

}
