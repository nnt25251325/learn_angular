import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCartService } from './../../shared/services/shopping-cart.service';
import { Product } from './../../shared/models/product.models';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Component({
	selector: 'app-product-quantity',
	templateUrl: './product-quantity.component.html',
	styleUrls: ['./product-quantity.component.scss']
})
export class ProductQuantityComponent implements OnInit {

	@Input() product: Product;
	@Input('shopping-cart') shoppingCart: ShoppingCart;

	constructor(
		private shoppingCartService: ShoppingCartService
	) { }

	ngOnInit() {
	}

	addToCart():void {
		this.shoppingCartService.addToCart(this.product);
	}

	removeFromCart():void {
		this.shoppingCartService.removeFromCart(this.product);
	}

}
