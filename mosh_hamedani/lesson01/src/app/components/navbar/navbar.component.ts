import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './../../shared/services/auth.service';
import { ShoppingCartService } from './../../shared/services/shopping-cart.service';
import { AppUser } from './../../shared/models/app-user.models';
import { ShoppingCart } from './../../shared/models/shopping-cart.models';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

	appUser: AppUser;
	cart$: Observable<ShoppingCart>;

	constructor(
		private authService: AuthService,
		private shoppingCartService: ShoppingCartService
	) { }

	async ngOnInit() {
		this.authService.appUser$.subscribe((user: AppUser) => {
			if (user) {
				console.log(user);
				this.appUser = user;
			}
		});

		this.cart$ = await this.shoppingCartService.getCart();
	}

	logout(): void {
		this.authService.logout();
	}

}
