import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

	public colorDefault: string;
	public fontsizeDefault: number;

	constructor() {
		this.init();
	}

	init() {
		this.colorDefault = 'red';
		this.fontsizeDefault = 16;
	}

	getColor(color) {
		this.colorDefault = color;
	}

	getSize(size) {
		this.fontsizeDefault = size;
	}

	getReset() {
		this.init();
	}

}
