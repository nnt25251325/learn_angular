import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-reset',
	templateUrl: './reset.component.html',
	styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

	@Output('outReset') resetConnecter = new EventEmitter<boolean>();

	constructor() { }

	ngOnInit() {
	}

	outResetFn() {
		this.resetConnecter.emit(true);
	}

}
