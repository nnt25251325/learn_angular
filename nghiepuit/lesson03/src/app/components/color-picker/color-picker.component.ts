import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-color-picker',
	templateUrl: './color-picker.component.html',
	styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

	@Output('outColor') colorConnecter = new EventEmitter<string>();

	public colors: string[] = [];

	constructor() { }

	ngOnInit() {
		this.colors = ['red', 'green', 'blue', 'cyan', 'purple', 'orange'];
	}

	outColorFn(color) {
		this.colorConnecter.emit(color);
	}

}
