import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-size-setting',
	templateUrl: './size-setting.component.html',
	styleUrls: ['./size-setting.component.scss']
})
export class SizeSettingComponent implements OnInit {

	@Input('size') size: number;
	@Output('outSize') sizeConnecter = new EventEmitter<number>();

	constructor() { }

	ngOnInit() {
	}

	decSize() {
		if (this.size - 2 >= 8) {
			this.size -= 2;
		}
		this.sizeConnecter.emit(this.size);
	}

	incSize() {
		if (this.size + 2 <= 36) {
			this.size += 2;
		}
		this.sizeConnecter.emit(this.size);
	}

}
