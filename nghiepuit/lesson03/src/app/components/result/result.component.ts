import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-result',
	templateUrl: './result.component.html',
	styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

	@Input('color') color: string;
	@Input('size') size: number;

	constructor() { }

	ngOnInit() {
	}

	setStyles() {
		return {
			'color': this.color,
			'font-size': this.size + 'px',
			'border-color': this.color
		};
	}

}
