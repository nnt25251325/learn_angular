import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from './../../models/task.class';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-task-list',
	templateUrl: './task-list.component.html',
	styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

	@Input('tasks') tasks: Task[];
	@Output('setStatus') setStatusConnector = new EventEmitter<Task>();
	@Output('delete') deleteConnector = new EventEmitter<number>();
	@Output('update') updateConnector = new EventEmitter<Task>();

	public subscription: Subscription;
	public status: number = 0; //0 là all, -1 là false, 1 là true

	constructor(
		public activatedRoute: ActivatedRoute
	) { }

	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe((data: Params) => {
			this.status = data.completed ? (data.completed == 'true' ? 1 : -1) : 0;
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

}
