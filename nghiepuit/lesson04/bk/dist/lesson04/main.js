(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/tasks/tasks.component */ "./src/app/components/tasks/tasks.component.ts");




var routes = [
    {
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full'
    },
    {
        path: 'tasks',
        component: _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_3__["TasksComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header\">\r\n\t<div class=\"g-row\">\r\n\t\t<div class=\"g-col\">\r\n\t\t\t<h1 class=\"header__title\">Todo Angular</h1>\r\n\t\t</div>\r\n\t</div>\r\n</header>\r\n\r\n<main class=\"main\">\r\n\t<router-outlet></router-outlet>\r\n</main>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'lesson04';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/tasks/tasks.component */ "./src/app/components/tasks/tasks.component.ts");
/* harmony import */ var _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/task-list/task-list.component */ "./src/app/components/task-list/task-list.component.ts");
/* harmony import */ var _components_task_item_task_item_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/task-item/task-item.component */ "./src/app/components/task-item/task-item.component.ts");
/* harmony import */ var _components_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/task-form/task-form.component */ "./src/app/components/task-form/task-form.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/task.service */ "./src/app/services/task.service.ts");











//Services

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_7__["TasksComponent"],
                _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_8__["TaskListComponent"],
                _components_task_item_task_item_component__WEBPACK_IMPORTED_MODULE_9__["TaskItemComponent"],
                _components_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_10__["TaskFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]
            ],
            providers: [
                _services_task_service__WEBPACK_IMPORTED_MODULE_11__["TaskService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/task-form/task-form.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1mb3JtL3Rhc2stZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task-form/task-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"task-form\" novalidate=\"\" (submit)=\"onSubmit($event)\">\r\n\t<input\r\n\t\ttype=\"text\"\r\n\t\tname=\"title\"\r\n\t\trequired=\"\"\r\n\t\tplaceholder=\"Enter your task?\"\r\n\t\tclass=\"task-form__input\"\r\n\t\tautocomplete=\"off\"\r\n\t\t[(ngModel)]=\"title\"\r\n\t/>\r\n</form>\r\n<!-- (keypress.enter)=\"onSubmit(title.value)\" -->\r\n"

/***/ }),

/***/ "./src/app/components/task-form/task-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.ts ***!
  \*************************************************************/
/*! exports provided: TaskFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskFormComponent", function() { return TaskFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent() {
        this.title = '';
        this.addTask = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    TaskFormComponent.prototype.ngOnInit = function () {
    };
    TaskFormComponent.prototype.onSubmit = function (e) {
        e.preventDefault();
        this.addTask.emit(this.title);
        this.title = '';
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('addTask'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskFormComponent.prototype, "addTask", void 0);
    TaskFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-form',
            template: __webpack_require__(/*! ./task-form.component.html */ "./src/app/components/task-form/task-form.component.html"),
            styles: [__webpack_require__(/*! ./task-form.component.css */ "./src/app/components/task-form/task-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TaskFormComponent);
    return TaskFormComponent;
}());



/***/ }),

/***/ "./src/app/components/task-item/task-item.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/task-item/task-item.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1pdGVtL3Rhc2staXRlbS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task-item/task-item.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/task-item/task-item.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"task-item\" [class.task-item--completed]=\"task.completed\" [class.task-item--editing]=\"isEditing\">\r\n\t<div class=\"cell\">\r\n\t\t<button\r\n\t\t\taria-label=\"Mark task as completed\"\r\n\t\t\tclass=\"task-item__button\"\r\n\t\t\ttype=\"button\"\r\n\t\t\t(click)=\"setStatusConnector.emit()\"\r\n\t\t\t*ngIf=\"isEditing == false\"\r\n\t\t>\r\n\t\t\t<span class=\"icon fa fa-check\" [class.icon--active]=\"task.completed\"><i class=\"hidden\">done</i></span>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"cell\">\r\n\t\t<div class=\"task-item__title\" *ngIf=\"isEditing == false\">{{ task.title }}</div>\r\n\t\t<form\r\n\t\t\tclass=\"task-form\"\r\n\t\t\tnovalidate=\"\"\r\n\t\t\t*ngIf=\"isEditing == true\"\r\n\t\t\t(ngSubmit)=\"updateConnector.emit()\"\r\n\t\t>\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tname=\"title\"\r\n\t\t\t\tclass=\"task-item__input\"\r\n\t\t\t\tautofocus=\"\"\r\n\t\t\t\tautocomplete=\"off\"\r\n\t\t\t\t[(ngModel)]=\"task.title\"\r\n\t\t\t/>\r\n\t\t</form>\r\n\t</div>\r\n\t<div class=\"cell\">\r\n\t\t<ng-container *ngIf=\"isEditing == true; else stopEditing\">\r\n\t\t\t<button aria-label=\"Cancel editing\" class=\"task-item__button\" type=\"button\" (click)=\"onStopEditing()\">\r\n\t\t\t\t<span class=\"icon fa fa-close\"><i class=\"hidden\">Cancel</i></span>\r\n\t\t\t</button>\r\n\t\t</ng-container>\r\n\t\t<ng-template #stopEditing>\r\n\t\t\t<button\r\n\t\t\t\tclass=\"task-item__button\"\r\n\t\t\t\ttype=\"button\"\r\n\t\t\t\t(click)=\"onEditing()\"\r\n\t\t\t>\r\n\t\t\t\t<span class=\"icon fa fa-pencil\"><i class=\"hidden\">edit</i></span>\r\n\t\t\t</button>\r\n\t\t\t<button\r\n\t\t\t\tclass=\"task-item__button\"\r\n\t\t\t\ttype=\"button\"\r\n\t\t\t\t(click)=\"deleteConnector.emit()\"\r\n\t\t\t>\r\n\t\t\t\t<span class=\"icon fa fa-trash\"><i class=\"hidden\">delete</i></span>\r\n\t\t\t</button>\r\n\t\t</ng-template>\r\n\t</div>\r\n</div>\r\n\r\n<!-- <div class=\"task-item task-item--editing\">\r\n\t<div class=\"cell\">\r\n\t</div>\r\n\t<div class=\"cell\">\r\n\t\t<form class=\"task-form\" novalidate=\"\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tname=\"title\"\r\n\t\t\t\tclass=\"task-item__input\"\r\n\t\t\t\tautofocus=\"\"\r\n\t\t\t\tautocomplete=\"off\"\r\n\t\t\t/>\r\n\t\t</form>\r\n\t</div>\r\n\t<div class=\"cell\">\r\n\t\t<button aria-label=\"Cancel editing\" class=\"task-item__button\" type=\"button\">\r\n\t\t\t<span class=\"icon fa fa-close\"><i class=\"hidden\">Cancel</i></span>\r\n\t\t</button>\r\n\t</div>\r\n</div> -->"

/***/ }),

/***/ "./src/app/components/task-item/task-item.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/task-item/task-item.component.ts ***!
  \*************************************************************/
/*! exports provided: TaskItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskItemComponent", function() { return TaskItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_task_class__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../models/task.class */ "./src/app/models/task.class.ts");



var TaskItemComponent = /** @class */ (function () {
    function TaskItemComponent() {
        this.setStatusConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.deleteConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.updateConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isEditing = false;
    }
    TaskItemComponent.prototype.ngOnInit = function () {
    };
    TaskItemComponent.prototype.onEditing = function () {
        this.isEditing = true;
    };
    TaskItemComponent.prototype.onStopEditing = function () {
        this.isEditing = false;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('task'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_task_class__WEBPACK_IMPORTED_MODULE_2__["Task"])
    ], TaskItemComponent.prototype, "task", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('setStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskItemComponent.prototype, "setStatusConnector", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('delete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskItemComponent.prototype, "deleteConnector", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('update'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskItemComponent.prototype, "updateConnector", void 0);
    TaskItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-item',
            template: __webpack_require__(/*! ./task-item.component.html */ "./src/app/components/task-item/task-item.component.html"),
            styles: [__webpack_require__(/*! ./task-item.component.css */ "./src/app/components/task-item/task-item.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TaskItemComponent);
    return TaskItemComponent;
}());



/***/ }),

/***/ "./src/app/components/task-list/task-list.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1saXN0L3Rhc2stbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task-list/task-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"task-filters\">\r\n\t<li [routerLink]=\"['/tasks']\">\r\n\t\t<a [class.active]=\"status == 0\">View All</a>\r\n\t</li>\r\n\t<li [routerLink]=\"['/tasks', { completed: false }]\">\r\n\t\t<a [class.active]=\"status == -1\">In Progress</a>\r\n\t</li>\r\n\t<li [routerLink]=\"['/tasks', { completed: true }]\">\r\n\t\t<a [class.active]=\"status == 1\">Completed</a>\r\n\t</li>\r\n</ul>\r\n<div class=\"task-list\">\r\n\t<app-task-item\r\n\t\t*ngFor=\"let task of tasks\"\r\n\t\t[task]=\"task\"\r\n\t\t(setStatus)=\"setStatusConnector.emit(task)\"\r\n\t\t(delete)=\"deleteConnector.emit(task.id)\"\r\n\t\t(update)=\"updateConnector.emit(task)\"\r\n\t></app-task-item>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/task-list/task-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.ts ***!
  \*************************************************************/
/*! exports provided: TaskListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListComponent", function() { return TaskListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var TaskListComponent = /** @class */ (function () {
    function TaskListComponent(activatedRoute) {
        this.activatedRoute = activatedRoute;
        this.setStatusConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.deleteConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.updateConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.status = 0; //0 là all, -1 là false, 1 là true
    }
    TaskListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoute.params.subscribe(function (data) {
            _this.status = data.completed ? (data.completed == 'true' ? 1 : -1) : 0;
        });
    };
    TaskListComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('tasks'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TaskListComponent.prototype, "tasks", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('setStatus'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListComponent.prototype, "setStatusConnector", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('delete'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListComponent.prototype, "deleteConnector", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('update'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TaskListComponent.prototype, "updateConnector", void 0);
    TaskListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list',
            template: __webpack_require__(/*! ./task-list.component.html */ "./src/app/components/task-list/task-list.component.html"),
            styles: [__webpack_require__(/*! ./task-list.component.css */ "./src/app/components/task-list/task-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TaskListComponent);
    return TaskListComponent;
}());



/***/ }),

/***/ "./src/app/components/tasks/tasks.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFza3MvdGFza3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tasks/tasks.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"g-row\">\r\n\t<div class=\"g-col\">\r\n\t\t<app-task-form (addTask)=\"addTask($event)\"></app-task-form>\r\n\t</div>\r\n\t<div class=\"g-col\">\r\n\t\t<app-task-list\r\n\t\t\t[tasks]=\"tasks\"\r\n\t\t\t(setStatus)=\"setStatus($event)\"\r\n\t\t\t(delete)=\"onDelete($event)\"\r\n\t\t\t(update)=\"onUpdate($event)\"\r\n\t\t></app-task-list>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/tasks/tasks.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.ts ***!
  \*****************************************************/
/*! exports provided: TasksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksComponent", function() { return TasksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _models_task_class__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/task.class */ "./src/app/models/task.class.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var TasksComponent = /** @class */ (function () {
    function TasksComponent(taskService, activatedRoute) {
        this.taskService = taskService;
        this.activatedRoute = activatedRoute;
        this.tasks = [];
    }
    TasksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.taskService.getAll().subscribe(function (tasks) {
            _this.subscriptionParams = _this.activatedRoute.params.subscribe(function (data) {
                var status = data.completed ? (data.completed == 'true' ? 1 : -1) : 0;
                _this.tasks = tasks.filter(function (data) {
                    if (status == 1) {
                        return data.completed == true;
                    }
                    else if (status == -1) {
                        return data.completed == false;
                    }
                    else {
                        return data;
                    }
                });
            });
        });
    };
    TasksComponent.prototype.addTask = function (title) {
        var _this = this;
        var task = new _models_task_class__WEBPACK_IMPORTED_MODULE_3__["Task"](title);
        this.subscription = this.taskService.add(task).subscribe(function (data) {
            _this.tasks.push(data);
        });
    };
    TasksComponent.prototype.setStatus = function (task) {
        var _this = this;
        task.completed = !task.completed;
        this.subscription = this.taskService.update(task).subscribe(function (data) {
            _this.updateData(data);
        });
    };
    TasksComponent.prototype.updateData = function (data) {
        for (var i = 0; i < this.tasks.length; i++) {
            if (this.tasks[i].id == data.id) {
                this.tasks[i] = data;
                break;
            }
        }
    };
    TasksComponent.prototype.onDelete = function (id) {
        var _this = this;
        this.subscription = this.taskService.delete(id).subscribe(function (data) {
            _this.updateDataAfterDelete(id);
        });
    };
    TasksComponent.prototype.updateDataAfterDelete = function (id) {
        for (var i = 0; i < this.tasks.length; i++) {
            if (this.tasks[i].id == id) {
                this.tasks.splice(i, 1);
                break;
            }
        }
    };
    TasksComponent.prototype.onUpdate = function (task) {
        var _this = this;
        this.subscription = this.taskService.update(task).subscribe(function (data) {
            _this.updateData(data);
        });
    };
    TasksComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscriptionParams) {
            this.subscriptionParams.unsubscribe();
        }
    };
    TasksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tasks',
            template: __webpack_require__(/*! ./tasks.component.html */ "./src/app/components/tasks/tasks.component.html"),
            styles: [__webpack_require__(/*! ./tasks.component.css */ "./src/app/components/tasks/tasks.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], TasksComponent);
    return TasksComponent;
}());



/***/ }),

/***/ "./src/app/models/task.class.ts":
/*!**************************************!*\
  !*** ./src/app/models/task.class.ts ***!
  \**************************************/
/*! exports provided: Task */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task", function() { return Task; });
var Task = /** @class */ (function () {
    function Task(title) {
        this.title = title;
        this.completed = false;
    }
    return Task;
}());



/***/ }),

/***/ "./src/app/services/task.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/task.service.ts ***!
  \******************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var TaskService = /** @class */ (function () {
    function TaskService(http) {
        this.http = http;
        this.API_URL = 'http://localhost:4000/tasks';
    }
    TaskService.prototype.getAll = function () {
        return this.http.get(this.API_URL);
    };
    TaskService.prototype.add = function (task) {
        return this.http.post(this.API_URL, {
            title: task.title,
            completed: task.completed
        });
    };
    TaskService.prototype.update = function (task) {
        return this.http.put(this.API_URL + "/" + task.id, {
            title: task.title,
            completed: task.completed
        });
    };
    TaskService.prototype.delete = function (id) {
        return this.http.delete(this.API_URL + "/" + id);
    };
    TaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\learn_angular\nghiepuit\lesson04\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map