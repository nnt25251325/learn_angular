import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { CourseService } from './../../services/course.service';
import { Course } from './../../models/course.class';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	selector: 'app-course-edit',
	templateUrl: './course-edit.component.html',
	styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public subscriptionParams: Subscription;
	public course: Course;

	constructor(
		public courseService: CourseService,
		public routerService: Router,
		public activatedRouteService: ActivatedRoute,
	) { }

	ngOnInit() {
		this.course = new Course();
		this.loadData();
	}

	loadData() {
		this.subscriptionParams = this.activatedRouteService.params.subscribe((data: Params) => {
			let id = data.id;
			this.subscription = this.courseService.getCourse(id).subscribe((course: Course) => {
				this.course = course;
			})
		});
	}

	onEditCourse() {
		this.subscription = this.courseService.updateCourse(this.course).subscribe((data: Course) => {
			if (data) {
				this.routerService.navigate(['courses']);
			}
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		if (this.subscriptionParams) {
			this.subscriptionParams.unsubscribe();
		}
	}

}
