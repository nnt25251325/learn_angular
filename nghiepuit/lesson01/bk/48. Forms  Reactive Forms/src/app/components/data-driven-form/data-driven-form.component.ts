import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-data-driven-form',
  templateUrl: './data-driven-form.component.html',
  styleUrls: ['./data-driven-form.component.css']
})
export class DataDrivenFormComponent implements OnInit, OnDestroy {

	public frmUser: FormGroup;

  constructor(
  	private _formBuilder: FormBuilder
  ) { }

  ngOnInit() {
  	this.createForm();
  }

  createForm() {
  	this.frmUser = this._formBuilder.group({
  		username: ['', [
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(20)
			]],
  		password: ['', [
				Validators.required,
				Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,}$")
			]],
  		fullname: ['', Validators.required],
  		email: ['', [
				Validators.required,
				Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")
  		]],
  		phone: ['']
  	});

  	this.frmUser.valueChanges.subscribe(data => {
  		console.log(data);
  	});
  }

  onSubmitForm() {
  	console.log(this.frmUser);
  }

  onResetForm() {
  	this.frmUser.reset();
  }

  ngOnDestroy() {
  }

}
