(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/not-found/not-found.component */ "./src/app/components/not-found/not-found.component.ts");
/* harmony import */ var _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/tasks/tasks.component */ "./src/app/components/tasks/tasks.component.ts");
/* harmony import */ var _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/task-list/task-list.component */ "./src/app/components/task-list/task-list.component.ts");
/* harmony import */ var _components_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/task-detail/task-detail.component */ "./src/app/components/task-detail/task-detail.component.ts");
/* harmony import */ var _components_task_action_task_action_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/task-action/task-action.component */ "./src/app/components/task-action/task-action.component.ts");









var routes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
    },
    {
        path: '',
        component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
    },
    {
        path: 'tasks',
        component: _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_6__["TaskListComponent"]
    },
    {
        path: 'taskadd',
        component: _components_task_action_task_action_component__WEBPACK_IMPORTED_MODULE_8__["TaskActionComponent"]
    },
    {
        path: 'task/:id',
        component: _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_5__["TasksComponent"],
        children: [
            {
                path: '',
                component: _components_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_7__["TaskDetailComponent"]
            },
            {
                path: 'edit',
                component: _components_task_action_task_action_component__WEBPACK_IMPORTED_MODULE_8__["TaskActionComponent"]
            }
        ]
    },
    {
        path: '**',
        component: _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/not-found/not-found.component */ "./src/app/components/not-found/not-found.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/tasks/tasks.component */ "./src/app/components/tasks/tasks.component.ts");
/* harmony import */ var _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/task-list/task-list.component */ "./src/app/components/task-list/task-list.component.ts");
/* harmony import */ var _components_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/task-detail/task-detail.component */ "./src/app/components/task-detail/task-detail.component.ts");
/* harmony import */ var _components_task_action_task_action_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/task-action/task-action.component */ "./src/app/components/task-action/task-action.component.ts");
/* harmony import */ var _components_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/task-form/task-form.component */ "./src/app/components/task-form/task-form.component.ts");
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_9__["NotFoundComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_10__["HeaderComponent"],
                _components_tasks_tasks_component__WEBPACK_IMPORTED_MODULE_11__["TasksComponent"],
                _components_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_12__["TaskListComponent"],
                _components_task_detail_task_detail_component__WEBPACK_IMPORTED_MODULE_13__["TaskDetailComponent"],
                _components_task_action_task_action_component__WEBPACK_IMPORTED_MODULE_14__["TaskActionComponent"],
                _components_task_form_task_form_component__WEBPACK_IMPORTED_MODULE_15__["TaskFormComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]
            ],
            providers: [
                _services_task_service__WEBPACK_IMPORTED_MODULE_6__["TaskService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\" role=\"navigation\">\r\n  <div class=\"container\">\r\n    <div class=\"navbar-header\">\r\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">\r\n        <span class=\"sr-only\">Toggle navigation</span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n      </button>\r\n    </div>\r\n    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\r\n      <ul class=\"nav navbar-nav\">\r\n        <li routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\">\r\n          <a [routerLink]=\"['/']\">Trang chủ</a>\r\n        </li>\r\n        <li routerLinkActive=\"active\">\r\n          <a [routerLink]=\"['/tasks']\">Quản lý công việc</a>\r\n        </li>\r\n        <li routerLinkActive=\"active\">\r\n          <a [routerLink]=\"['/taskadd']\">Thêm công việc</a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 class=\"text-center\"><strong>Trang chủ</strong></h2>\r\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/not-found/not-found.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/not-found/not-found.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 class=\"text-center\"><strong>Không tìm thấy trang</strong></h2>"

/***/ }),

/***/ "./src/app/components/not-found/not-found.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.ts ***!
  \*************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/components/not-found/not-found.component.html"),
            styles: [__webpack_require__(/*! ./not-found.component.css */ "./src/app/components/not-found/not-found.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/components/task-action/task-action.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/task-action/task-action.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1hY3Rpb24vdGFzay1hY3Rpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/task-action/task-action.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/task-action/task-action.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-xs-6 col-xs-push-3\">\r\n\t\t\t\t<div class=\"panel panel-primary\">\r\n\t\t\t\t\t<div class=\"panel-heading\">\r\n\t\t\t\t\t\t<h3 class=\"panel-title text-center\">{{ task.id ? 'Sửa Công Việc' : 'Thêm Công Việc' }}</h3>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"panel-body\">\r\n\t\t\t\t\t\t<form (submit)=\"onSave($event)\">\r\n\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<label>Tên:</label>\r\n\t\t\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\t\t\t\t\tname=\"name\"\r\n\t\t\t\t\t\t\t\t\trequired=\"\"\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"task.name\"\r\n\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<label>Trạng Thái:</label>\r\n\t\t\t\t\t\t\t\t<select\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\t\t\tname=\"status\"\r\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"task.status\"\r\n\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t\t<option value=\"true\">Kích Hoạt</option>\r\n\t\t\t\t\t\t\t\t\t<option value=\"false\">Ẩn</option>\r\n\t\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group text-center\">\r\n\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-save mr-5\"></i>Lưu Lại</button>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<hr>\r\n\t\t\t\t<p class=\"text-center\"><button class=\"btn btn-default\" [routerLink]=\"['/tasks']\">Quay lại danh sách</button></p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/task-action/task-action.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/task-action/task-action.component.ts ***!
  \*****************************************************************/
/*! exports provided: TaskActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskActionComponent", function() { return TaskActionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_task_class__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/task.class */ "./src/app/models/task.class.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/task.service */ "./src/app/services/task.service.ts");





var TaskActionComponent = /** @class */ (function () {
    function TaskActionComponent(taskService, routerService, activatedRouteService) {
        this.taskService = taskService;
        this.routerService = routerService;
        this.activatedRouteService = activatedRouteService;
        this.task = new _models_task_class__WEBPACK_IMPORTED_MODULE_3__["Task"]('', false);
    }
    TaskActionComponent.prototype.ngOnInit = function () {
        this.getTask();
    };
    TaskActionComponent.prototype.getTask = function () {
        var _this = this;
        this.subscriptionParams = this.activatedRouteService.parent.params.subscribe(function (data) {
            _this.task.id = data.id;
            if (_this.task.id) {
                _this.subscription = _this.taskService.getTask(_this.task.id).subscribe(function (data) {
                    _this.task = data;
                });
            }
        });
    };
    TaskActionComponent.prototype.onSave = function (e) {
        var _this = this;
        e.preventDefault();
        console.log(this.task);
        if (this.task.id) {
            this.subscription = this.taskService.updateTask(this.task).subscribe(function (data) {
                data ? _this.routerService.navigate(['tasks']) : '';
            });
        }
        else {
            this.subscription = this.taskService.addTask(this.task).subscribe(function (data) {
                data ? _this.routerService.navigate(['tasks']) : '';
            });
        }
    };
    TaskActionComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscriptionParams) {
            this.subscriptionParams.unsubscribe();
        }
    };
    TaskActionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-action',
            template: __webpack_require__(/*! ./task-action.component.html */ "./src/app/components/task-action/task-action.component.html"),
            styles: [__webpack_require__(/*! ./task-action.component.css */ "./src/app/components/task-action/task-action.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_4__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TaskActionComponent);
    return TaskActionComponent;
}());



/***/ }),

/***/ "./src/app/components/task-detail/task-detail.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/task-detail/task-detail.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1kZXRhaWwvdGFzay1kZXRhaWwuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/task-detail/task-detail.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/task-detail/task-detail.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n\t<div class=\"container\">\r\n\t\t<h2 class=\"text-center mb-15\">{{ task.name }}</h2>\r\n\t\t<p class=\"text-center mb-15\">\r\n\t\t\tTrạng thái:\r\n\t\t\t<span\r\n\t\t\t\tclass=\"label label-status\"\r\n\t\t\t\t[class.label-danger]=\"task.status\"\r\n\t\t\t\t[class.label-info]=\"!task.status\"\r\n\t\t\t\t(click)=\"onUpdateStatus(task)\"\r\n\t\t\t>\r\n\t\t\t\t{{ task.status ? 'Kích hoạt' : 'Ẩn' }}\r\n\t\t\t</span>\r\n\t\t</p>\r\n\t\t<hr>\r\n\t\t<p class=\"text-center\"><button class=\"btn btn-default\" [routerLink]=\"['/tasks']\">Quay lại danh sách</button></p>\r\n\t</div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/components/task-detail/task-detail.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/task-detail/task-detail.component.ts ***!
  \*****************************************************************/
/*! exports provided: TaskDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskDetailComponent", function() { return TaskDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_task_class__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/task.class */ "./src/app/models/task.class.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/task.service */ "./src/app/services/task.service.ts");





var TaskDetailComponent = /** @class */ (function () {
    function TaskDetailComponent(taskService, activatedRouteService) {
        this.taskService = taskService;
        this.activatedRouteService = activatedRouteService;
        this.task = new _models_task_class__WEBPACK_IMPORTED_MODULE_3__["Task"]('', false);
    }
    TaskDetailComponent.prototype.ngOnInit = function () {
        this.getTask();
    };
    TaskDetailComponent.prototype.getTask = function () {
        var _this = this;
        this.subscriptionParams = this.activatedRouteService.parent.params.subscribe(function (data) {
            var id = data.id;
            if (id) {
                _this.subscription = _this.taskService.getTask(id).subscribe(function (data) {
                    _this.task = data;
                });
            }
        });
    };
    TaskDetailComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscriptionParams) {
            this.subscriptionParams.unsubscribe();
        }
    };
    TaskDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-detail',
            template: __webpack_require__(/*! ./task-detail.component.html */ "./src/app/components/task-detail/task-detail.component.html"),
            styles: [__webpack_require__(/*! ./task-detail.component.css */ "./src/app/components/task-detail/task-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_4__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TaskDetailComponent);
    return TaskDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/task-form/task-form.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1mb3JtL3Rhc2stZm9ybS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task-form/task-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"box-task-control mb-15\">\r\n\t<div class=\"row\">\r\n\t\t<div class=\"col-xs-6\">\r\n\t\t\t<form class=\"search-wrp\" (submit)=\"onSearch($event)\">\r\n\t\t\t\t<div class=\"input-group\">\r\n\t\t\t\t\t<input\r\n\t\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\t\tname=\"keyword\"\r\n\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\tplaceholder=\"Nhập từ khóa...\"\r\n\t\t\t\t\t\t[(ngModel)]=\"keyword\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t<span class=\"input-group-btn\">\r\n\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-search mr-5\"></i>Tìm</button>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<button\r\n\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\tclass=\"btn btn-default btn-clear\"\r\n\t\t\t\t\t(click)=\"onClear()\"\r\n\t\t\t\t><i class=\"fa fa-close\"></i></button>\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t\t<div class=\"col-xs-6\">\r\n\t\t\t<div class=\"dropdown\">\r\n\t\t\t\t<button class=\"btn btn-primary dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Sắp Xếp <i class=\"fa fa-caret-square-o-down ml-5\"></i></button>\r\n\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t<li (click)=\"onSort('nameasc')\">\r\n\t\t\t\t\t\t<span [class.sort-selected]=\"sort == 'nameasc'\"><span class=\"fa fa-sort-alpha-asc pr-5\">Tên A-Z</span></span>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t<li (click)=\"onSort('namedesc')\">\r\n\t\t\t\t\t\t<span [class.sort-selected]=\"sort == 'namedesc'\"><span class=\"fa fa-sort-alpha-desc pr-5\">Tên Z-A</span></span>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t<li role=\"separator\" class=\"divider\"></li>\r\n\t\t\t\t\t<li (click)=\"onSort('idasc')\">\r\n\t\t\t\t\t\t<span [class.sort-selected]=\"sort == 'idasc'\"><span class=\"fa fa-sort-alpha-asc pr-5\">ID tăng dần</span></span>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t<li (click)=\"onSort('iddesc')\">\r\n\t\t\t\t\t\t<span [class.sort-selected]=\"sort == 'iddesc'\"><span class=\"fa fa-sort-alpha-asc pr-5\">ID giảm dần</span></span>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t</ul>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/task-form/task-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/task-form/task-form.component.ts ***!
  \*************************************************************/
/*! exports provided: TaskFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskFormComponent", function() { return TaskFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var TaskFormComponent = /** @class */ (function () {
    function TaskFormComponent(routerService) {
        this.routerService = routerService;
        this.keyword = '';
        this.sort = '';
    }
    TaskFormComponent.prototype.ngOnInit = function () {
    };
    TaskFormComponent.prototype.onSearch = function (e) {
        e.preventDefault();
        this.routerService.navigate(['tasks'], {
            queryParams: { keyword: this.keyword },
            queryParamsHandling: 'merge'
        });
    };
    TaskFormComponent.prototype.onClear = function () {
        this.routerService.navigate(['tasks'], {
            queryParams: { keyword: '' },
            queryParamsHandling: 'merge'
        });
    };
    TaskFormComponent.prototype.onSort = function (sortBy) {
        this.routerService.navigate(['tasks'], {
            queryParams: { sort: sortBy },
            queryParamsHandling: 'merge'
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('keyword'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TaskFormComponent.prototype, "keyword", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('sort'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], TaskFormComponent.prototype, "sort", void 0);
    TaskFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-form',
            template: __webpack_require__(/*! ./task-form.component.html */ "./src/app/components/task-form/task-form.component.html"),
            styles: [__webpack_require__(/*! ./task-form.component.css */ "./src/app/components/task-form/task-form.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], TaskFormComponent);
    return TaskFormComponent;
}());



/***/ }),

/***/ "./src/app/components/task-list/task-list.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay1saXN0L3Rhc2stbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task-list/task-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\r\n\t<div class=\"container\">\r\n\t\t<button class=\"btn btn-primary mb-15\" [routerLink]=\"['/taskadd']\">Thêm công việc</button>\r\n\t\t<app-task-form [keyword]=\"keyword\" [sort]=\"sort\"></app-task-form>\r\n\t\t<p class=\"mt-15 mb-15\" *ngIf=\"keyword != ''\">Bạn đang tìm kiếm: <strong>{{ keyword }}</strong></p>\r\n\t\t<div class=\"box-tbl\">\r\n\t\t\t<table class=\"table table-bordered table-hover\">\r\n\t\t\t\t<thead>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<th class=\"text-center\">STT</th>\r\n\t\t\t\t\t\t<th class=\"text-center\">ID</th>\r\n\t\t\t\t\t\t<th class=\"text-center\">Tên</th>\r\n\t\t\t\t\t\t<th class=\"text-center\">Trạng Thái</th>\r\n\t\t\t\t\t\t<th class=\"text-center\">Hành Động</th>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</thead>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<!-- <tr>\r\n\t\t\t\t\t\t<td></td>\r\n\t\t\t\t\t\t<td><input type=\"text\" class=\"form-control\" (input)=\"onFilterName($event)\"></td>\r\n\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t<select class=\"form-control\" (change)=\"onFilterStatus($event)\">\r\n\t\t\t\t\t\t\t\t<option value=\"-1\">Tất Cả</option>\r\n\t\t\t\t\t\t\t\t<option value=\"0\">Ẩn</option>\r\n\t\t\t\t\t\t\t\t<option value=\"1\">Kích Hoạt</option>\r\n\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td class=\"text-center\"><button type=\"button\" class=\"btn btn-default btn-clear\">Bỏ lọc</button></td>\r\n\t\t\t\t\t</tr> -->\r\n\t\t\t\t\t<tr *ngFor=\"let task of tasksFilter; let i = index;\">\r\n\t\t\t\t\t\t<td class=\"text-center\">{{ i+1 }}</td>\r\n\t\t\t\t\t\t<td class=\"text-center\">{{ task.id }}</td>\r\n\t\t\t\t\t\t<td><a [routerLink]=\"['/task', task.id]\">{{ task.name }}</a></td>\r\n\t\t\t\t\t\t<td class=\"text-center\">\r\n\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\tclass=\"label label-status\"\r\n\t\t\t\t\t\t\t\t[class.label-danger]=\"task.status\"\r\n\t\t\t\t\t\t\t\t[class.label-info]=\"!task.status\"\r\n\t\t\t\t\t\t\t\t(click)=\"onUpdateStatus(task)\"\r\n\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t{{ task.status ? 'Kích hoạt' : 'Ẩn' }}\r\n\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td class=\"text-center\">\r\n\t\t\t\t\t\t\t<button\r\n\t\t\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-success\"\r\n\t\t\t\t\t\t\t\t[routerLink]=\"['/task', task.id, 'edit']\"\r\n\t\t\t\t\t\t\t><i class=\"fa fa-pencil mr-5\"></i>Sửa</button>\r\n\t\t\t\t\t\t\t&nbsp;\r\n\t\t\t\t\t\t\t<button\r\n\t\t\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-danger\"\r\n\t\t\t\t\t\t\t\t(click)=\"onDelete(task.id)\"\r\n\t\t\t\t\t\t\t><i class=\"fa fa-trash mr-5\"></i>Xóa</button>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t</div>\r\n\t</div>\r\n</section>"

/***/ }),

/***/ "./src/app/components/task-list/task-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/task-list/task-list.component.ts ***!
  \*************************************************************/
/*! exports provided: TaskListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListComponent", function() { return TaskListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var TaskListComponent = /** @class */ (function () {
    function TaskListComponent(taskService, activatedRouteService) {
        this.taskService = taskService;
        this.activatedRouteService = activatedRouteService;
        this.tasks = [];
        this.tasksFilter = [];
        this.keyword = '';
        this.sort = '';
    }
    TaskListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.taskService.getAllTasks().subscribe(function (data) {
            _this.tasks = _this.tasksFilter = data;
            _this.onSearch();
            _this.onSort();
        });
    };
    TaskListComponent.prototype.onUpdateStatus = function (task) {
        var _this = this;
        task.status = !task.status;
        this.subscription = this.taskService.updateTask(task).subscribe(function (data) {
            console.log(data);
            var index = _this.findIndex(_this.tasksFilter, task.id);
            _this.tasksFilter[index] = task;
        });
    };
    TaskListComponent.prototype.onDelete = function (id) {
        var _this = this;
        if (confirm("Bạn chắc chắn muốn xóa?")) {
            this.subscription = this.taskService.deleteTask(id).subscribe(function (data) {
                var index = _this.findIndex(_this.tasksFilter, id);
                _this.tasksFilter.splice(index, 1);
            });
        }
    };
    TaskListComponent.prototype.onSearch = function () {
        var _this = this;
        this.subscriptionParams = this.activatedRouteService.queryParams.subscribe(function (data) {
            data.keyword ? console.log(data) : '';
            _this.keyword = data.keyword ? data.keyword : '';
            _this.tasksFilter = _this.tasks.filter(function (task) {
                return task.name.toLowerCase().indexOf(_this.keyword) != -1;
            });
        });
    };
    TaskListComponent.prototype.onSort = function () {
        var _this = this;
        this.subscriptionParams = this.activatedRouteService.queryParams.subscribe(function (data) {
            data.sort ? console.log(data) : '';
            _this.sort = data.sort ? data.sort : 'nameasc';
            if (_this.sort == 'nameasc' || _this.sort == 'namedesc') {
                var sortValue_1 = (_this.sort == 'nameasc' ? 1 : -1);
                _this.tasksFilter.sort(function (a, b) {
                    if (a.name > b.name)
                        return sortValue_1;
                    else if (a.name < b.name)
                        return -sortValue_1;
                    else
                        return 0;
                });
            }
            else {
                var sortValue_2 = (_this.sort == 'idasc' ? 1 : -1);
                _this.tasksFilter.sort(function (a, b) {
                    if (+a.id > +b.id)
                        return sortValue_2;
                    else if (+a.id < +b.id)
                        return -sortValue_2;
                    else
                        return 0;
                });
            }
        });
    };
    // onFilterName(e) {
    // 	let filterName = e.target.value;
    // 	console.log(filterName);
    // 	this.tasksFilter = this.tasks.filter((task) => {
    // 		return task.name.toLowerCase().indexOf(filterName.toLowerCase()) != -1
    // 	});
    // }
    // onFilterStatus(e) {
    // 	let filterStatus = parseInt(e.target.value);
    // 	console.log(filterStatus);
    // 	this.tasksFilter = this.tasks.filter((task) => {
    // 		if(filterStatus == -1) { // -1 tương ứng Tất cả
    // 			return this.tasks;
    // 		} else { // 0 hoặc 1 tương ứng với Ẩn hoặc Kích hoạt
    // 			return task.status == (filterStatus == 1 ? true : false);
    // 		}
    // 	});
    // }
    TaskListComponent.prototype.findIndex = function (list, id) {
        var result = -1;
        for (var i = 0; i < list.length; i++) {
            if (list[i].id == id) {
                result = i;
                break;
            }
        }
        return result;
    };
    TaskListComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.subscriptionParams) {
            this.subscriptionParams.unsubscribe();
        }
    };
    TaskListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-list',
            template: __webpack_require__(/*! ./task-list.component.html */ "./src/app/components/task-list/task-list.component.html"),
            styles: [__webpack_require__(/*! ./task-list.component.css */ "./src/app/components/task-list/task-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], TaskListComponent);
    return TaskListComponent;
}());



/***/ }),

/***/ "./src/app/components/tasks/tasks.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFza3MvdGFza3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/tasks/tasks.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/components/tasks/tasks.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/tasks/tasks.component.ts ***!
  \*****************************************************/
/*! exports provided: TasksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksComponent", function() { return TasksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TasksComponent = /** @class */ (function () {
    function TasksComponent() {
    }
    TasksComponent.prototype.ngOnInit = function () {
    };
    TasksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tasks',
            template: __webpack_require__(/*! ./tasks.component.html */ "./src/app/components/tasks/tasks.component.html"),
            styles: [__webpack_require__(/*! ./tasks.component.css */ "./src/app/components/tasks/tasks.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TasksComponent);
    return TasksComponent;
}());



/***/ }),

/***/ "./src/app/models/task.class.ts":
/*!**************************************!*\
  !*** ./src/app/models/task.class.ts ***!
  \**************************************/
/*! exports provided: Task */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task", function() { return Task; });
var Task = /** @class */ (function () {
    function Task(name, status) {
        this.name = name;
        this.status = status;
    }
    return Task;
}());



/***/ }),

/***/ "./src/app/services/task.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/task.service.ts ***!
  \******************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var TaskService = /** @class */ (function () {
    //==> login with github tinhnnevolable
    function TaskService(http) {
        this.http = http;
        // public API_URL: string = ' http://localhost:4000/tasks';
        this.API_URL = 'http://5c05d49d6b84ee00137d25e4.mockapi.io/tasks_todo_list_01';
    }
    TaskService.prototype.getAllTasks = function () {
        return this.http.get(this.API_URL);
    };
    TaskService.prototype.getTask = function (id) {
        return this.http.get(this.API_URL + "/" + id);
    };
    TaskService.prototype.addTask = function (task) {
        return this.http.post(this.API_URL, task);
    };
    TaskService.prototype.updateTask = function (task) {
        return this.http.put(this.API_URL + "/" + task.id, task);
    };
    TaskService.prototype.deleteTask = function (id) {
        return this.http.delete(this.API_URL + "/" + id);
    };
    TaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\learn_angular\tinhnn\lesson01\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map