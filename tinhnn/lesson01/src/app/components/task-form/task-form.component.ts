import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-task-form',
	templateUrl: './task-form.component.html',
	styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

	@Input('keyword') keyword: string = '';
	@Input('sort') sort: string = '';

	constructor(
		public routerService: Router
	) { }

	ngOnInit() {
	}

	onSearch(e) {
		e.preventDefault();
		this.routerService.navigate(['tasks'], {
			queryParams: {keyword: this.keyword},
			queryParamsHandling: 'merge'
		});
	}

	onClear() {
		this.routerService.navigate(['tasks'], {
			queryParams: {keyword: ''},
			queryParamsHandling: 'merge'
		});
	}

	onSort(sortBy) {
		this.routerService.navigate(['tasks'], {
			queryParams: {sort: sortBy},
			queryParamsHandling: 'merge'
		});
	}

}
