import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Task } from './../../models/task.class';
import { TaskService } from './../../services/task.service';

@Component({
	selector: 'app-task-action',
	templateUrl: './task-action.component.html',
	styleUrls: ['./task-action.component.css']
})
export class TaskActionComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public subscriptionParams: Subscription;
	public task: Task = new Task('', false);

	constructor(
		public taskService: TaskService,
		public routerService: Router,
		public activatedRouteService: ActivatedRoute
	) { }

	ngOnInit() {
		this.getTask();
	}

	getTask() {
		this.subscriptionParams = this.activatedRouteService.parent.params.subscribe((data: Params) => {
			this.task.id = data.id;
			if (this.task.id) {
				this.subscription = this.taskService.getTask(this.task.id).subscribe((data: Task) => {
					this.task = data;
				});
			}
		});
	}

	onSave(e) {
		e.preventDefault();
		console.log(this.task);
		if (this.task.id) {
			this.subscription = this.taskService.updateTask(this.task).subscribe((data: Task) => {
				data ? this.routerService.navigate(['tasks']) : '';
			});
		} else {
			this.subscription = this.taskService.addTask(this.task).subscribe((data: Task) => {
				data ? this.routerService.navigate(['tasks']) : '';
			});
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		if (this.subscriptionParams) {
			this.subscriptionParams.unsubscribe();
		}
	}

}
