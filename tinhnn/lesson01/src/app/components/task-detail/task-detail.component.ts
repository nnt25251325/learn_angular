import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { Task } from './../../models/task.class';
import { TaskService } from './../../services/task.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public subscriptionParams: Subscription;
	public task: Task = new Task('', false);

  constructor(
  	public taskService: TaskService,
		public activatedRouteService: ActivatedRoute
  ) { }

  ngOnInit() {
		this.getTask();
	}

	getTask() {
		this.subscriptionParams = this.activatedRouteService.parent.params.subscribe((data: Params) => {
			let id = data.id;
			if (id) {
				this.subscription = this.taskService.getTask(id).subscribe((data: Task) => {
					this.task = data;
				});
			}
		});
	}

  ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		if (this.subscriptionParams) {
			this.subscriptionParams.unsubscribe();
		}
	}

}
