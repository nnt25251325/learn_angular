import { Component, OnInit } from '@angular/core';
import { TaskService } from './../../services/task.service';
import { Task } from './../../models/task.class';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	selector: 'app-task-list',
	templateUrl: './task-list.component.html',
	styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

	public subscription: Subscription;
	public subscriptionParams: Subscription;
	public tasks: Task[] = [];
	public tasksFilter: Task[] = [];
	public keyword: string = '';
	public sort: string = '';

	constructor(
		public taskService: TaskService,
		public activatedRouteService: ActivatedRoute
	) { }

	ngOnInit() {
		this.subscription = this.taskService.getAllTasks().subscribe((data: Task[]) => {
			this.tasks = this.tasksFilter = data;
			this.onSearch();
			this.onSort();
		});
	}

	onUpdateStatus(task) {
		task.status = !task.status;
		this.subscription = this.taskService.updateTask(task).subscribe((data: Task) => {
			console.log(data);
			let index = this.findIndex(this.tasksFilter, task.id);
			this.tasksFilter[index] = task;
		});
	}

	onDelete(id: number) {
		if(confirm("Bạn chắc chắn muốn xóa?")) {
			this.subscription = this.taskService.deleteTask(id).subscribe((data: Task) => {
				let index = this.findIndex(this.tasksFilter, id);
				this.tasksFilter.splice(index, 1);
			});
		}
	}

	onSearch() {
		this.subscriptionParams = this.activatedRouteService.queryParams.subscribe((data: Params) => {
			data.keyword ? console.log(data) : '';
			this.keyword = data.keyword ? data.keyword : '';
			this.tasksFilter = this.tasks.filter((task) => {
				return task.name.toLowerCase().indexOf(this.keyword) != -1
			});
		});
	}

	onSort() {
		this.subscriptionParams = this.activatedRouteService.queryParams.subscribe((data: Params) => {
			data.sort ? console.log(data) : '';
			this.sort = data.sort ? data.sort : 'nameasc';
			if(this.sort == 'nameasc' || this.sort == 'namedesc') {
				let sortValue = (this.sort == 'nameasc' ? 1 : -1);
				this.tasksFilter.sort((a, b) => {
					if(a.name > b.name) return sortValue;
					else if(a.name < b.name) return -sortValue;
					else return 0;
				});
			} else {
				let sortValue = (this.sort == 'idasc' ? 1 : -1);
				this.tasksFilter.sort((a, b) => {
					if(+a.id > +b.id) return sortValue;
					else if(+a.id < +b.id) return -sortValue;
					else return 0;
				});
			}
		});
	}

	// onFilterName(e) {
	// 	let filterName = e.target.value;
	// 	console.log(filterName);
	// 	this.tasksFilter = this.tasks.filter((task) => {
	// 		return task.name.toLowerCase().indexOf(filterName.toLowerCase()) != -1
	// 	});
	// }

	// onFilterStatus(e) {
	// 	let filterStatus = parseInt(e.target.value);
	// 	console.log(filterStatus);
	// 	this.tasksFilter = this.tasks.filter((task) => {
	// 		if(filterStatus == -1) { // -1 tương ứng Tất cả
	// 			return this.tasks;
	// 		} else { // 0 hoặc 1 tương ứng với Ẩn hoặc Kích hoạt
	// 			return task.status == (filterStatus == 1 ? true : false);
	// 		}
	// 	});
	// }

	findIndex(list, id) {
		var result = -1;
		for (var i = 0; i < list.length; i++) {
			if (list[i].id == id) {
				result = i;
				break;
			}
		}
		return result;
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		if (this.subscriptionParams) {
			this.subscriptionParams.unsubscribe();
		}
	}

}
