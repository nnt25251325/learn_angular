export class Task {

	public id: number;
	public name: string;
	public status: boolean;

	constructor(name: string, status: boolean) {
		this.name = name;
		this.status = status;
	}

}
