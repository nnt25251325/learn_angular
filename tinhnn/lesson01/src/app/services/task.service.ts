import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from './../models/task.class';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

	// public API_URL: string = ' http://localhost:4000/tasks';
	public API_URL: string = 'http://5c05d49d6b84ee00137d25e4.mockapi.io/tasks_todo_list_01';
	//==> login with github tinhnnevolable

  constructor(
  	public http: HttpClient
  ) { }

  getAllTasks(): Observable<Task[]> {
		return this.http.get<Task[]>(this.API_URL);
	}

	getTask(id: number): Observable<Task> {
		return this.http.get<Task>(`${this.API_URL}/${id}`);
	}

	addTask(task: Task): Observable<Task> {
		return this.http.post<Task>(this.API_URL, task);
	}

	updateTask(task: Task): Observable<Task> {
		return this.http.put<Task>(`${this.API_URL}/${task.id}`, task);
	}

	deleteTask(id: number): Observable<Task> {
		return this.http.delete<Task>(`${this.API_URL}/${id}`);
	}
}
