import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskDetailComponent } from './components/task-detail/task-detail.component';
import { TaskActionComponent } from './components/task-action/task-action.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/',
		pathMatch: 'full'
	},
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'tasks',
		component: TaskListComponent
	},
	{
		path: 'taskadd',
		component: TaskActionComponent
	},
	{
		path: 'task/:id',
		component: TasksComponent,
		children: [
			{
				path: '',
				component: TaskDetailComponent
			},
			{
				path: 'edit',
				component: TaskActionComponent
			}
		]
	},
	{
		path: '**',
		component: NotFoundComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
