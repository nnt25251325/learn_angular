import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Blog } from '../../models/Blog';

@Component({
  selector: 'app-blog-table',
  templateUrl: './blog-table.component.html',
  styleUrls: ['./blog-table.component.css']
})
export class BlogTableComponent implements OnInit {
  @Input() blogList: Blog;
  @Output() delete = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onDelete(blogId: number): void {
    this.delete.emit(blogId);
  }
}
