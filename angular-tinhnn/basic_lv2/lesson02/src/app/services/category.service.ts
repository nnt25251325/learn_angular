import { Injectable } from '@angular/core';

import { API_URL_CATEGORIES } from '../constants/index';
import { Category } from '../models/Category';

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(
    private http: HttpClient
  ) { }

  getCategoryList(): Observable<Category[]> {
    return this.http.get<Category[]>(API_URL_CATEGORIES).pipe(
      tap((data: Category[]) => console.log('CALL SERVICE: getCategoryList', data)),
      catchError((err: Error) => {
        console.log(err);
        return of([]);
      })
    );
  }
}
