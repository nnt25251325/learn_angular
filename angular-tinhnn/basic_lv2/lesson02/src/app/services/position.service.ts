import { Injectable } from '@angular/core';

import { API_URL_POSITIONS } from '../constants/index';
import { Position } from '../models/Position';

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PositionService {
  constructor(
    private http: HttpClient
  ) { }

  getPositionList(): Observable<Position[]> {
    return this.http.get<Position[]>(API_URL_POSITIONS).pipe(
      tap((data: Position[]) => console.log('CALL SERVICE: getPositionList', data)),
      catchError((err: Error) => {
        console.log(err);
        return of([]);
      })
    );
  }
}
