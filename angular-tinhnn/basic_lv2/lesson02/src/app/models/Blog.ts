import { Category } from './Category';

export class Blog {
  id?: number;
  title: string;
  desc: string;
  detail: string;
  shortDetail?: string;
  categoryId: number;
  category?: Category;
  publicStatus: boolean;
  publicAt: number;
  positionIds: number[];
  positionNameList?: string[];
  thumb: string;
  dateModified: number;
}
