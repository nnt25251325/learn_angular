import { Injectable } from '@angular/core';

import { API_URL_BLOGS } from '../constants/index';
import { Blog } from '../models/Blog';
import { Condition } from '../models/Condition';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  constructor(
    private http: HttpClient
  ) { }

  getBlogList(condition: Condition = {}): Observable<Blog[]> {
    const query = this.buildQuery(condition);
    console.log('query:', query);

    return this.http.get<Blog[]>(`${API_URL_BLOGS}/${query}`).pipe(
      tap((data: Blog[]) => console.log('CALL SERVICE: getBlogList', data)),
      catchError((err: Error) => {
        console.log(err);
        return of([]);
      })
    );
  }

  getBlog(id: number): Observable<Blog> {
    return this.http.get<Blog>(`${API_URL_BLOGS}/${id}`).pipe(
      tap((data: Blog) => console.log('CALL SERVICE: getBlog', data)),
      catchError((err: Error) => {
        console.log(err);
        return of(new Blog());
      })
    );
  }

  createBlog(newBlog: Blog): Observable<Blog> {
    return this.http.post<Blog>(API_URL_BLOGS, newBlog, httpOptions).pipe(
      tap((data: Blog) => console.log('CALL SERVICE: createBlog', data)),
      catchError((err: Error) => {
        console.log(err);
        return of(new Blog());
      })
    );
  }

  updateBlog(blog: Blog): Observable<any> {
    return this.http.put(`${API_URL_BLOGS}/${blog.id}`, blog, httpOptions).pipe(
      tap((data: Blog) => console.log('CALL SERVICE: updateBlog', data)),
      catchError((err: Error) => {
        console.log(err);
        return of(new Blog());
      })
    );
  }

  deteleBlog(blogId: number): Observable<Blog> {
    return this.http.delete<Blog>(`${API_URL_BLOGS}/${blogId}`, httpOptions).pipe(
      tap(() => console.log('CALL SERVICE: deteleBlog', blogId)),
      catchError((err: Error) => {
        console.log(err);
        return of(null);
      })
    );
  }

  private buildQuery(condition: Condition): string {
    const defaultParams = '?_sort=dateModified&_order=desc';
    let query = '';

    if (Object.keys(condition).length) {
      const conditionQuery = Object.keys(condition).map((key: string) => {
        if (!Number.isNaN(+condition[key])) {
          return key + '=' + condition[key];
        }

        return key + '_like=' + condition[key];
      });

      return query += defaultParams + '&' + conditionQuery.join('&');
    } else {
      return query = defaultParams;
    }
  }
}
