import { NgModule } from '@angular/core';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { NzCardModule } from 'ng-zorro-antd';

import { WelcomeComponent } from './welcome.component';


@NgModule({
  imports: [
    WelcomeRoutingModule,
    NzCardModule
  ],

  declarations: [
    WelcomeComponent
  ],

  exports: [
    WelcomeComponent
  ]
})
export class WelcomeModule { }
