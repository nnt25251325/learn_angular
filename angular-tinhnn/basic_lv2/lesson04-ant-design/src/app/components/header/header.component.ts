import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isCollapsed: boolean;
  @Output() toggleCollapse = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onToggle(): void {
    this.toggleCollapse.emit();
  }

}
