import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Blog } from '../../models/Blog';
import { Category } from '../../models/Category';
import { Position } from '../../models/Position';

import { CategoryService } from '../../services/category.service';
import { PositionService } from '../../services/position.service';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'app-blog-form',
  templateUrl: './blog-form.component.html',
  styleUrls: ['./blog-form.component.scss']
})
export class BlogFormComponent implements OnInit, OnDestroy {
  subscribeCategoryList: Subscription;
  subscribePositionList: Subscription;
  subscribeParams: Subscription;
  subscribeGetBlog: Subscription;
  subscribeCreateBlog: Subscription;
  subscribeUpdateBlog: Subscription;

  categoryList: { label: string, value: number }[] = [];
  positionList: Position[] = [];
  blogForm: FormGroup;
  id: number;
  isSubmitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    // private messageService: MessageService,
    private categoryService: CategoryService,
    private positionService: PositionService,
    private blogService: BlogService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getCategoryList();
    this.getPositionList();
    this.getBlog();
  }

  createForm() {
    this.blogForm = this.fb.group({
      title: new FormControl(null, [Validators.required, Validators.minLength(10)]),
      desc: new FormControl(null, Validators.required),
      detail: new FormControl(null, [Validators.required]),
      positionIds: new FormControl(null, [Validators.required]),
      thumb: new FormControl(null, [Validators.required]),
      categoryId: new FormControl(null, [Validators.required]),
      publicStatus: new FormControl(false, [Validators.required]),
      publicAt: new FormControl(null, [Validators.required])
    });

    this.blogForm.valueChanges.subscribe(data => {
      // console.log('blogForm 5656565', data);
  	});
  }

  getCategoryList(): void {
    this.subscribeCategoryList = this.categoryService
      .getCategoryList()
      .subscribe((data: Category[]) => {
        if (Array.isArray(data) && data.length) {
          this.categoryList = data.map((item: Category) => {
            return {
              label: item.name,
              value: item.id
            }
          });
        }
      });
  }

  getPositionList(): void {
    this.subscribePositionList = this.positionService
      .getPositionList()
      .subscribe((data: Position[]) => {
        if (Array.isArray(data) && data.length) {
          this.positionList = data
        }
      });
  }

  getBlog(): void {
    this.subscribeParams = this.activatedRoute.params.subscribe((data: Params) => {
      console.log('Router params', data)
      this.id = data.id;

      if (data.id) {
        this.subscribeGetBlog = this.blogService
          .getBlog(data.id)
          .subscribe((data: Blog) => {
            const positionIds = this.positionList.map(item => {
              return {
                label: item.name,
                value: item.id,
                checked: data.positionIds.some(posId => posId === item.id)
              }
            });

            this.blogForm.setValue({
              title: data.title,
              desc: data.desc,
              detail: data.detail,
              thumb: data.thumb,
              categoryId: data.categoryId,
              positionIds,
              publicStatus: data.publicStatus,
              publicAt: new Date(data.publicAt)
            });
          });
      }
    });
  }

  onSubmit(): void {
    console.info('onSubmit', this.blogForm);
    // this.isSubmitted = true;

    for (const i in this.blogForm.controls) {
      this.blogForm.controls[i].markAsDirty();
      this.blogForm.controls[i].updateValueAndValidity();
    }

    if (this.blogForm.invalid) {
      // this.messageService.add({
      //   severity: 'error',
      //   detail: 'There is an unsatisfactory input!'
      // });

      return;
    }

    const blog: Blog = new Blog();

    blog.title = this.blogForm.value.title;
    blog.desc = this.blogForm.value.desc;
    blog.detail = this.blogForm.value.detail;
    blog.categoryId = this.blogForm.value.categoryId;
    blog.publicStatus = this.blogForm.value.publicStatus;
    blog.publicAt = (new Date(this.blogForm.value.publicAt)).getTime();
    blog.positionIds = this.blogForm.value.positionIds;
    blog.thumb = this.blogForm.value.thumb;
    blog.dateModified = Date.now();

    console.log('blog', blog);

    if (this.id) {
      blog.id = this.id;
      this.subscribeCreateBlog = this.blogService
        .updateBlog(blog)
        .subscribe(() => {
          // this.messageService.add({
          //   severity: 'success',
          //   detail: 'The blog was updated successfully!'
          // });
        });
    } else {
      this.subscribeUpdateBlog = this.blogService
        .createBlog(blog)
        .subscribe(data => {
          console.log(111111111, data)
          // this.messageService.add({
          //   severity: 'success',
          //   detail: 'The blog was created successfully!'
          // });
          this.router.navigate(['blogs']);
        });
    }
  }

  resetForm(): void {
    console.log(1111222, this.blogForm.get('publicAt'))

    // this.blogForm.reset();
  }

  ngOnDestroy() {
    if (this.subscribeCategoryList) {
      this.subscribeCategoryList.unsubscribe();
    }
    if (this.subscribePositionList) {
      this.subscribePositionList.unsubscribe();
    }
    if (this.subscribeGetBlog) {
      this.subscribeGetBlog.unsubscribe();
    }
    if (this.subscribeCreateBlog) {
      this.subscribeCreateBlog.unsubscribe();
    }
    if (this.subscribeUpdateBlog) {
      this.subscribeUpdateBlog.unsubscribe();
    }
    if (this.subscribeParams) {
      this.subscribeParams.unsubscribe();
    }
  }
}
