import { NgModule } from '@angular/core';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

import {
  MenuFoldOutline,
  MenuUnfoldOutline,
  DashboardOutline,
  FormOutline,
  SettingOutline,
  UserOutline,
  LineChartOutline,
  LogoutOutline,
  HomeOutline,
  UnorderedListOutline,
  PlusCircleOutline,
  EditOutline,
  DeleteOutline,
  SaveOutline,
  CloseOutline,
  SearchOutline,
  ClearOutline,
  FontColorsOutline,
  ArrowLeftOutline,
} from '@ant-design/icons-angular/icons';

const icons = [
  MenuFoldOutline,
  MenuUnfoldOutline,
  DashboardOutline,
  FormOutline,
  SettingOutline,
  UserOutline,
  LineChartOutline,
  LogoutOutline,
  HomeOutline,
  UnorderedListOutline,
  PlusCircleOutline,
  EditOutline,
  DeleteOutline,
  SaveOutline,
  CloseOutline,
  SearchOutline,
  ClearOutline,
  FontColorsOutline,
  ArrowLeftOutline,
];

@NgModule({
  imports: [NzIconModule],
  exports: [NzIconModule],
  providers: [
    { provide: NZ_ICONS, useValue: icons }
  ]
})
export class IconsProviderModule {
}
