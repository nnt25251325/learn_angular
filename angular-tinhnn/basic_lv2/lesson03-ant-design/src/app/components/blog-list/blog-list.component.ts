import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

import { Blog } from '../../models/Blog';
import { Category } from 'src/app/models/Category';
import { Position } from '../../models/Position';
import { Condition } from '../../models/Condition';

import { NzModalService } from 'ng-zorro-antd';
import { NzNotificationService } from 'ng-zorro-antd';
import { CategoryService } from '../../services/category.service';
import { PositionService } from '../../services/position.service';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit, OnDestroy {
  subscribe: Subscription;
  subscribeDeteleBlog: Subscription;
  categoryList: Category[] = [];
  positionList: Position[] = [];
  blogList: Blog[] = [];
  condition: Condition = {};

  constructor(
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private categoryService: CategoryService,
    private positionService: PositionService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.fetch();
  }

  fetch(): void {
    const values$ = forkJoin(
      this.categoryService.getCategoryList(),
      this.positionService.getPositionList(),
      this.blogService.getBlogList(this.condition)
    ).pipe(
      map(([categoryList, positionList, blogList]) => {
        return {
          categoryList,
          positionList,
          blogList
        }
      })
    );

    this.subscribe = values$.subscribe((data: Object) => {
      this.categoryList = data['categoryList']
      this.positionList = data['positionList']
      this.blogList = data['blogList'].map((item: Blog) => {
        return {
          ...item,
          categoryName: this.getCategoryName(item.categoryId, this.categoryList),
          positionNameList: this.getPositionNameList(item.positionIds, this.positionList)
        };
      });
    });
  }

  getCategoryName(categoryId: number, categoryList: Category[]): string {
    if (!(categoryId && Array.isArray(categoryList) && categoryList.length)) {
      return ''
    }

    const found = categoryList.find(item => item.id === categoryId)

    if (found && found.name) {
      return found.name
    }
  }

  getPositionNameList(positionIds: number[], positionList: Position[]): string[] {
    const positionNameList = [];

    positionIds.forEach(positionId => {
      const found = positionList.find((item: Position) => item.id === +positionId);
      if (found && found.name) {
        positionNameList.push(found);
      }
    });

    return Array.isArray(positionNameList) && positionNameList.length
      ? positionNameList.map(item => item.name)
      : [];
  }

  onConfirmDelete(blogId: number): void {
    this.modalService.confirm({
      nzTitle: '<strong>Delete Confirmation</strong>',
      nzContent: 'Do you want to delete this blog?',
      nzOkText: 'Yes',
      nzCancelText: 'Cancel',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.subscribeDeteleBlog = this.blogService
          .deteleBlog(blogId)
          .subscribe(() => {
            this.blogList = this.blogList.filter(item => item.id !== blogId);

            this.notificationService.create(
              'success',
              'Notification',
              'The blog was deleted successfully!'
            );
          });
      }
    });
  }

  onSearch(condition: Condition): void {
    this.condition = Object.assign({}, condition);
    this.fetch();
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }

    if (this.subscribeDeteleBlog) {
      this.subscribeDeteleBlog.unsubscribe();
    }
  }
}
