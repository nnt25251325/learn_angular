import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  @Input() isCollapsed: boolean;

  constructor() { }

  ngOnInit() {
  }

}
