import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';

/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd';
import { NzCardModule } from 'ng-zorro-antd';
import { NzDropDownModule } from 'ng-zorro-antd';
import { NzTableModule } from 'ng-zorro-antd';
import { NzModalModule } from 'ng-zorro-antd';
import { NzSelectModule } from 'ng-zorro-antd';

import { NzModalService } from 'ng-zorro-antd';
import { NzNotificationService } from 'ng-zorro-antd';
import { BlogService } from './services/blog.service';
import { PositionService } from './services/position.service';
import { CategoryService } from './services/category.service';

import { AppComponent } from './app.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { BlogListComponent } from './components/blog-list/blog-list.component';
import { BlogSearchFormComponent } from './components/blog-search-form/blog-search-form.component';
import { BlogTableComponent } from './components/blog-table/blog-table.component';
import { BlogFormComponent } from './components/blog-form/blog-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    NotFoundComponent,
    BlogListComponent,
    BlogSearchFormComponent,
    BlogTableComponent,
    BlogFormComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzCardModule,
    NzDropDownModule,
    NzTableModule,
    NzModalModule,
    NzSelectModule,
  ],

  providers: [
    { provide: NZ_I18N, useValue: en_US },
    NzModalService,
    NzNotificationService,
    BlogService,
    PositionService,
    CategoryService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
