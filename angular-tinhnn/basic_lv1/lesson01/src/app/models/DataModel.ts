export class User {
  id = 0;
  name = '';
  email = '';
  addresses: Address[];
}

export class Address {
  street = '';
  city = '';
  state = '';
}

export const users: User[] = [
  {
    id: 1,
    name: 'Name 1',
    email: 'a1@a.com',
    addresses: [
      { street: 'Street 1', city: 'City 1', state: 'State 1' },
      { street: 'Street 2', city: 'City 2', state: 'State 2' },
    ]
  },
  {
    id: 2,
    name: 'Name 2',
    email: 'a2@a.com',
    addresses: [
      { street: 'Street 3', city: 'City 3', state: 'State 3' },
    ]
  },
  {
    id: 3,
    name: 'Name 3',
    email: 'a3@a.com',
    addresses: []
  },
];

export const states = ['State 1', 'State 2', 'State 3', 'State 4'];
