import { Injectable } from '@angular/core';
// import { fakeMovies } from '../fake-movies';
import { Movie } from '../models/Movie';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable()
export class MovieService {
  private API_URL = 'http://localhost:4000/movies';

  constructor(
    private http: HttpClient,
    public messageService: MessageService
  ) { }

  getMovies(): Observable<Movie[]> {
    this.messageService.add(`${new Date().toLocaleString()}. Get movie list`);

    return this.http.get<Movie[]>(this.API_URL).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: getMovies', data)),
      catchError(err => {
        console.log(err);
        return of([]);
      })
    );
    // return of(fakeMovies);
  }

  getMovieFromId(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${this.API_URL}/${id}`).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: getMovieFromId', data)),
      catchError(err => {
        console.log(err);
        return of(new Movie());
      })
    );
    // return of(fakeMovies.find(item => item.id === id));
  }

  updateMovie(movie: Movie): Observable<any> {
    return this.http.put(`${this.API_URL}/${movie.id}`, movie, httpOptions).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: updateMovie', data)),
      catchError(err => {
        console.log(err);
        return of(new Movie());
      })
    );
  }

  addMovie(newMovie: Movie): Observable<Movie> {
    return this.http.post<Movie>(this.API_URL, newMovie, httpOptions).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: addMovie', data)),
      catchError(err => {
        console.log(err);
        return of(new Movie());
      })
    );
  }

  deteleMovie(movieId: number): Observable<Movie> {
    return this.http.delete<Movie>(`${this.API_URL}/${movieId}`, httpOptions).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: deteleMovie', movieId)),
      catchError(err => {
        console.log(err);
        return of(null);
      })
    );
  }

  searchMovies(keyword: string): Observable<Movie[]> {
    if (!keyword.trim()) {
      return of([]);
    }

    return this.http.get<Movie[]>(`${this.API_URL}?name_like=${keyword}`).pipe(
      tap(data => console.log('CALL SERVICE SUCCESSFULLY: searchMovies', data)),
      catchError(err => {
        console.log(err);
        return of(null);
      })
    );
  }
}
