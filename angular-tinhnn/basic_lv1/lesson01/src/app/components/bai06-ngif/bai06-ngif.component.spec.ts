import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai06NgifComponent } from './bai06-ngif.component';

describe('Bai06NgifComponent', () => {
  let component: Bai06NgifComponent;
  let fixture: ComponentFixture<Bai06NgifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai06NgifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai06NgifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
