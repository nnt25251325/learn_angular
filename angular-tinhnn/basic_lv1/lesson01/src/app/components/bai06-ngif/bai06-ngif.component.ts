import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { fakeMovies } from '../../fake-movies';

@Component({
  selector: 'app-bai06-ngif',
  templateUrl: './bai06-ngif.component.html',
  styleUrls: ['./bai06-ngif.component.css']
})
export class Bai06NgifComponent implements OnInit {
  movies: Movie[] = fakeMovies;
  selectedMovie: Movie;

  constructor() { }

  ngOnInit() {
  }

  onSelect(movie: Movie): void {
    this.selectedMovie = movie;
    console.log(`selectedMovie = ${JSON.stringify(this.selectedMovie)}`);
  }

}
