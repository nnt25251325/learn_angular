import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-items-search',
  templateUrl: './items-search.component.html',
  styleUrls: ['./items-search.component.css']
})
export class ItemsSearchComponent implements OnInit {
  movies$: Observable<Movie[]>;
  private searchedSubject = new Subject<string>();

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.movies$ = this.searchedSubject.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(
        (keyword: string) => this.movieService.searchMovies(keyword)
      )
    );
  }

  search(keyword: string): void {
    this.searchedSubject.next(keyword);
  }
}
