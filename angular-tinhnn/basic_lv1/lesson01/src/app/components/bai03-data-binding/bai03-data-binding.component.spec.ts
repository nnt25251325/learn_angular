import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai03DataBindingComponent } from './bai03-data-binding.component';

describe('Bai03DataBindingComponent', () => {
  let component: Bai03DataBindingComponent;
  let fixture: ComponentFixture<Bai03DataBindingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai03DataBindingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai03DataBindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
