import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';

@Component({
  selector: 'app-bai03-data-binding',
  templateUrl: './bai03-data-binding.component.html',
  styleUrls: ['./bai03-data-binding.component.css']
})
export class Bai03DataBindingComponent implements OnInit {
  movie: Movie = {
    id: 1,
    name: 'Star',
    releaseYear: 1988
  };

  constructor() { }

  ngOnInit() {
  }

}
