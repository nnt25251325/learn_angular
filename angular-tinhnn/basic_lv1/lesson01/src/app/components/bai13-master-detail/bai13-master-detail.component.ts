import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai13-master-detail',
  templateUrl: './bai13-master-detail.component.html',
  styleUrls: ['./bai13-master-detail.component.css']
})
export class Bai13MasterDetailComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data.slice(0, 5));
  }
}
