import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai16HttpPostComponent } from './bai16-http-post.component';

describe('Bai16HttpPostComponent', () => {
  let component: Bai16HttpPostComponent;
  let fixture: ComponentFixture<Bai16HttpPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai16HttpPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai16HttpPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
