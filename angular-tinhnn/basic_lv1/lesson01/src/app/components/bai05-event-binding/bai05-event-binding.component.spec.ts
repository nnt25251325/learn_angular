import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai05EventBindingComponent } from './bai05-event-binding.component';

describe('Bai05EventBindingComponent', () => {
  let component: Bai05EventBindingComponent;
  let fixture: ComponentFixture<Bai05EventBindingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai05EventBindingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai05EventBindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
