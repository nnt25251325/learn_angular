import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai09RxjsComponent } from './bai09-rxjs.component';

describe('Bai09RxjsComponent', () => {
  let component: Bai09RxjsComponent;
  let fixture: ComponentFixture<Bai09RxjsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai09RxjsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai09RxjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
