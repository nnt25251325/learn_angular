import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai17HttpDeleteComponent } from './bai17-http-delete.component';

describe('Bai17HttpDeleteComponent', () => {
  let component: Bai17HttpDeleteComponent;
  let fixture: ComponentFixture<Bai17HttpDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai17HttpDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai17HttpDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
