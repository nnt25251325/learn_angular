import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai10-service-call-other-service',
  templateUrl: './bai10-service-call-other-service.component.html',
  styleUrls: ['./bai10-service-call-other-service.component.css']
})
export class Bai10ServiceCallOtherServiceComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMoviesFromServices();
  }

  getMoviesFromServices(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data);
  }

  onSelect(movie: Movie): void {
    this.selectedMovie = movie;
    console.log(`selectedMovie = ${JSON.stringify(this.selectedMovie)}`);
  }

}
