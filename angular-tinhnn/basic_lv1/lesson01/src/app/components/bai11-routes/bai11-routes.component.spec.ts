import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai11RoutesComponent } from './bai11-routes.component';

describe('Bai11RoutesComponent', () => {
  let component: Bai11RoutesComponent;
  let fixture: ComponentFixture<Bai11RoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai11RoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai11RoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
