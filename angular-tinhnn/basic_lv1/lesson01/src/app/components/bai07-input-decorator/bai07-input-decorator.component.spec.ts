import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai07InputDecoratorComponent } from './bai07-input-decorator.component';

describe('Bai07InputDecoratorComponent', () => {
  let component: Bai07InputDecoratorComponent;
  let fixture: ComponentFixture<Bai07InputDecoratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai07InputDecoratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai07InputDecoratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
