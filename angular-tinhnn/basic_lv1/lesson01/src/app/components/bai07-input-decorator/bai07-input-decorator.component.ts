import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { fakeMovies } from '../../fake-movies';

@Component({
  selector: 'app-bai07-input-decorator',
  templateUrl: './bai07-input-decorator.component.html',
  styleUrls: ['./bai07-input-decorator.component.css']
})
export class Bai07InputDecoratorComponent implements OnInit {
  movies: Movie[] = fakeMovies;
  selectedMovie: Movie;

  constructor() { }

  ngOnInit() {
  }

  onSelect(movie: Movie): void {
    this.selectedMovie = movie;
    console.log(`selectedMovie = ${JSON.stringify(this.selectedMovie)}`);
  }

}
