import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai08ServiceComponent } from './bai08-service.component';

describe('Bai08ServiceComponent', () => {
  let component: Bai08ServiceComponent;
  let fixture: ComponentFixture<Bai08ServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai08ServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai08ServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
