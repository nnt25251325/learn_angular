import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai19TemplateDrivenFormsComponent } from './bai19-template-driven-forms.component';

describe('Bai19TemplateDrivenFormsComponent', () => {
  let component: Bai19TemplateDrivenFormsComponent;
  let fixture: ComponentFixture<Bai19TemplateDrivenFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai19TemplateDrivenFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai19TemplateDrivenFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
