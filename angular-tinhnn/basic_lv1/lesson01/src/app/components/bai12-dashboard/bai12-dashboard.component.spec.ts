import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai12DashboardComponent } from './bai12-dashboard.component';

describe('Bai12DashboardComponent', () => {
  let component: Bai12DashboardComponent;
  let fixture: ComponentFixture<Bai12DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai12DashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai12DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
