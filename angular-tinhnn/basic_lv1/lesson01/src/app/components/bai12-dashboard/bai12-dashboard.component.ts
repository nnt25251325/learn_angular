import { Component, OnInit } from '@angular/core';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai12-dashboard',
  templateUrl: './bai12-dashboard.component.html',
  styleUrls: ['./bai12-dashboard.component.css']
})
export class Bai12DashboardComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data.slice(0, 5));
  }
}
