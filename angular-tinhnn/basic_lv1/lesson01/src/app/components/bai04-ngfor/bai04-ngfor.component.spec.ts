import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai04NgforComponent } from './bai04-ngfor.component';

describe('Bai04NgforComponent', () => {
  let component: Bai04NgforComponent;
  let fixture: ComponentFixture<Bai04NgforComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bai04NgforComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bai04NgforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
