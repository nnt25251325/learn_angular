import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRouter01Component } from './test-router-01.component';

describe('TestRouter01Component', () => {
  let component: TestRouter01Component;
  let fixture: ComponentFixture<TestRouter01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestRouter01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRouter01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
