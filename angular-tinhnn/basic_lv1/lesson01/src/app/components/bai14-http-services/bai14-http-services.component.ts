import { Component, OnInit } from '@angular/core';
import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-bai14-http-services',
  templateUrl: './bai14-http-services.component.html',
  styleUrls: ['./bai14-http-services.component.css']
})
export class Bai14HttpServicesComponent implements OnInit {
  movies: Movie[];
  selectedMovie: Movie;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMoviesFromServices();
  }

  getMoviesFromServices(): void {
    this.movieService.getMovies().subscribe((data: Movie[]) => this.movies = data);
  }
}
