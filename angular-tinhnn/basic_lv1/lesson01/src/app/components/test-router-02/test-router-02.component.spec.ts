import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRouter02Component } from './test-router-02.component';

describe('TestRouter02Component', () => {
  let component: TestRouter02Component;
  let fixture: ComponentFixture<TestRouter02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestRouter02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRouter02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
