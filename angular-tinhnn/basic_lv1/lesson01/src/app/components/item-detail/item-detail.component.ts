import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Movie } from '../../models/Movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  movie: Movie;

  constructor(
    private activatedRouteService: ActivatedRoute,
    private locationService: Location,
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.getMovieFromRoute();
  }

  getMovieFromRoute(): void {
    const id = +this.activatedRouteService.snapshot.paramMap.get('id');
    console.log(id);
    this.movieService.getMovieFromId(id).subscribe(data => this.movie = data);
  }

  save(): void {
    this.movie.releaseYear = +this.movie.releaseYear;
    console.log('METHOD: save', this.movie);
    this.movieService.updateMovie(this.movie).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.locationService.back();
  }
}
