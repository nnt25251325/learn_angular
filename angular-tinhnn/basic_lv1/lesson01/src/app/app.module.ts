import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MovieService } from './services/movie.service';
import { MessageService } from './services/message.service';

import { AppComponent } from './app.component';
import { Bai01Component } from './components/bai01/bai01.component';
import { Bai02ComponentComponent } from './components/bai02-component/bai02-component.component';
import { Bai03DataBindingComponent } from './components/bai03-data-binding/bai03-data-binding.component';
import { Bai04NgforComponent } from './components/bai04-ngfor/bai04-ngfor.component';
import { Bai05EventBindingComponent } from './components/bai05-event-binding/bai05-event-binding.component';
import { Bai06NgifComponent } from './components/bai06-ngif/bai06-ngif.component';
import { Bai07InputDecoratorComponent } from './components/bai07-input-decorator/bai07-input-decorator.component';
import { Bai08ServiceComponent } from './components/bai08-service/bai08-service.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { Bai09RxjsComponent } from './components/bai09-rxjs/bai09-rxjs.component';
import {
  Bai10ServiceCallOtherServiceComponent
} from './components/bai10-service-call-other-service/bai10-service-call-other-service.component';
import { MessagesComponent } from './components/messages/messages.component';
import { Bai11RoutesComponent } from './components/bai11-routes/bai11-routes.component';
import { TestRouter01Component } from './components/test-router-01/test-router-01.component';
import { TestRouter02Component } from './components/test-router-02/test-router-02.component';
import { Bai12DashboardComponent } from './components/bai12-dashboard/bai12-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Bai13MasterDetailComponent } from './components/bai13-master-detail/bai13-master-detail.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { Bai14HttpServicesComponent } from './components/bai14-http-services/bai14-http-services.component';
import { Bai15HttpPutComponent } from './components/bai15-http-put/bai15-http-put.component';
import { Bai16HttpPostComponent } from './components/bai16-http-post/bai16-http-post.component';
import { Bai17HttpDeleteComponent } from './components/bai17-http-delete/bai17-http-delete.component';
import { Bai18SearchComponent } from './components/bai18-search/bai18-search.component';
import { HeaderComponent } from './components/header/header.component';
import { ItemsSearchComponent } from './components/items-search/items-search.component';
import {
  Bai19TemplateDrivenFormsComponent
} from './components/bai19-template-driven-forms/bai19-template-driven-forms.component';
import { Bai20ReactiveFormsComponent } from './components/bai20-reactive-forms/bai20-reactive-forms.component';

@NgModule({
  declarations: [
    AppComponent,
    Bai01Component,
    Bai02ComponentComponent,
    Bai03DataBindingComponent,
    Bai04NgforComponent,
    Bai05EventBindingComponent,
    Bai06NgifComponent,
    Bai07InputDecoratorComponent,
    Bai08ServiceComponent,
    MovieDetailComponent,
    Bai09RxjsComponent,
    Bai10ServiceCallOtherServiceComponent,
    MessagesComponent,
    Bai11RoutesComponent,
    TestRouter01Component,
    TestRouter02Component,
    Bai12DashboardComponent,
    DashboardComponent,
    Bai13MasterDetailComponent,
    ItemDetailComponent,
    Bai14HttpServicesComponent,
    Bai15HttpPutComponent,
    Bai16HttpPostComponent,
    Bai17HttpDeleteComponent,
    Bai18SearchComponent,
    HeaderComponent,
    ItemsSearchComponent,
    Bai19TemplateDrivenFormsComponent,
    Bai20ReactiveFormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    MovieService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
