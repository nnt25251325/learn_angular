import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Bai01Component } from './components/bai01/bai01.component';
import { Bai02ComponentComponent } from './components/bai02-component/bai02-component.component';
import { Bai03DataBindingComponent } from './components/bai03-data-binding/bai03-data-binding.component';
import { Bai04NgforComponent } from './components/bai04-ngfor/bai04-ngfor.component';
import { Bai05EventBindingComponent } from './components/bai05-event-binding/bai05-event-binding.component';
import { Bai06NgifComponent } from './components/bai06-ngif/bai06-ngif.component';
import { Bai07InputDecoratorComponent } from './components/bai07-input-decorator/bai07-input-decorator.component';
import { Bai08ServiceComponent } from './components/bai08-service/bai08-service.component';
import { Bai09RxjsComponent } from './components/bai09-rxjs/bai09-rxjs.component';
import {
  Bai10ServiceCallOtherServiceComponent
} from './components/bai10-service-call-other-service/bai10-service-call-other-service.component';
import { Bai11RoutesComponent } from './components/bai11-routes/bai11-routes.component';
import { TestRouter01Component } from './components/test-router-01/test-router-01.component';
import { TestRouter02Component } from './components/test-router-02/test-router-02.component';
import { Bai12DashboardComponent } from './components/bai12-dashboard/bai12-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Bai13MasterDetailComponent } from './components/bai13-master-detail/bai13-master-detail.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { Bai14HttpServicesComponent } from './components/bai14-http-services/bai14-http-services.component';
import { Bai15HttpPutComponent } from './components/bai15-http-put/bai15-http-put.component';
import { Bai16HttpPostComponent } from './components/bai16-http-post/bai16-http-post.component';
import { Bai17HttpDeleteComponent } from './components/bai17-http-delete/bai17-http-delete.component';
import { Bai18SearchComponent } from './components/bai18-search/bai18-search.component';
import { Bai19TemplateDrivenFormsComponent } from './components/bai19-template-driven-forms/bai19-template-driven-forms.component';
import { Bai20ReactiveFormsComponent } from './components/bai20-reactive-forms/bai20-reactive-forms.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'bai01',
    component: Bai01Component
  },
  {
    path: 'bai02',
    component: Bai02ComponentComponent
  },
  {
    path: 'bai03',
    component: Bai03DataBindingComponent
  },
  {
    path: 'bai04',
    component: Bai04NgforComponent
  },
  {
    path: 'bai05',
    component: Bai05EventBindingComponent
  },
  {
    path: 'bai06',
    component: Bai06NgifComponent
  },
  {
    path: 'bai07',
    component: Bai07InputDecoratorComponent
  },
  {
    path: 'bai08',
    component: Bai08ServiceComponent
  },
  {
    path: 'bai09',
    component: Bai09RxjsComponent
  },
  {
    path: 'bai10',
    component: Bai10ServiceCallOtherServiceComponent
  },
  {
    path: 'bai11',
    component: Bai11RoutesComponent,
    children: [
      {
        path: 'test-router-01',
        component: TestRouter01Component
      },
      {
        path: 'test-router-02',
        component: TestRouter02Component
      }
    ]
  },
  {
    path: 'bai12',
    component: Bai12DashboardComponent
  },
  {
    path: 'bai13',
    component: Bai13MasterDetailComponent
  },
  {
    path: 'bai13/:id',
    component: ItemDetailComponent
  },
  {
    path: 'bai14',
    component: Bai14HttpServicesComponent
  },
  {
    path: 'bai15',
    component: Bai15HttpPutComponent
  },
  {
    path: 'bai16',
    component: Bai16HttpPostComponent
  },
  {
    path: 'bai17',
    component: Bai17HttpDeleteComponent
  },
  {
    path: 'bai18',
    component: Bai18SearchComponent
  },
  {
    path: 'bai19',
    component: Bai19TemplateDrivenFormsComponent
  },
  {
    path: 'bai20',
    component: Bai20ReactiveFormsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
