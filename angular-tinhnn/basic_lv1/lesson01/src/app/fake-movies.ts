import { Movie } from './models/Movie';

export const fakeMovies: Movie[] = [
  {
    id: 1,
    name: 'Movie 1',
    releaseYear: 1901
  },
  {
    id: 2,
    name: 'Movie 2',
    releaseYear: 1902
  },
  {
    id: 3,
    name: 'Movie 3',
    releaseYear: 1903
  },
  {
    id: 4,
    name: 'Movie 4',
    releaseYear: 1904
  },
  {
    id: 5,
    name: 'Movie 5',
    releaseYear: 1905
  },
  {
    id: 6,
    name: 'Movie 6',
    releaseYear: 1906
  },
  {
    id: 7,
    name: 'Movie 7',
    releaseYear: 1907
  },
  {
    id: 8,
    name: 'Movie 8',
    releaseYear: 1908
  },
  {
    id: 9,
    name: 'Movie 9',
    releaseYear: 1909
  }
]
