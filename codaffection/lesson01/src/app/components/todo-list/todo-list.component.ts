import { Component, OnInit } from '@angular/core';
import { TodoService } from './../../services/todo.service';

@Component({
	selector: 'app-todo-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

	public todoListArray: any[];
	public title: string = '';

	constructor(
		private todoService: TodoService
	) { }

	ngOnInit() {
		this.todoService.getToDoList().snapshotChanges().subscribe(item =>{
			// console.log(item);
			this.todoListArray = [];
			item.forEach(element => {
				var x = element.payload.toJSON();
				x['$key'] = element.key;
				this.todoListArray.push(x);
			});

			//sort array completed false -> true
			this.todoListArray.sort((a, b) => {
				return a.completed - b.completed;
			});
		});
	}

	onAddTodo(e) {
		e.preventDefault();
		if (this.title.trim()) {
			this.todoService.addTodo(this.title);
			this.title = '';
		}
	}

	onUpdateStatus(todo: any) {
		this.todoService.updateTodo(todo.$key, {
			title: todo.title,
			completed: !todo.completed
		});
	}

	onUpdateTitle(todo: any) {
		this.todoService.updateTodo(todo.$key, {
			title: todo.title
		});
	}

	onDeleteTodo($key: string) {
		if (confirm('Are you sure you want to delete?')) {
			this.todoService.deleteTodo($key);
		}
	}

}
