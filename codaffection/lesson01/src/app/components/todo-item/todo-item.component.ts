import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {

	@Input('todo') todo: any;
	@Output('updateStatus') updateStatusConnector = new EventEmitter<any>();
	@Output('updateTitle') updateTitleConnector = new EventEmitter<any>();
	@Output('deleteTodo') deleteTodoConnector = new EventEmitter<any>();

	public isEditing: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
