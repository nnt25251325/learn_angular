import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
	providedIn: 'root'
})

export class TodoService {

	todoList: AngularFireList<any>;

	constructor(
		private firebasedb: AngularFireDatabase
	) { }

	getToDoList() {
		this.todoList = this.firebasedb.list('todolist');
		return this.todoList;
	}

	addTodo(title: string) {
		this.todoList.push({
			title: title,
			completed: false
		});
	}

	updateTodo($key: string, todo: any) {
		this.todoList.update($key, todo);
	}

	deleteTodo($key: string) {
		this.todoList.remove($key);
	}

}
