import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';

import { TodoService } from './services/todo.service';

@NgModule({
	declarations: [
		AppComponent,
		TodoListComponent,
		TodoItemComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		AngularFireModule.initializeApp(environment.firebase),
		AngularFireDatabaseModule
	],
	providers: [
		TodoService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
