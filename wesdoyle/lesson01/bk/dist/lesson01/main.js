(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _guards_secure_inner_pages_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./guards/secure-inner-pages.guard */ "./src/app/guards/secure-inner-pages.guard.ts");
/* harmony import */ var _components_signup_signup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/signup/signup.component */ "./src/app/components/signup/signup.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_chatroom_chatroom_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/chatroom/chatroom.component */ "./src/app/components/chatroom/chatroom.component.ts");
/* harmony import */ var _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/not-found/not-found.component */ "./src/app/components/not-found/not-found.component.ts");









var routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'signup', component: _components_signup_signup_component__WEBPACK_IMPORTED_MODULE_5__["SignupComponent"], canActivate: [_guards_secure_inner_pages_guard__WEBPACK_IMPORTED_MODULE_4__["SecureInnerPagesGuard"]] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"], canActivate: [_guards_secure_inner_pages_guard__WEBPACK_IMPORTED_MODULE_4__["SecureInnerPagesGuard"]] },
    { path: 'chat', component: _components_chatroom_chatroom_component__WEBPACK_IMPORTED_MODULE_7__["ChatroomComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] },
    { path: '**', component: _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_8__["NotFoundComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"app\">\r\n\t<app-header></app-header>\r\n\t<router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#app {\n  min-width: 1000px;\n  margin: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcd2VzZG95bGVcXGxlc3NvbjAxL3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxpQkFBaUI7RUFDakIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2FwcCB7XHJcblx0bWluLXdpZHRoOiAxMDAwcHg7XHJcblx0bWFyZ2luOiBhdXRvO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_signup_signup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/signup/signup.component */ "./src/app/components/signup/signup.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_chatroom_chatroom_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/chatroom/chatroom.component */ "./src/app/components/chatroom/chatroom.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/not-found/not-found.component */ "./src/app/components/not-found/not-found.component.ts");
/* harmony import */ var _components_feed_feed_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/feed/feed.component */ "./src/app/components/feed/feed.component.ts");
/* harmony import */ var _components_message_message_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/message/message.component */ "./src/app/components/message/message.component.ts");
/* harmony import */ var _components_chat_form_chat_form_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/chat-form/chat-form.component */ "./src/app/components/chat-form/chat-form.component.ts");
/* harmony import */ var _components_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/user-list/user-list.component */ "./src/app/components/user-list/user-list.component.ts");
/* harmony import */ var _components_user_item_user_item_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/user-item/user-item.component */ "./src/app/components/user-item/user-item.component.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/chat.service */ "./src/app/services/chat.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _guards_secure_inner_pages_guard__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./guards/secure-inner-pages.guard */ "./src/app/guards/secure-inner-pages.guard.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _components_signup_signup_component__WEBPACK_IMPORTED_MODULE_9__["SignupComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
                _components_chatroom_chatroom_component__WEBPACK_IMPORTED_MODULE_11__["ChatroomComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
                _components_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_13__["NotFoundComponent"],
                _components_feed_feed_component__WEBPACK_IMPORTED_MODULE_14__["FeedComponent"],
                _components_message_message_component__WEBPACK_IMPORTED_MODULE_15__["MessageComponent"],
                _components_chat_form_chat_form_component__WEBPACK_IMPORTED_MODULE_16__["ChatFormComponent"],
                _components_user_list_user_list_component__WEBPACK_IMPORTED_MODULE_17__["UserListComponent"],
                _components_user_item_user_item_component__WEBPACK_IMPORTED_MODULE_18__["UserItemComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_fire_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabaseModule"],
                _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuthModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_4__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_23__["environment"].firebase),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"]
            ],
            providers: [
                _services_chat_service__WEBPACK_IMPORTED_MODULE_19__["ChatService"],
                _services_auth_service__WEBPACK_IMPORTED_MODULE_20__["AuthService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_21__["AuthGuard"],
                _guards_secure_inner_pages_guard__WEBPACK_IMPORTED_MODULE_22__["SecureInnerPagesGuard"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/chat-form/chat-form.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/chat-form/chat-form.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"chat-form-wrapper\">\r\n\t<div class=\"form-chat\">\r\n\t\t<button\r\n\t\t\ttype=\"button\"\r\n\t\t\tclass=\"btn-edit btn btn-sm btn-warning\"\r\n\t\t\t*ngIf=\"isEditing\"\r\n\t\t\t(click)=\"chatMessage = null; message = ''; isEditing = false;\"\r\n\t\t>Cancel Edit</button>\r\n\t\t<div class=\"input-group\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t[ngClass]=\"{ 'is-editing': isEditing }\"\r\n\t\t\t\t[(ngModel)]=\"message\"\r\n\t\t\t\t(keydown)=\"handleSubmit($event)\"\r\n\t\t\t>\r\n\t\t\t<span class=\"input-group-btn\">\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" (click)=\"send()\">SEND</button>\r\n\t\t\t</span>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/chat-form/chat-form.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/chat-form/chat-form.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chat-form-wrapper {\n  height: 70px;\n  background: #f0f3f7;\n  padding: 10px 0; }\n\n.form-chat {\n  position: relative;\n  max-width: 1050px;\n  padding: 0 25px;\n  margin: auto; }\n\n.form-chat .btn-edit {\n    position: absolute;\n    left: 25px;\n    bottom: 115%;\n    padding: 1px 6px;\n    z-index: 10; }\n\n.form-chat .input-group .form-control {\n    height: 50px;\n    font-size: 16px;\n    box-shadow: none;\n    border: 1px solid #09cefe;\n    border-radius: 8px 0 0 8px; }\n\n.form-chat .input-group .form-control.is-editing {\n      background: #e0e0e0; }\n\n.form-chat .input-group .input-group-btn .btn {\n    font-weight: bold;\n    font-size: 16px;\n    border-radius: 0 8px 8px 0;\n    padding: 13px 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGF0LWZvcm0vRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcd2VzZG95bGVcXGxlc3NvbjAxL3NyY1xcYXBwXFxjb21wb25lbnRzXFxjaGF0LWZvcm1cXGNoYXQtZm9ybS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBOztBQUVoQjtFQUNDLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFKYjtJQU1FLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixXQUFXLEVBQUE7O0FBVmI7SUFjRyxZQUFZO0lBQ1osZUFBZTtJQUNmLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsMEJBQTBCLEVBQUE7O0FBbEI3QjtNQW9CSSxtQkFBbUIsRUFBQTs7QUFwQnZCO0lBeUJJLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsMEJBQTBCO0lBQzFCLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jaGF0LWZvcm0vY2hhdC1mb3JtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYXQtZm9ybS13cmFwcGVyIHtcclxuXHRoZWlnaHQ6IDcwcHg7XHJcblx0YmFja2dyb3VuZDogI2YwZjNmNztcclxuXHRwYWRkaW5nOiAxMHB4IDA7XHJcbn1cclxuLmZvcm0tY2hhdCB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdG1heC13aWR0aDogMTA1MHB4O1xyXG5cdHBhZGRpbmc6IDAgMjVweDtcclxuXHRtYXJnaW46IGF1dG87XHJcblx0LmJ0bi1lZGl0IHtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdGxlZnQ6IDI1cHg7XHJcblx0XHRib3R0b206IDExNSU7XHJcblx0XHRwYWRkaW5nOiAxcHggNnB4O1xyXG5cdFx0ei1pbmRleDogMTA7XHJcblx0fVxyXG5cdC5pbnB1dC1ncm91cCB7XHJcblx0XHQuZm9ybS1jb250cm9sIHtcclxuXHRcdFx0aGVpZ2h0OiA1MHB4O1xyXG5cdFx0XHRmb250LXNpemU6IDE2cHg7XHJcblx0XHRcdGJveC1zaGFkb3c6IG5vbmU7XHJcblx0XHRcdGJvcmRlcjogMXB4IHNvbGlkICMwOWNlZmU7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDhweCAwIDAgOHB4O1xyXG5cdFx0XHQmLmlzLWVkaXRpbmcge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6ICNlMGUwZTA7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdC5pbnB1dC1ncm91cC1idG4ge1xyXG5cdFx0XHQuYnRuIHtcclxuXHRcdFx0XHRmb250LXdlaWdodDogYm9sZDtcclxuXHRcdFx0XHRmb250LXNpemU6IDE2cHg7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogMCA4cHggOHB4IDA7XHJcblx0XHRcdFx0cGFkZGluZzogMTNweCAzMHB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/chat-form/chat-form.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/chat-form/chat-form.component.ts ***!
  \*************************************************************/
/*! exports provided: ChatFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatFormComponent", function() { return ChatFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/chat.service */ "./src/app/services/chat.service.ts");



var ChatFormComponent = /** @class */ (function () {
    function ChatFormComponent(chatService) {
        this.chatService = chatService;
        this.message = '';
        this.isEditing = false;
    }
    ChatFormComponent.prototype.ngOnInit = function () {
    };
    ChatFormComponent.prototype.ngOnChanges = function (changes) {
        if (changes.objMessage.currentValue) {
            console.log(changes.objMessage.currentValue);
            this.chatMessage = changes.objMessage.currentValue.messageData;
            this.message = this.chatMessage.message;
            this.isEditing = true;
        }
    };
    ChatFormComponent.prototype.send = function () {
        if (!this.message.trim()) {
            return false;
        }
        ;
        if (!this.chatMessage) {
            this.chatService.sendMessage(this.message);
        }
        else {
            this.chatService.updateMessage(this.chatMessage['key'], this.message);
            this.isEditing = false;
        }
        this.message = '';
    };
    ChatFormComponent.prototype.handleSubmit = function (event) {
        if (event.keyCode === 13)
            this.send();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ChatFormComponent.prototype, "objMessage", void 0);
    ChatFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat-form',
            template: __webpack_require__(/*! ./chat-form.component.html */ "./src/app/components/chat-form/chat-form.component.html"),
            styles: [__webpack_require__(/*! ./chat-form.component.scss */ "./src/app/components/chat-form/chat-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], ChatFormComponent);
    return ChatFormComponent;
}());



/***/ }),

/***/ "./src/app/components/chatroom/chatroom.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/chatroom/chatroom.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"chatroom-page\">\r\n\t<div class=\"user-list-wrapper\">\r\n\t\t<app-user-list></app-user-list>\r\n\t</div>\r\n\t<div class=\"feed-wrapper\">\r\n\t\t<app-feed (updateMessage)=\"objMessage = $event\"></app-feed>\r\n\t\t<app-chat-form [objMessage]=\"objMessage\"></app-chat-form>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/chatroom/chatroom.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/chatroom/chatroom.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chatroom-page:after {\n  display: table;\n  content: '';\n  clear: both; }\n\n.user-list-wrapper {\n  float: left;\n  width: 25%;\n  height: calc(100vh - 70px);\n  font-size: 16px;\n  color: #fff;\n  background: #4c5e70;\n  border-right: 1px solid #222;\n  overflow-y: auto;\n  padding: 25px; }\n\n.feed-wrapper {\n  float: left;\n  width: 75%;\n  height: calc(100vh - 70px);\n  font-size: 16px;\n  background: #f0f3f7; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGF0cm9vbS9EOlxceGFtcHA1NVxcaHRkb2NzXFxsZWFybl9hbmd1bGFyXFx3ZXNkb3lsZVxcbGVzc29uMDEvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNoYXRyb29tXFxjaGF0cm9vbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsV0FBVyxFQUFBOztBQUdiO0VBQ0MsV0FBVztFQUNYLFVBQVU7RUFDViwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsNEJBQTRCO0VBQzVCLGdCQUFnQjtFQUNoQixhQUFhLEVBQUE7O0FBRWQ7RUFDQyxXQUFXO0VBQ1gsVUFBVTtFQUNWLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NoYXRyb29tL2NoYXRyb29tLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYXRyb29tLXBhZ2Uge1xyXG5cdCY6YWZ0ZXIge1xyXG5cdFx0ZGlzcGxheTogdGFibGU7XHJcblx0XHRjb250ZW50OiAnJztcclxuXHRcdGNsZWFyOiBib3RoO1xyXG5cdH1cclxufVxyXG4udXNlci1saXN0LXdyYXBwZXIge1xyXG5cdGZsb2F0OiBsZWZ0O1xyXG5cdHdpZHRoOiAyNSU7XHJcblx0aGVpZ2h0OiBjYWxjKDEwMHZoIC0gNzBweCk7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdGNvbG9yOiAjZmZmO1xyXG5cdGJhY2tncm91bmQ6ICM0YzVlNzA7XHJcblx0Ym9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzIyMjtcclxuXHRvdmVyZmxvdy15OiBhdXRvO1xyXG5cdHBhZGRpbmc6IDI1cHg7XHJcbn1cclxuLmZlZWQtd3JhcHBlciB7XHJcblx0ZmxvYXQ6IGxlZnQ7XHJcblx0d2lkdGg6IDc1JTtcclxuXHRoZWlnaHQ6IGNhbGMoMTAwdmggLSA3MHB4KTtcclxuXHRmb250LXNpemU6IDE2cHg7XHJcblx0YmFja2dyb3VuZDogI2YwZjNmNztcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/chatroom/chatroom.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/chatroom/chatroom.component.ts ***!
  \***********************************************************/
/*! exports provided: ChatroomComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatroomComponent", function() { return ChatroomComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ChatroomComponent = /** @class */ (function () {
    function ChatroomComponent() {
    }
    ChatroomComponent.prototype.ngOnInit = function () { };
    ChatroomComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chatroom',
            template: __webpack_require__(/*! ./chatroom.component.html */ "./src/app/components/chatroom/chatroom.component.html"),
            styles: [__webpack_require__(/*! ./chatroom.component.scss */ "./src/app/components/chatroom/chatroom.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ChatroomComponent);
    return ChatroomComponent;
}());



/***/ }),

/***/ "./src/app/components/feed/feed.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/feed/feed.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"feed-list\" #scroller>\r\n\t<div class=\"messages\">\r\n\t\t<div *ngFor=\"let message of feed | async\" class=\"message\">\r\n\t\t\t<app-message\r\n\t\t\t\t[chatMessage]=\"message\"\r\n\t\t\t\t(updateMessage)=\"onUpdateMessage(message)\"\r\n\t\t\t></app-message>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/feed/feed.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/feed/feed.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".feed-list {\n  height: calc(100vh - 70px - 70px);\n  overflow-y: auto; }\n\n.messages {\n  max-width: 1050px;\n  padding: 30px 25px 0;\n  margin: auto; }\n\n.message {\n  margin: 0 0 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mZWVkL0Q6XFx4YW1wcDU1XFxodGRvY3NcXGxlYXJuX2FuZ3VsYXJcXHdlc2RveWxlXFxsZXNzb24wMS9zcmNcXGFwcFxcY29tcG9uZW50c1xcZmVlZFxcZmVlZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGlDQUFpQztFQUNqQyxnQkFBZ0IsRUFBQTs7QUFFakI7RUFDQyxpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLFlBQVksRUFBQTs7QUFFYjtFQUNDLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9mZWVkL2ZlZWQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmVlZC1saXN0IHtcclxuXHRoZWlnaHQ6IGNhbGMoMTAwdmggLSA3MHB4IC0gNzBweCk7XHJcblx0b3ZlcmZsb3cteTogYXV0bztcclxufVxyXG4ubWVzc2FnZXMge1xyXG5cdG1heC13aWR0aDogMTA1MHB4O1xyXG5cdHBhZGRpbmc6IDMwcHggMjVweCAwO1xyXG5cdG1hcmdpbjogYXV0bztcclxufVxyXG4ubWVzc2FnZSB7XHJcblx0bWFyZ2luOiAwIDAgMjBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/feed/feed.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/feed/feed.component.ts ***!
  \***************************************************/
/*! exports provided: FeedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedComponent", function() { return FeedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/chat.service */ "./src/app/services/chat.service.ts");




var FeedComponent = /** @class */ (function () {
    function FeedComponent(chatService) {
        this.chatService = chatService;
        this.updateMessageConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    FeedComponent.prototype.ngOnInit = function () {
        this.getFeed();
    };
    FeedComponent.prototype.ngOnChanges = function () {
        this.getFeed();
    };
    FeedComponent.prototype.getFeed = function () {
        // this.feed = this.chatService.getMessages().valueChanges().subscribe(data => {
        // 	console.log(data);
        // });
        this.feed = this.chatService.getMessages().snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
            // console.log(changes);
            return changes.map(function (c) {
                // console.log(c.payload.val());
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ key: c.payload.key }, c.payload.val());
            });
        }));
    };
    FeedComponent.prototype.scrollToBottom = function () {
        this.feedContainer.nativeElement.scrollTop = this.feedContainer.nativeElement.scrollHeight;
    };
    FeedComponent.prototype.onUpdateMessage = function (message) {
        var objMessage = {
            messageData: message,
            ranNumber: Math.random()
        };
        this.updateMessageConnector.emit(objMessage);
    };
    FeedComponent.prototype.ngAfterViewChecked = function () {
        this.scrollToBottom();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('scroller'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FeedComponent.prototype, "feedContainer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('updateMessage'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FeedComponent.prototype, "updateMessageConnector", void 0);
    FeedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-feed',
            template: __webpack_require__(/*! ./feed.component.html */ "./src/app/components/feed/feed.component.html"),
            styles: [__webpack_require__(/*! ./feed.component.scss */ "./src/app/components/feed/feed.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"]])
    ], FeedComponent);
    return FeedComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n\t<div class=\"main-nav\">\n\t\t<div class=\"container-fluid\">\n\t\t\t<div class=\"nav-inner\">\n\t\t\t\t<div class=\"heading\"><a routerLink=\"/\">Chat Room</a></div>\n\t\t\t\t<div class=\"links\">\n\t\t\t\t\t<a *ngIf=\"!currentUser\" routerLink=\"/login\">Login</a>\n\t\t\t\t\t<a *ngIf=\"!currentUser\" routerLink=\"/signup\">Sign Up</a>\n\t\t\t\t\t<span *ngIf=\"currentUser\" class=\"user-email\">Hello, {{ currentUser.email }}</span>\n\t\t\t\t\t<a *ngIf=\"currentUser\" (click)=\"logout()\">Logout</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n"

/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header .main-nav {\n  background: #e3eaf1; }\n  header .main-nav .nav-inner {\n    position: relative;\n    height: 70px; }\n  header .main-nav .heading {\n    position: absolute;\n    left: 0;\n    top: 50%;\n    -webkit-transform: translate(0, -50%);\n            transform: translate(0, -50%); }\n  header .main-nav .heading a {\n      font-weight: bold;\n      font-size: 30px;\n      color: #4c5e70;\n      text-decoration: none; }\n  header .main-nav .heading a:hover {\n        color: #09cefe; }\n  header .main-nav .links {\n    position: absolute;\n    right: 0;\n    top: 50%;\n    -webkit-transform: translate(0, -50%);\n            transform: translate(0, -50%); }\n  header .main-nav .links a,\n    header .main-nav .links .user-email {\n      font-weight: bold;\n      font-size: 16px;\n      color: #4c5e70;\n      text-decoration: none;\n      margin-right: 15px; }\n  header .main-nav .links a:hover,\n      header .main-nav .links .user-email:hover {\n        color: #09cefe; }\n  header .main-nav .links .user-email {\n      color: #09cefe; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcd2VzZG95bGVcXGxlc3NvbjAxL3NyY1xcYXBwXFxjb21wb25lbnRzXFxoZWFkZXJcXGhlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVFLG1CQUFtQixFQUFBO0VBRnJCO0lBSUcsa0JBQWtCO0lBQ2xCLFlBQVksRUFBQTtFQUxmO0lBUUcsa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxRQUFRO0lBQ1IscUNBQTZCO1lBQTdCLDZCQUE2QixFQUFBO0VBWGhDO01BYUksaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixjQUFjO01BQ2QscUJBQXFCLEVBQUE7RUFoQnpCO1FBa0JLLGNBQWMsRUFBQTtFQWxCbkI7SUF1Qkcsa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRO0lBQ1IscUNBQTZCO1lBQTdCLDZCQUE2QixFQUFBO0VBMUJoQzs7TUE2QkksaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixjQUFjO01BQ2QscUJBQXFCO01BQ3JCLGtCQUFrQixFQUFBO0VBakN0Qjs7UUFtQ0ssY0FBYyxFQUFBO0VBbkNuQjtNQXVDSSxjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXIge1xyXG5cdC5tYWluLW5hdiB7XHJcblx0XHRiYWNrZ3JvdW5kOiAjZTNlYWYxO1xyXG5cdFx0Lm5hdi1pbm5lciB7XHJcblx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0aGVpZ2h0OiA3MHB4O1xyXG5cdFx0fVxyXG5cdFx0LmhlYWRpbmcge1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdGxlZnQ6IDA7XHJcblx0XHRcdHRvcDogNTAlO1xyXG5cdFx0XHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcclxuXHRcdFx0YSB7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAzMHB4O1xyXG5cdFx0XHRcdGNvbG9yOiAjNGM1ZTcwO1xyXG5cdFx0XHRcdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHRcdFx0XHQmOmhvdmVyIHtcclxuXHRcdFx0XHRcdGNvbG9yOiAjMDljZWZlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0LmxpbmtzIHtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRyaWdodDogMDtcclxuXHRcdFx0dG9wOiA1MCU7XHJcblx0XHRcdHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIC01MCUpO1xyXG5cdFx0XHRhLFxyXG5cdFx0XHQudXNlci1lbWFpbCB7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdFx0XHRcdGNvbG9yOiAjNGM1ZTcwO1xyXG5cdFx0XHRcdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuXHRcdFx0XHRtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcblx0XHRcdFx0Jjpob3ZlciB7XHJcblx0XHRcdFx0XHRjb2xvcjogIzA5Y2VmZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0LnVzZXItZW1haWwge1xyXG5cdFx0XHRcdGNvbG9yOiAjMDljZWZlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(authService) {
        var _this = this;
        this.authService = authService;
        this.authService.getCurrentUser.subscribe(function (user) {
            _this.currentUser = user;
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.logout = function () {
        this.authService.logout();
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-sm-6 col-sm-offset-3\">\r\n\t\t\t\t<form class=\"form\" [formGroup]=\"formLogin\" (ngSubmit)=\"onLogin()\">\r\n\t\t\t\t\t<legend>Log In</legend>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<label>Email</label>\r\n\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\t\t\tname=\"email\"\r\n\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\tformControlName=\"email\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.email.errors }\"\r\n\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.email.errors?.required\" class=\"label label-danger\">Email is required.</span>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.email.errors?.pattern\" class=\"label label-danger\">Email is badly formatted.</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<label>Password</label>\r\n\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\ttype=\"password\"\r\n\t\t\t\t\t\t\tname=\"password\"\r\n\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\tformControlName=\"password\"\r\n\t\t\t\t\t\t/>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-block btn-lg btn-primary\">\r\n\t\t\t\t\t\t\tLog In\r\n\t\t\t\t\t\t\t&nbsp;<img *ngIf=\"isLoading\" src=\"data:image/gif;base64,R0lGODlhFAAUAPAAAP////7+/iH5BAkKAAAAIf4aQ3JlYXRlZCB3aXRoIGFqYXhsb2FkLmluZm8AIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAFAAUAAACJoSPqcudAZ2CUSIaJGX1dc9Z4kgeH5eZqXOW7ttsS2uss0hrNpwUACH5BAkKAAAALAAAAAAUABQAAAImhI+py50RHINRvgrgbFR1/1lHKJamJqGGymHmC5ckwtKO28Z1nBQAIfkECQoAAAAsAAAAABQAFAAAAiaEj6nL7Y9CgErSySSOdmnKgeLzGV2jbWdWiusIs1vyHnN13XnsFAAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAStLJbNWb+g8aHDBmXLlIWMiy6rZGb9XMDgrhrVEAACH5BAkKAAAALAAAAAAUABQAAAInhI+py+2PQoBK0sls1Zv6D2aYwwGlGKaqcbKtO0bRKMVLbT94WC8FACH5BAkKAAAALAAAAAAUABQAAAIlhI+py+2PQoBK0sls1Zv6D2aYwwGlGKbkCUljwpqx8TZueINzAQAh+QQJCgAAACwAAAAAFAAUAAACJ4SPqcvtj0KAStLJbNWb+n9I2CeO2Vg+HLCCriG68dLCdXiZ3v0eBQAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAYB4JpVWYd79o1NhspPh8qPOR7juuSRuxFH2bsFIAADs=\" />\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group text-right\">\r\n\t\t\t\t\t\t<a routerLink=\"/signup\">No account? <strong>Create one here</strong></a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<p class=\"alert alert-danger\" *ngIf=\"errorMsg\">Error: {{ errorMsg }}</p>\r\n\t\t\t\t\t<p>\r\n\t\t\t\t\t\t<strong>Note</strong>: You can create a new account or use this account: <br/>\r\n\t\t\t\t\t\tEmail: <i>mark@test.com</i> <br/>\r\n\t\t\t\t\t\tPass: <i>123456</i>\r\n\t\t\t\t\t</p>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page {\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  min-height: calc(100vh - 70px);\n  background: #4c5e70;\n  padding: 50px 0; }\n  .login-page .form {\n    background: #fff;\n    padding: 25px; }\n  .login-page .form legend {\n      font-weight: bold;\n      font-size: 28px;\n      color: #4c5e70;\n      border: 0;\n      text-align: center; }\n  .login-page .form label {\n      color: #4c5e70; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9EOlxceGFtcHA1NVxcaHRkb2NzXFxsZWFybl9hbmd1bGFyXFx3ZXNkb3lsZVxcbGVzc29uMDEvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGxvZ2luXFxsb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGFBQWE7RUFDYixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0VBTmhCO0lBUUUsZ0JBQWdCO0lBQ2hCLGFBQWEsRUFBQTtFQVRmO01BV0csaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixjQUFjO01BQ2QsU0FBUztNQUNULGtCQUFrQixFQUFBO0VBZnJCO01Ba0JHLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9naW4tcGFnZSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gNzBweCk7XHJcblx0YmFja2dyb3VuZDogIzRjNWU3MDtcclxuXHRwYWRkaW5nOiA1MHB4IDA7XHJcblx0LmZvcm0ge1xyXG5cdFx0YmFja2dyb3VuZDogI2ZmZjtcclxuXHRcdHBhZGRpbmc6IDI1cHg7XHJcblx0XHRsZWdlbmQge1xyXG5cdFx0XHRmb250LXdlaWdodDogYm9sZDtcclxuXHRcdFx0Zm9udC1zaXplOiAyOHB4O1xyXG5cdFx0XHRjb2xvcjogIzRjNWU3MDtcclxuXHRcdFx0Ym9yZGVyOiAwO1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHR9XHJcblx0XHRsYWJlbCB7XHJcblx0XHRcdGNvbG9yOiAjNGM1ZTcwO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, formBuilder) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.isSubmitted = false;
        this.isLoading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.formLogin.valueChanges.subscribe(function (data) {
            _this.email = data.email;
            _this.password = data.password;
        });
        // reset login status
        this.authService.logout();
    };
    LoginComponent.prototype.createForm = function () {
        this.formLogin = this.formBuilder.group({
            email: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{2,}")
                ]],
            password: ['']
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () {
            return this.formLogin.controls;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        this.isSubmitted = true;
        if (this.formLogin.invalid)
            return false;
        this.isLoading = true;
        this.authService.login(this.email, this.password)
            .catch(function (error) {
            console.log(error);
            _this.errorMsg = error.message;
            _this.isLoading = false;
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/message/message.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/message/message.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"message-inner\" [ngClass]=\"{'is-own-message': isOwnMessage}\">\r\n\t<div class=\"message-head\">\r\n\t\t<span class=\"sender\">{{ chatMessage.userName }}</span>,&nbsp;\r\n\t\t<span class=\"timestamp\">{{ chatMessage.timeSent | date: 'medium' }}</span>\r\n\t</div>\r\n\t<div class=\"message-content\">\r\n\t\t<div class=\"txt\">{{ chatMessage.message }}</div>\r\n\t\t<div class=\"box-action\" *ngIf=\"isOwnMessage\">\r\n\t\t\t<a class=\"text-info\" (click)=\"updateMessageConnector.emit()\">Edit</a>\r\n\t\t\t&nbsp;|&nbsp;\r\n\t\t\t<a class=\"text-danger\" (click)=\"onDeleteMessage(chatMessage.key)\">Delete</a>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/message/message.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/message/message.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".message-inner {\n  max-width: 90%; }\n  .message-inner.is-own-message {\n    text-align: right;\n    margin-right: 0;\n    margin-left: auto; }\n  .message-inner.is-own-message .message-content {\n      background: #cff1ff;\n      text-align: left; }\n  .message-inner .message-head .sender,\n  .message-inner .message-head .timestamp {\n    font-size: 12px;\n    font-style: normal;\n    color: #aaa; }\n  .message-inner .message-head .sender {\n    font-weight: bold;\n    font-size: 14px;\n    color: #09cefe; }\n  .message-inner .message-content {\n    display: inline-block;\n    vertical-align: middle;\n    background: #e3eaf1;\n    border-radius: 8px;\n    padding: 10px; }\n  .message-inner .message-content .box-action {\n      text-align: right; }\n  .message-inner .message-content .box-action a {\n        font-size: 11px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tZXNzYWdlL0Q6XFx4YW1wcDU1XFxodGRvY3NcXGxlYXJuX2FuZ3VsYXJcXHdlc2RveWxlXFxsZXNzb24wMS9zcmNcXGFwcFxcY29tcG9uZW50c1xcbWVzc2FnZVxcbWVzc2FnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGNBQWMsRUFBQTtFQURmO0lBR0UsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixpQkFBaUIsRUFBQTtFQUxuQjtNQU9HLG1CQUFtQjtNQUNuQixnQkFBZ0IsRUFBQTtFQVJuQjs7SUFjRyxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLFdBQVcsRUFBQTtFQWhCZDtJQW1CRyxpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGNBQWMsRUFBQTtFQXJCakI7SUF5QkUscUJBQXFCO0lBQ3JCLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGFBQWEsRUFBQTtFQTdCZjtNQStCRyxpQkFBaUIsRUFBQTtFQS9CcEI7UUFpQ0ksZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tZXNzYWdlL21lc3NhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWVzc2FnZS1pbm5lciB7XHJcblx0bWF4LXdpZHRoOiA5MCU7XHJcblx0Ji5pcy1vd24tbWVzc2FnZSB7XHJcblx0XHR0ZXh0LWFsaWduOiByaWdodDtcclxuXHRcdG1hcmdpbi1yaWdodDogMDtcclxuXHRcdG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG5cdFx0Lm1lc3NhZ2UtY29udGVudCB7XHJcblx0XHRcdGJhY2tncm91bmQ6ICNjZmYxZmY7XHJcblx0XHRcdHRleHQtYWxpZ246IGxlZnQ7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5tZXNzYWdlLWhlYWQge1xyXG5cdFx0LnNlbmRlcixcclxuXHRcdC50aW1lc3RhbXAge1xyXG5cdFx0XHRmb250LXNpemU6IDEycHg7XHJcblx0XHRcdGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuXHRcdFx0Y29sb3I6ICNhYWE7XHJcblx0XHR9XHJcblx0XHQuc2VuZGVyIHtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0XHRcdGZvbnQtc2l6ZTogMTRweDtcclxuXHRcdFx0Y29sb3I6ICMwOWNlZmU7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5tZXNzYWdlLWNvbnRlbnQge1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0dmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuXHRcdGJhY2tncm91bmQ6ICNlM2VhZjE7XHJcblx0XHRib3JkZXItcmFkaXVzOiA4cHg7XHJcblx0XHRwYWRkaW5nOiAxMHB4O1xyXG5cdFx0LmJveC1hY3Rpb24ge1xyXG5cdFx0XHR0ZXh0LWFsaWduOiByaWdodDtcclxuXHRcdFx0YSB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/message/message.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/message/message.component.ts ***!
  \*********************************************************/
/*! exports provided: MessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageComponent", function() { return MessageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_chat_message_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../models/chat-message.model */ "./src/app/models/chat-message.model.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/chat.service */ "./src/app/services/chat.service.ts");





var MessageComponent = /** @class */ (function () {
    function MessageComponent(authService, chatService) {
        this.authService = authService;
        this.chatService = chatService;
        this.updateMessageConnector = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    MessageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getCurrentUser.subscribe(function (user) {
            if (user) {
                _this.ownEmail = user.email;
                _this.isOwnMessage = (_this.ownEmail === _this.chatMessage.email);
            }
        });
    };
    MessageComponent.prototype.onDeleteMessage = function (key) {
        if (confirm('Are you sure you want to delete this message?')) {
            this.chatService.deteleMessage(key);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_chat_message_model__WEBPACK_IMPORTED_MODULE_2__["ChatMessage"])
    ], MessageComponent.prototype, "chatMessage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])('updateMessage'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MessageComponent.prototype, "updateMessageConnector", void 0);
    MessageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-message',
            template: __webpack_require__(/*! ./message.component.html */ "./src/app/components/message/message.component.html"),
            styles: [__webpack_require__(/*! ./message.component.scss */ "./src/app/components/message/message.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_4__["ChatService"]])
    ], MessageComponent);
    return MessageComponent;
}());



/***/ }),

/***/ "./src/app/components/not-found/not-found.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"text-center\"><strong>404 - Page Not Found</strong></h1>\n<hr>\n<p class=\"text-center\"><button type=\"button\" class=\"btn btn-primary\" routerLink=\"/\">Back To Home</button></p>\n"

/***/ }),

/***/ "./src/app/components/not-found/not-found.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/not-found/not-found.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/not-found/not-found.component.ts ***!
  \*************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/components/not-found/not-found.component.html"),
            styles: [__webpack_require__(/*! ./not-found.component.scss */ "./src/app/components/not-found/not-found.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/components/signup/signup.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/signup/signup.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"signup-page\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"row\">\r\n\t\t\t<div class=\"col-sm-6 col-sm-offset-3\">\r\n\t\t\t\t<form class=\"form\" [formGroup]=\"formSignUp\" (ngSubmit)=\"onSignUp()\">\r\n\t\t\t\t\t<legend>Sign Up</legend>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<label>Email</label>\r\n\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\t\t\tname=\"email\"\r\n\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\tplaceholder=\"Enter your email address\"\r\n\t\t\t\t\t\t\tformControlName=\"email\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.email.errors }\"\r\n\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.email.errors?.required\" class=\"label label-danger\">Email is require.</span>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.email.errors?.pattern\" class=\"label label-danger\">Email is badly formatted</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<label>Password</label>\r\n\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\ttype=\"password\"\r\n\t\t\t\t\t\t\tname=\"password\"\r\n\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\tplaceholder=\"Choose a password\"\r\n\t\t\t\t\t\t\tformControlName=\"password\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.password.errors }\"\r\n\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.password.errors?.required\" class=\"label label-danger\">Password is required</span>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.password.errors?.minlength\" class=\"label label-danger\">Password should be at least 6 characters</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<label>Display name</label>\r\n\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\ttype=\"text\"\r\n\t\t\t\t\t\t\tname=\"displayName\"\r\n\t\t\t\t\t\t\tplaceholder=\"Enter a display name\"\r\n\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\tformControlName=\"displayName\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{ 'is-invalid': isSubmitted && f.displayName.errors }\"\r\n\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t<span *ngIf=\"isSubmitted && f.displayName.errors?.required\" class=\"label label-danger\">Display name is required</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-block btn-lg btn-primary\">\r\n\t\t\t\t\t\t\tSign Up\r\n\t\t\t\t\t\t\t&nbsp;<img *ngIf=\"isLoading\" src=\"data:image/gif;base64,R0lGODlhFAAUAPAAAP////7+/iH5BAkKAAAAIf4aQ3JlYXRlZCB3aXRoIGFqYXhsb2FkLmluZm8AIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAFAAUAAACJoSPqcudAZ2CUSIaJGX1dc9Z4kgeH5eZqXOW7ttsS2uss0hrNpwUACH5BAkKAAAALAAAAAAUABQAAAImhI+py50RHINRvgrgbFR1/1lHKJamJqGGymHmC5ckwtKO28Z1nBQAIfkECQoAAAAsAAAAABQAFAAAAiaEj6nL7Y9CgErSySSOdmnKgeLzGV2jbWdWiusIs1vyHnN13XnsFAAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAStLJbNWb+g8aHDBmXLlIWMiy6rZGb9XMDgrhrVEAACH5BAkKAAAALAAAAAAUABQAAAInhI+py+2PQoBK0sls1Zv6D2aYwwGlGKaqcbKtO0bRKMVLbT94WC8FACH5BAkKAAAALAAAAAAUABQAAAIlhI+py+2PQoBK0sls1Zv6D2aYwwGlGKbkCUljwpqx8TZueINzAQAh+QQJCgAAACwAAAAAFAAUAAACJ4SPqcvtj0KAStLJbNWb+n9I2CeO2Vg+HLCCriG68dLCdXiZ3v0eBQAh+QQJCgAAACwAAAAAFAAUAAACJoSPqcvtj0KAYB4JpVWYd79o1NhspPh8qPOR7juuSRuxFH2bsFIAADs=\" />\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<p class=\"alert alert-danger\" *ngIf=\"errorMsg\">Error: {{ errorMsg }}</p>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/signup/signup.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/signup/signup.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".signup-page {\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  min-height: calc(100vh - 70px);\n  background: #4c5e70;\n  padding: 50px 0; }\n  .signup-page .form {\n    background: #fff;\n    padding: 25px; }\n  .signup-page .form legend {\n      font-weight: bold;\n      font-size: 28px;\n      color: #4c5e70;\n      border: 0;\n      text-align: center; }\n  .signup-page .form label {\n      color: #4c5e70; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zaWdudXAvRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcd2VzZG95bGVcXGxlc3NvbjAxL3NyY1xcYXBwXFxjb21wb25lbnRzXFxzaWdudXBcXHNpZ251cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGFBQWE7RUFDYixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0VBTmhCO0lBUUUsZ0JBQWdCO0lBQ2hCLGFBQWEsRUFBQTtFQVRmO01BV0csaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixjQUFjO01BQ2QsU0FBUztNQUNULGtCQUFrQixFQUFBO0VBZnJCO01Ba0JHLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWdudXAtcGFnZSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gNzBweCk7XHJcblx0YmFja2dyb3VuZDogIzRjNWU3MDtcclxuXHRwYWRkaW5nOiA1MHB4IDA7XHJcblx0LmZvcm0ge1xyXG5cdFx0YmFja2dyb3VuZDogI2ZmZjtcclxuXHRcdHBhZGRpbmc6IDI1cHg7XHJcblx0XHRsZWdlbmQge1xyXG5cdFx0XHRmb250LXdlaWdodDogYm9sZDtcclxuXHRcdFx0Zm9udC1zaXplOiAyOHB4O1xyXG5cdFx0XHRjb2xvcjogIzRjNWU3MDtcclxuXHRcdFx0Ym9yZGVyOiAwO1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHR9XHJcblx0XHRsYWJlbCB7XHJcblx0XHRcdGNvbG9yOiAjNGM1ZTcwO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/signup/signup.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/signup/signup.component.ts ***!
  \*******************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");




var SignupComponent = /** @class */ (function () {
    function SignupComponent(authService, formBuilder) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.isSubmitted = false;
        this.isLoading = false;
    }
    SignupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.subscription = this.formSignUp.valueChanges.subscribe(function (data) {
            _this.email = data.email;
            _this.password = data.password;
            _this.displayName = data.displayName;
        });
    };
    SignupComponent.prototype.createForm = function () {
        this.formSignUp = this.formBuilder.group({
            email: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{2,}")
                ]],
            password: ['', [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)
                ]],
            displayName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    Object.defineProperty(SignupComponent.prototype, "f", {
        get: function () {
            return this.formSignUp.controls;
        },
        enumerable: true,
        configurable: true
    });
    SignupComponent.prototype.onSignUp = function () {
        var _this = this;
        this.isSubmitted = true;
        if (this.formSignUp.invalid)
            return false;
        this.isLoading = true;
        this.authService.signup(this.email, this.password, this.displayName)
            .catch(function (error) {
            console.log(error);
            _this.errorMsg = error.message;
            _this.isLoading = false;
        });
    };
    SignupComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/components/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/components/signup/signup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/components/user-item/user-item.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/user-item/user-item.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"user-item\">\r\n\t<span class=\"status\" [ngClass]=(user.status)></span>\r\n\t<span class=\"user-name\">{{ user.displayName }}</span>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/user-item/user-item.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/user-item/user-item.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user-item {\n  height: auto;\n  padding: 10px;\n  margin-top: 10px;\n  border-radius: 1px;\n  align-items: flex-start;\n  color: #fff;\n  background: #58718c;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.23);\n  border-radius: 8px;\n  transition: ease-in 0.2s; }\n  .user-item .online {\n    background-color: #0FA; }\n  .user-item .busy {\n    background-color: #FB0; }\n  .user-item .offline {\n    background-color: #888; }\n  .user-item .status {\n    display: inline-block;\n    vertical-align: middle;\n    min-width: 10px;\n    min-height: 10px;\n    border-radius: 50%;\n    margin-right: 10px; }\n  .user-item .user-name {\n    display: inline-block;\n    vertical-align: middle; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2VyLWl0ZW0vRDpcXHhhbXBwNTVcXGh0ZG9jc1xcbGVhcm5fYW5ndWxhclxcd2VzZG95bGVcXGxlc3NvbjAxL3NyY1xcYXBwXFxjb21wb25lbnRzXFx1c2VyLWl0ZW1cXHVzZXItaXRlbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQVk7RUFDWixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQix1QkFBc0I7RUFDdEIsV0FBVztFQUNYLG1CQUFtQjtFQUNuQix5Q0FBeUM7RUFDekMsa0JBQWtCO0VBQ2xCLHdCQUF3QixFQUFBO0VBVnpCO0lBWUUsc0JBQXNCLEVBQUE7RUFaeEI7SUFlRSxzQkFBc0IsRUFBQTtFQWZ4QjtJQWtCRSxzQkFBc0IsRUFBQTtFQWxCeEI7SUFxQkUscUJBQXFCO0lBQ3JCLHNCQUFzQjtJQUN0QixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixrQkFBa0IsRUFBQTtFQTFCcEI7SUE2QkUscUJBQXFCO0lBQ3JCLHNCQUFzQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy91c2VyLWl0ZW0vdXNlci1pdGVtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaXRlbXtcclxuXHRoZWlnaHQ6IGF1dG87XHJcblx0cGFkZGluZzogMTBweDtcclxuXHRtYXJnaW4tdG9wOiAxMHB4O1xyXG5cdGJvcmRlci1yYWRpdXM6IDFweDtcclxuXHRhbGlnbi1pdGVtczpmbGV4LXN0YXJ0O1xyXG5cdGNvbG9yOiAjZmZmO1xyXG5cdGJhY2tncm91bmQ6ICM1ODcxOGM7XHJcblx0Ym94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4yMyk7XHJcblx0Ym9yZGVyLXJhZGl1czogOHB4O1xyXG5cdHRyYW5zaXRpb246IGVhc2UtaW4gMC4ycztcclxuXHQub25saW5le1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogIzBGQTtcclxuXHR9XHJcblx0LmJ1c3l7XHJcblx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjRkIwO1xyXG5cdH1cclxuXHQub2ZmbGluZXtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICM4ODg7XHJcblx0fVxyXG5cdC5zdGF0dXN7XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHR2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG5cdFx0bWluLXdpZHRoOiAxMHB4O1xyXG5cdFx0bWluLWhlaWdodDogMTBweDtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRcdG1hcmdpbi1yaWdodDogMTBweDtcclxuXHR9XHJcblx0LnVzZXItbmFtZXtcclxuXHRcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRcdHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcblx0fVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/user-item/user-item.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/user-item/user-item.component.ts ***!
  \*************************************************************/
/*! exports provided: UserItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserItemComponent", function() { return UserItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../models/user.model */ "./src/app/models/user.model.ts");



var UserItemComponent = /** @class */ (function () {
    function UserItemComponent() {
    }
    UserItemComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_user_model__WEBPACK_IMPORTED_MODULE_2__["User"])
    ], UserItemComponent.prototype, "user", void 0);
    UserItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-item',
            template: __webpack_require__(/*! ./user-item.component.html */ "./src/app/components/user-item/user-item.component.html"),
            styles: [__webpack_require__(/*! ./user-item.component.scss */ "./src/app/components/user-item/user-item.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserItemComponent);
    return UserItemComponent;
}());



/***/ }),

/***/ "./src/app/components/user-list/user-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/user-list/user-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"user-list\">\r\n\t<app-user-item [user]=user *ngFor=\"let user of users\"></app-user-item>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/user-list/user-list.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/user-list/user-list.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci1saXN0L3VzZXItbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/user-list/user-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/user-list/user-list.component.ts ***!
  \*************************************************************/
/*! exports provided: UserListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListComponent", function() { return UserListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/chat.service */ "./src/app/services/chat.service.ts");



var UserListComponent = /** @class */ (function () {
    function UserListComponent(chatService) {
        var _this = this;
        this.chatService = chatService;
        chatService.getUsers().valueChanges().subscribe(function (data) {
            _this.users = data;
        });
    }
    UserListComponent.prototype.ngOnInit = function () {
    };
    UserListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! ./user-list.component.html */ "./src/app/components/user-list/user-list.component.html"),
            styles: [__webpack_require__(/*! ./user-list.component.scss */ "./src/app/components/user-list/user-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_chat_service__WEBPACK_IMPORTED_MODULE_2__["ChatService"]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../services/auth.service */ "./src/app/services/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var currentUser = this.authService.getCurrentUserValue;
        if (currentUser)
            return true;
        console.log('Access denied! You are not signed in!');
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/guards/secure-inner-pages.guard.ts":
/*!****************************************************!*\
  !*** ./src/app/guards/secure-inner-pages.guard.ts ***!
  \****************************************************/
/*! exports provided: SecureInnerPagesGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecureInnerPagesGuard", function() { return SecureInnerPagesGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../services/auth.service */ "./src/app/services/auth.service.ts");




var SecureInnerPagesGuard = /** @class */ (function () {
    function SecureInnerPagesGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    SecureInnerPagesGuard.prototype.canActivate = function (next, state) {
        var currentUser = this.authService.getCurrentUserValue;
        if (!currentUser)
            return true;
        console.log('You are signed in!');
        this.router.navigate(['chat']);
        return false;
    };
    SecureInnerPagesGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SecureInnerPagesGuard);
    return SecureInnerPagesGuard;
}());



/***/ }),

/***/ "./src/app/models/chat-message.model.ts":
/*!**********************************************!*\
  !*** ./src/app/models/chat-message.model.ts ***!
  \**********************************************/
/*! exports provided: ChatMessage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatMessage", function() { return ChatMessage; });
var ChatMessage = /** @class */ (function () {
    function ChatMessage() {
        this.timeSent = new Date();
    }
    return ChatMessage;
}());



/***/ }),

/***/ "./src/app/models/user.model.ts":
/*!**************************************!*\
  !*** ./src/app/models/user.model.ts ***!
  \**************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");






var AuthService = /** @class */ (function () {
    function AuthService(afAuth, db, router) {
        this.afAuth = afAuth;
        this.db = db;
        this.router = router;
        this.currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    Object.defineProperty(AuthService.prototype, "getCurrentUserValue", {
        get: function () {
            return this.currentUserSubject.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "getCurrentUser", {
        get: function () {
            return this.currentUser;
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.signup = function (email, password, displayName) {
        var _this = this;
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
            .then(function (res) {
            if (res.user) {
                console.log('SIGNUP SUCCESS: ' + res.user.email);
                localStorage.setItem('currentUser', JSON.stringify(res.user));
                _this.currentUserSubject.next(res.user);
                _this.setUserData(res.user.uid, email, displayName, 'online');
                _this.router.navigate(['chat']);
            }
        });
    };
    AuthService.prototype.login = function (email, password) {
        var _this = this;
        return this.afAuth.auth.signInWithEmailAndPassword(email, password)
            .then(function (res) {
            if (res.user) {
                console.log('LOGIN SUCCESS: ' + res.user.email);
                localStorage.setItem('currentUser', JSON.stringify(res.user));
                _this.currentUserSubject.next(res.user);
                _this.updateUserStatus(res.user.uid, 'online');
                _this.router.navigate(['chat']);
            }
        });
    };
    AuthService.prototype.logout = function () {
        this.afAuth.auth.signOut();
        this.currentUserSubject.value ? this.updateUserStatus(this.currentUserSubject.value.uid, 'offline') : '';
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigate(['login']);
    };
    AuthService.prototype.setUserData = function (id, email, displayName, status) {
        var path = "users/" + id;
        var data = {
            email: email,
            displayName: displayName,
            status: status
        };
        this.db.object(path).update(data)
            .catch(function (error) { return console.log(error); });
    };
    AuthService.prototype.updateUserStatus = function (id, status) {
        var path = "users/" + id;
        var data = {
            status: status
        };
        this.db.object(path).update(data)
            .catch(function (error) { return console.log(error); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/chat.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/chat.service.ts ***!
  \******************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");




var ChatService = /** @class */ (function () {
    function ChatService(db, afAuth) {
        var _this = this;
        this.db = db;
        this.afAuth = afAuth;
        this.afAuth.authState.subscribe(function (user) {
            if (user) {
                _this.user = user;
            }
            _this.getUser().snapshotChanges().subscribe(function (data) {
                console.log('CURRENT USER: ' + JSON.stringify(data.payload.val()));
                var user = data.payload.val();
                _this.userName = user['displayName'];
            });
            ;
        });
    }
    ChatService.prototype.getUser = function () {
        console.log('CALL: getUser()');
        var userId = this.user.uid;
        var path = "/users/" + userId;
        return this.db.object(path);
    };
    ChatService.prototype.getUsers = function () {
        console.log('CALL: getUsers()');
        return this.db.list('/users');
    };
    ChatService.prototype.getMessages = function () {
        console.log('CALL: getMessages()');
        this.chatMessagesRef = this.db.list('messages', function (ref) { return ref.orderByKey().limitToLast(100); });
        return this.chatMessagesRef;
    };
    ChatService.prototype.sendMessage = function (msg) {
        console.log('CALL: sendMessage()');
        this.chatMessagesRef.push({
            email: this.user.email,
            userName: this.userName,
            message: msg,
            timeSent: this.getTimeStamp()
        });
    };
    ChatService.prototype.deteleMessage = function (key) {
        console.log('CALL: deteleMessage()');
        this.chatMessagesRef.remove(key);
    };
    ChatService.prototype.updateMessage = function (key, message) {
        console.log('CALL: updateMessage()');
        this.chatMessagesRef.update(key, { message: message });
    };
    ChatService.prototype.getTimeStamp = function () {
        var now = new Date();
        var date = now.getFullYear() + '/' +
            (now.getMonth() + 1) + '/' +
            now.getDate();
        var time = now.getHours() + ':' +
            now.getMinutes() + ':' +
            now.getSeconds();
        return (date + ' ' + time);
    };
    ChatService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCSCchVZttvQf4McGdbWSx2E8nK3yDWFH4",
        authDomain: "chat-demo-80085.firebaseapp.com",
        databaseURL: "https://chat-demo-80085.firebaseio.com",
        projectId: "chat-demo-80085",
        storageBucket: "chat-demo-80085.appspot.com",
        messagingSenderId: "48105687475"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\xampp55\htdocs\learn_angular\wesdoyle\lesson01\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map