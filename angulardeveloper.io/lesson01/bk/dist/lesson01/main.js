(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _customers_customers_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customers/customers.module */ "./src/app/customers/customers.module.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");





var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    {
        path: 'customers',
        loadChildren: function () { return _customers_customers_module__WEBPACK_IMPORTED_MODULE_3__["CustomersModule"]; }
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/store-devtools */ "./node_modules/@ngrx/store-devtools/fesm5/store-devtools.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["NavbarComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forRoot({}),
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["EffectsModule"].forRoot([]),
                _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__["StoreDevtoolsModule"].instrument({
                    maxAge: 25
                }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n\t<div class=\"container\">\n\t\t<div class=\"jumbotron\">\n\t\t\t<h1 class=\"display-4\">Welcome</h1>\n\t\t\t<p class=\"lead\">Please select your option below</p>\n\t\t\t<hr class=\"my-4\">\n\t\t\t<p class=\"lead\">\n\t\t\t\t<a class=\"btn btn-primary btn-lg\" [routerLink]=\"['customers']\" role=\"button\">Customers</a>\n\t\t\t</p>\n\t\t</div>\n\t</div>\n</section>\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary mb-5\">\r\n\t<div class=\"container\">\r\n\t\t<a class=\"navbar-brand\" [routerLink]=\"['/']\">NGRX</a>\r\n\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"\r\n\t\t\taria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n\t\t\t<span class=\"navbar-toggler-icon\"></span>\r\n\t\t</button>\r\n\t\t<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n\t\t\t<ul class=\"navbar-nav mr-auto\">\r\n\t\t\t\t<li class=\"nav-item active\">\r\n\t\t\t\t\t<a class=\"nav-link\" [routerLink]=\"['/customers']\">Customers</a>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t\t<ul class=\"navbar-nav ml-auto\">\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/components/navbar/navbar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/customers/actions/customer.actions.ts":
/*!*******************************************************!*\
  !*** ./src/app/customers/actions/customer.actions.ts ***!
  \*******************************************************/
/*! exports provided: CustomerActionTypes, LoadCustomers, LoadCustomersSuccess, LoadCustomersFail, LoadCustomer, LoadCustomerSuccess, LoadCustomerFail, CreateCustomer, CreateCustomerSuccess, CreateCustomerFail, UpdateCustomer, UpdateCustomerSuccess, UpdateCustomerFail, DeleteCustomer, DeleteCustomerSuccess, DeleteCustomerFail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerActionTypes", function() { return CustomerActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomers", function() { return LoadCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomersSuccess", function() { return LoadCustomersSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomersFail", function() { return LoadCustomersFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomer", function() { return LoadCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomerSuccess", function() { return LoadCustomerSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadCustomerFail", function() { return LoadCustomerFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCustomer", function() { return CreateCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCustomerSuccess", function() { return CreateCustomerSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCustomerFail", function() { return CreateCustomerFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCustomer", function() { return UpdateCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCustomerSuccess", function() { return UpdateCustomerSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCustomerFail", function() { return UpdateCustomerFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteCustomer", function() { return DeleteCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteCustomerSuccess", function() { return DeleteCustomerSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteCustomerFail", function() { return DeleteCustomerFail; });
var CustomerActionTypes;
(function (CustomerActionTypes) {
    CustomerActionTypes["LOAD_CUSTOMERS"] = "[Customer] Load Customers";
    CustomerActionTypes["LOAD_CUSTOMERS_SUCCESS"] = "[Customer] Load Customers Success";
    CustomerActionTypes["LOAD_CUSTOMERS_FAIL"] = "[Customer] Load Customers Fail";
    CustomerActionTypes["LOAD_CUSTOMER"] = "[Customer] Load Customer";
    CustomerActionTypes["LOAD_CUSTOMER_SUCCESS"] = "[Customer] Load Customer Success";
    CustomerActionTypes["LOAD_CUSTOMER_FAIL"] = "[Customer] Load Customer Fail";
    CustomerActionTypes["CREATE_CUSTOMER"] = "[Customer] Create Customer";
    CustomerActionTypes["CREATE_CUSTOMER_SUCCESS"] = "[Customer] Create Customer Success";
    CustomerActionTypes["CREATE_CUSTOMER_FAIL"] = "[Customer] Create Customer Fail";
    CustomerActionTypes["UPDATE_CUSTOMER"] = "[Customer] Update Customer";
    CustomerActionTypes["UPDATE_CUSTOMER_SUCCESS"] = "[Customer] Update Customer Success";
    CustomerActionTypes["UPDATE_CUSTOMER_FAIL"] = "[Customer] Update Customer Fail";
    CustomerActionTypes["DELETE_CUSTOMER"] = "[Customer] Delete Customer";
    CustomerActionTypes["DELETE_CUSTOMER_SUCCESS"] = "[Customer] Delete Customer Success";
    CustomerActionTypes["DELETE_CUSTOMER_FAIL"] = "[Customer] Delete Customer Fail";
})(CustomerActionTypes || (CustomerActionTypes = {}));
//Load All Customers
var LoadCustomers = /** @class */ (function () {
    function LoadCustomers() {
        this.type = CustomerActionTypes.LOAD_CUSTOMERS;
    }
    return LoadCustomers;
}());

var LoadCustomersSuccess = /** @class */ (function () {
    function LoadCustomersSuccess(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.LOAD_CUSTOMERS_SUCCESS;
    }
    return LoadCustomersSuccess;
}());

var LoadCustomersFail = /** @class */ (function () {
    function LoadCustomersFail(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.LOAD_CUSTOMERS_FAIL;
    }
    return LoadCustomersFail;
}());

//Load One Customer
var LoadCustomer = /** @class */ (function () {
    function LoadCustomer(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.LOAD_CUSTOMER;
    }
    return LoadCustomer;
}());

var LoadCustomerSuccess = /** @class */ (function () {
    function LoadCustomerSuccess(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.LOAD_CUSTOMER_SUCCESS;
    }
    return LoadCustomerSuccess;
}());

var LoadCustomerFail = /** @class */ (function () {
    function LoadCustomerFail(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.LOAD_CUSTOMER_FAIL;
    }
    return LoadCustomerFail;
}());

//Create One Customer
var CreateCustomer = /** @class */ (function () {
    function CreateCustomer(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.CREATE_CUSTOMER;
    }
    return CreateCustomer;
}());

var CreateCustomerSuccess = /** @class */ (function () {
    function CreateCustomerSuccess(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.CREATE_CUSTOMER_SUCCESS;
    }
    return CreateCustomerSuccess;
}());

var CreateCustomerFail = /** @class */ (function () {
    function CreateCustomerFail(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.CREATE_CUSTOMER_FAIL;
    }
    return CreateCustomerFail;
}());

//Update One Customer
var UpdateCustomer = /** @class */ (function () {
    function UpdateCustomer(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.UPDATE_CUSTOMER;
    }
    return UpdateCustomer;
}());

var UpdateCustomerSuccess = /** @class */ (function () {
    function UpdateCustomerSuccess(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.UPDATE_CUSTOMER_SUCCESS;
    }
    return UpdateCustomerSuccess;
}());

var UpdateCustomerFail = /** @class */ (function () {
    function UpdateCustomerFail(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.UPDATE_CUSTOMER_FAIL;
    }
    return UpdateCustomerFail;
}());

//Delete One Customer
var DeleteCustomer = /** @class */ (function () {
    function DeleteCustomer(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.DELETE_CUSTOMER;
    }
    return DeleteCustomer;
}());

var DeleteCustomerSuccess = /** @class */ (function () {
    function DeleteCustomerSuccess(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.DELETE_CUSTOMER_SUCCESS;
    }
    return DeleteCustomerSuccess;
}());

var DeleteCustomerFail = /** @class */ (function () {
    function DeleteCustomerFail(payload) {
        this.payload = payload;
        this.type = CustomerActionTypes.DELETE_CUSTOMER_FAIL;
    }
    return DeleteCustomerFail;
}());



/***/ }),

/***/ "./src/app/customers/components/customer-add/customer-add.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/customers/components/customer-add/customer-add.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVycy9jb21wb25lbnRzL2N1c3RvbWVyLWFkZC9jdXN0b21lci1hZGQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/customers/components/customer-add/customer-add.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/customers/components/customer-add/customer-add.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Add Customer</h3>\r\n\r\n<form class=\"form-inline mb-4\" [formGroup]=\"customerForm\" (submit)=\"createCustomer()\">\r\n\t<label class=\"sr-only\" for=\"Name\">Name</label>\r\n\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t<input\r\n\t\t\ttype=\"text\"\r\n\t\t\tclass=\"form-control\"\r\n\t\t\tplaceholder=\"Name\"\r\n\t\t\tformControlName=\"name\"\r\n\t\t>\r\n\t</div>\r\n\r\n\t<label class=\"sr-only\" for=\"Phone\">Phone</label>\r\n\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t<input\r\n\t\t\ttype=\"text\"\r\n\t\t\tclass=\"form-control\"\r\n\t\t\tplaceholder=\"Phone\"\r\n\t\t\tformControlName=\"phone\"\r\n\t\t>\r\n\t</div>\r\n\r\n\t<label class=\"sr-only\" for=\"Address\">Address</label>\r\n\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t<input\r\n\t\t\ttype=\"text\"\r\n\t\t\tclass=\"form-control\"\r\n\t\t\tplaceholder=\"Address\"\r\n\t\t\tformControlName=\"address\"\r\n\t\t>\r\n\t</div>\r\n\r\n\t<label class=\"sr-only\" for=\"membership\">Membership</label>\r\n\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t<select\r\n\t\t\tclass=\"form-control\"\r\n\t\t\tformControlName=\"membership\"\r\n\t\t>\r\n\t\t\t<option>Basic</option>\r\n\t\t\t<option>Pro</option>\r\n\t\t\t<option>Platinum</option>\r\n\t\t</select>\r\n\t</div>\r\n\t<button type=\"submit\" class=\"btn btn-primary mb-2\">Add Customer</button>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/customers/components/customer-add/customer-add.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/customers/components/customer-add/customer-add.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomerAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerAddComponent", function() { return CustomerAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_customer_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../actions/customer.actions */ "./src/app/customers/actions/customer.actions.ts");





var CustomerAddComponent = /** @class */ (function () {
    function CustomerAddComponent(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
    }
    CustomerAddComponent.prototype.ngOnInit = function () {
        this.customerForm = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            membership: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    CustomerAddComponent.prototype.createCustomer = function () {
        if (this.customerForm.valid) {
            var newCustomer = {
                name: this.customerForm.get('name').value,
                phone: this.customerForm.get('phone').value,
                address: this.customerForm.get('address').value,
                membership: this.customerForm.get('membership').value
            };
            this.store.dispatch(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_4__["CreateCustomer"](newCustomer));
            this.customerForm.reset();
        }
    };
    CustomerAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customer-add',
            template: __webpack_require__(/*! ./customer-add.component.html */ "./src/app/customers/components/customer-add/customer-add.component.html"),
            styles: [__webpack_require__(/*! ./customer-add.component.css */ "./src/app/customers/components/customer-add/customer-add.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], CustomerAddComponent);
    return CustomerAddComponent;
}());



/***/ }),

/***/ "./src/app/customers/components/customer-edit/customer-edit.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/customers/components/customer-edit/customer-edit.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVycy9jb21wb25lbnRzL2N1c3RvbWVyLWVkaXQvY3VzdG9tZXItZWRpdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/customers/components/customer-edit/customer-edit.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/customers/components/customer-edit/customer-edit.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mt-4\">\r\n\t<h3>Edit Customer</h3>\r\n\t<form [formGroup]=\"customerForm\" (submit)=\"updateCustomer()\" class=\"form-inline mb-4\">\r\n\t\t<label class=\"sr-only\" for=\"Name\">Name</label>\r\n\t\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\tplaceholder=\"Name\"\r\n\t\t\t\tformControlName=\"name\"\r\n\t\t\t>\r\n\t\t</div>\r\n\r\n\t\t<label class=\"sr-only\" for=\"Phone\">Phone</label>\r\n\t\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\tplaceholder=\"Phone\"\r\n\t\t\t\tformControlName=\"phone\"\r\n\t\t\t>\r\n\t\t</div>\r\n\r\n\t\t<label class=\"sr-only\" for=\"Address\">Address</label>\r\n\t\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t\t<input\r\n\t\t\t\ttype=\"text\"\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\tplaceholder=\"Address\"\r\n\t\t\t\tformControlName=\"address\"\r\n\t\t\t>\r\n\t\t</div>\r\n\r\n\t\t<label class=\"sr-only\" for=\"membership\">Membership</label>\r\n\t\t<div class=\"input-group mb-2 mr-sm-2\">\r\n\t\t\t<select\r\n\t\t\t\tclass=\"form-control\"\r\n\t\t\t\tformControlName=\"membership\"\r\n\t\t\t>\r\n\t\t\t\t<option>Basic</option>\r\n\t\t\t\t<option>Pro</option>\r\n\t\t\t\t<option>Platinum</option>\r\n\t\t\t</select>\r\n\t\t</div>\r\n\r\n\t\t<button type=\"submit\" class=\"btn btn-primary mb-2\">Update Customer</button>\r\n\t</form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/customers/components/customer-edit/customer-edit.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/customers/components/customer-edit/customer-edit.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CustomerEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerEditComponent", function() { return CustomerEditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_customer_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../actions/customer.actions */ "./src/app/customers/actions/customer.actions.ts");
/* harmony import */ var _reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../reducers/customer.reducer */ "./src/app/customers/reducers/customer.reducer.ts");






var CustomerEditComponent = /** @class */ (function () {
    function CustomerEditComponent(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
    }
    CustomerEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.customerForm = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            membership: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            id: null
        });
        this.customer$ = this.store.select(_reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_5__["getCurrentCustomer"]);
        this.customer$.subscribe(function (currentCustomer) {
            if (currentCustomer) {
                _this.customerForm.patchValue({
                    name: currentCustomer.name,
                    phone: currentCustomer.phone,
                    address: currentCustomer.address,
                    membership: currentCustomer.membership,
                    id: currentCustomer.id
                });
            }
        });
    };
    CustomerEditComponent.prototype.updateCustomer = function () {
        if (this.customerForm.valid) {
            var updatedCustomer = {
                name: this.customerForm.get('name').value,
                phone: this.customerForm.get('phone').value,
                address: this.customerForm.get('address').value,
                membership: this.customerForm.get('membership').value,
                id: this.customerForm.get('id').value
            };
            this.store.dispatch(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_4__["UpdateCustomer"](updatedCustomer));
        }
    };
    CustomerEditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customer-edit',
            template: __webpack_require__(/*! ./customer-edit.component.html */ "./src/app/customers/components/customer-edit/customer-edit.component.html"),
            styles: [__webpack_require__(/*! ./customer-edit.component.css */ "./src/app/customers/components/customer-edit/customer-edit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], CustomerEditComponent);
    return CustomerEditComponent;
}());



/***/ }),

/***/ "./src/app/customers/components/customer-list/customer-list.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/customers/components/customer-list/customer-list.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVycy9jb21wb25lbnRzL2N1c3RvbWVyLWxpc3QvY3VzdG9tZXItbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/customers/components/customer-list/customer-list.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/customers/components/customer-list/customer-list.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Customers</h3>\r\n<table class=\"table table-hover\">\r\n\t<thead>\r\n\t\t<tr class=\"table-primary\">\r\n\t\t\t<th scope=\"col\">Name</th>\r\n\t\t\t<th scope=\"col\">Phone</th>\r\n\t\t\t<th scope=\"col\">Address</th>\r\n\t\t\t<th scope=\"col\">Membership</th>\r\n\t\t\t<th></th>\r\n\t\t</tr>\r\n\t</thead>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td colspan=\"5\" class=\"alert alert-danger\" *ngIf=\"error$ | async as error\">{{ error.message }}</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t<tr *ngFor=\"let customer of (customers$ |async)\">\r\n\t\t\t<th scope=\"row\">{{ customer.name }}</th>\r\n\t\t\t<td>{{ customer.phone }}</td>\r\n\t\t\t<td>{{ customer.address }}</td>\r\n\t\t\t<td>{{ customer.membership }}</td>\r\n\t\t\t<th>\r\n\t\t\t\t<button class=\"btn btn-sm btn-info\" (click)=\"editCustomer(customer)\">Edit</button>\r\n\t\t\t\t&nbsp;\r\n\t\t\t\t<button class=\"btn btn-sm btn-danger\" (click)=\"deleteCustomer(customer)\">Delete</button>\r\n\t\t\t</th>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/customers/components/customer-list/customer-list.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/customers/components/customer-list/customer-list.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CustomerListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerListComponent", function() { return CustomerListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../actions/customer.actions */ "./src/app/customers/actions/customer.actions.ts");
/* harmony import */ var _reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../reducers/customer.reducer */ "./src/app/customers/reducers/customer.reducer.ts");





var CustomerListComponent = /** @class */ (function () {
    function CustomerListComponent(store) {
        this.store = store;
    }
    CustomerListComponent.prototype.ngOnInit = function () {
        this.store.dispatch(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["LoadCustomers"]());
        this.customers$ = this.store.select(_reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_4__["getCustomers"]);
        this.error$ = this.store.select(_reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_4__["getError"]);
        this.store.subscribe(function (state) {
            console.log(state);
        });
    };
    CustomerListComponent.prototype.deleteCustomer = function (customer) {
        if (confirm('Are You Sure You want to Delete the User?')) {
            this.store.dispatch(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["DeleteCustomer"](customer.id));
        }
    };
    CustomerListComponent.prototype.editCustomer = function (customer) {
        this.store.dispatch(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["LoadCustomer"](customer.id));
    };
    CustomerListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customer-list',
            template: __webpack_require__(/*! ./customer-list.component.html */ "./src/app/customers/components/customer-list/customer-list.component.html"),
            styles: [__webpack_require__(/*! ./customer-list.component.css */ "./src/app/customers/components/customer-list/customer-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], CustomerListComponent);
    return CustomerListComponent;
}());



/***/ }),

/***/ "./src/app/customers/components/customer/customer.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/customers/components/customer/customer.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVycy9jb21wb25lbnRzL2N1c3RvbWVyL2N1c3RvbWVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/customers/components/customer/customer.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/customers/components/customer/customer.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n\t<div class=\"container\">\n\t\t<app-customer-add></app-customer-add>\n\t\t<app-customer-list></app-customer-list>\n\t\t<app-customer-edit></app-customer-edit>\n\t</div>\n</section>\n"

/***/ }),

/***/ "./src/app/customers/components/customer/customer.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/customers/components/customer/customer.component.ts ***!
  \*********************************************************************/
/*! exports provided: CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CustomerComponent = /** @class */ (function () {
    function CustomerComponent() {
    }
    CustomerComponent.prototype.ngOnInit = function () {
    };
    CustomerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customer',
            template: __webpack_require__(/*! ./customer.component.html */ "./src/app/customers/components/customer/customer.component.html"),
            styles: [__webpack_require__(/*! ./customer.component.css */ "./src/app/customers/components/customer/customer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CustomerComponent);
    return CustomerComponent;
}());



/***/ }),

/***/ "./src/app/customers/customers-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/customers/customers-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: CustomersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersRoutingModule", function() { return CustomersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_customer_customer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/customer/customer.component */ "./src/app/customers/components/customer/customer.component.ts");




var customersRoutes = [
    { path: '', component: _components_customer_customer_component__WEBPACK_IMPORTED_MODULE_3__["CustomerComponent"] }
];
var CustomersRoutingModule = /** @class */ (function () {
    function CustomersRoutingModule() {
    }
    CustomersRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(customersRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CustomersRoutingModule);
    return CustomersRoutingModule;
}());



/***/ }),

/***/ "./src/app/customers/customers.module.ts":
/*!***********************************************!*\
  !*** ./src/app/customers/customers.module.ts ***!
  \***********************************************/
/*! exports provided: CustomersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersModule", function() { return CustomersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reducers/customer.reducer */ "./src/app/customers/reducers/customer.reducer.ts");
/* harmony import */ var _effects_customer_effects__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./effects/customer.effects */ "./src/app/customers/effects/customer.effects.ts");
/* harmony import */ var _components_customer_add_customer_add_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/customer-add/customer-add.component */ "./src/app/customers/components/customer-add/customer-add.component.ts");
/* harmony import */ var _components_customer_edit_customer_edit_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/customer-edit/customer-edit.component */ "./src/app/customers/components/customer-edit/customer-edit.component.ts");
/* harmony import */ var _components_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/customer-list/customer-list.component */ "./src/app/customers/components/customer-list/customer-list.component.ts");
/* harmony import */ var _components_customer_customer_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/customer/customer.component */ "./src/app/customers/components/customer/customer.component.ts");
/* harmony import */ var _customers_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./customers-routing.module */ "./src/app/customers/customers-routing.module.ts");













var CustomersModule = /** @class */ (function () {
    function CustomersModule() {
    }
    CustomersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _components_customer_add_customer_add_component__WEBPACK_IMPORTED_MODULE_8__["CustomerAddComponent"],
                _components_customer_edit_customer_edit_component__WEBPACK_IMPORTED_MODULE_9__["CustomerEditComponent"],
                _components_customer_list_customer_list_component__WEBPACK_IMPORTED_MODULE_10__["CustomerListComponent"],
                _components_customer_customer_component__WEBPACK_IMPORTED_MODULE_11__["CustomerComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forFeature('customers', _reducers_customer_reducer__WEBPACK_IMPORTED_MODULE_6__["customerReducer"]),
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["EffectsModule"].forFeature([_effects_customer_effects__WEBPACK_IMPORTED_MODULE_7__["CustomerEffect"]]),
                _customers_routing_module__WEBPACK_IMPORTED_MODULE_12__["CustomersRoutingModule"]
            ]
        })
    ], CustomersModule);
    return CustomersModule;
}());



/***/ }),

/***/ "./src/app/customers/effects/customer.effects.ts":
/*!*******************************************************!*\
  !*** ./src/app/customers/effects/customer.effects.ts ***!
  \*******************************************************/
/*! exports provided: CustomerEffect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerEffect", function() { return CustomerEffect; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../services/customer.service */ "./src/app/customers/services/customer.service.ts");
/* harmony import */ var _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../actions/customer.actions */ "./src/app/customers/actions/customer.actions.ts");







var CustomerEffect = /** @class */ (function () {
    function CustomerEffect(actions$, customerService) {
        var _this = this;
        this.actions$ = actions$;
        this.customerService = customerService;
        this.loadCustomers$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CustomerActionTypes"].LOAD_CUSTOMERS), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (action) {
            return _this.customerService.getCustomers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (customers) {
                return new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["LoadCustomersSuccess"](customers);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
                console.log(err);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["LoadCustomersFail"](err));
            }));
        }));
        this.loadCustomer$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CustomerActionTypes"].LOAD_CUSTOMER), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (action) {
            return _this.customerService.getCustomerById(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (customer) {
                return new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["LoadCustomerSuccess"](customer);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
                console.log(err);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["LoadCustomerFail"](err));
            }));
        }));
        this.createCustomer$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CustomerActionTypes"].CREATE_CUSTOMER), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (action) {
            console.log(action);
            return action.payload;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (customer) {
            return _this.customerService.createCustomer(customer).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (newCustomer) {
                console.log(newCustomer);
                return new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CreateCustomerSuccess"](newCustomer);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
                console.log(err);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CreateCustomerFail"](err));
            }));
        }));
        this.updateCustomer$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CustomerActionTypes"].UPDATE_CUSTOMER), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (action) {
            console.log(action);
            return action.payload;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (customer) {
            return _this.customerService.updateCustomer(customer).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (updateCustomer) {
                return new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["UpdateCustomerSuccess"]({
                    id: updateCustomer.id,
                    changes: updateCustomer
                });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
                console.log(err);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["UpdateCustomerFail"](err));
            }));
        }));
        this.deleteCustomer$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["CustomerActionTypes"].DELETE_CUSTOMER), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (action) {
            console.log(action);
            return action.payload;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (id) {
            return _this.customerService.deleteCustomer(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function () {
                return new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["DeleteCustomerSuccess"](id);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
                console.log(err);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(new _actions_customer_actions__WEBPACK_IMPORTED_MODULE_6__["DeleteCustomerFail"](err));
            }));
        }));
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], CustomerEffect.prototype, "loadCustomers$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], CustomerEffect.prototype, "loadCustomer$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], CustomerEffect.prototype, "createCustomer$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], CustomerEffect.prototype, "updateCustomer$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], CustomerEffect.prototype, "deleteCustomer$", void 0);
    CustomerEffect = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Actions"],
            _services_customer_service__WEBPACK_IMPORTED_MODULE_5__["CustomerService"]])
    ], CustomerEffect);
    return CustomerEffect;
}());



/***/ }),

/***/ "./src/app/customers/reducers/customer.reducer.ts":
/*!********************************************************!*\
  !*** ./src/app/customers/reducers/customer.reducer.ts ***!
  \********************************************************/
/*! exports provided: customerAdapter, initialState, customerReducer, getCustomers, getCustomersLoading, getCustomersLoaded, getError, getCurrentCustomerId, getCurrentCustomer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customerAdapter", function() { return customerAdapter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customerReducer", function() { return customerReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomers", function() { return getCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomersLoading", function() { return getCustomersLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomersLoaded", function() { return getCustomersLoaded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getError", function() { return getError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrentCustomerId", function() { return getCurrentCustomerId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrentCustomer", function() { return getCurrentCustomer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _ngrx_entity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/entity */ "./node_modules/@ngrx/entity/fesm5/entity.js");
/* harmony import */ var _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../actions/customer.actions */ "./src/app/customers/actions/customer.actions.ts");




var customerAdapter = Object(_ngrx_entity__WEBPACK_IMPORTED_MODULE_2__["createEntityAdapter"])();
var initialState = customerAdapter.getInitialState({
    selectedCustomerId: null,
    loading: false,
    loaded: false,
    error: null
});
function customerReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        //Load All Customers
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].LOAD_CUSTOMERS_SUCCESS:
            console.log(action, state);
            return customerAdapter.addAll(action.payload, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { loading: false, loaded: true, error: null }));
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].LOAD_CUSTOMERS_FAIL:
            console.log(action, state);
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { loading: false, loaded: false, error: action.payload });
        //Load One Customer
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].LOAD_CUSTOMER_SUCCESS: {
            console.log(action, state);
            return customerAdapter.addOne(action.payload, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { selectedCustomerId: action.payload.id }));
        }
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].LOAD_CUSTOMER_FAIL: {
            console.log(action, state);
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { error: action.payload });
        }
        //Create One Customer
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].CREATE_CUSTOMER_SUCCESS: {
            console.log(action, state);
            return customerAdapter.addOne(action.payload, state);
        }
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].CREATE_CUSTOMER_FAIL: {
            console.log(action, state);
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { error: action.payload });
        }
        //Update One Customer
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].UPDATE_CUSTOMER_SUCCESS: {
            console.log(action, state);
            return customerAdapter.updateOne(action.payload, state);
        }
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].UPDATE_CUSTOMER_FAIL: {
            console.log(action, state);
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { error: action.payload });
        }
        //Delete One Customer
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].DELETE_CUSTOMER_SUCCESS: {
            console.log(action, state);
            return customerAdapter.removeOne(action.payload, state);
        }
        case _actions_customer_actions__WEBPACK_IMPORTED_MODULE_3__["CustomerActionTypes"].DELETE_CUSTOMER_FAIL: {
            console.log(action, state);
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { error: action.payload });
        }
        default:
            return state;
    }
}
var getCustomerFeatureState = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createFeatureSelector"])('customers');
var getCustomers = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, customerAdapter.getSelectors().selectAll);
var getCustomersLoading = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, function (state) {
    // console.log(state);
    return state.loading;
});
var getCustomersLoaded = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, function (state) {
    // console.log(state);
    return state.loaded;
});
var getError = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, function (state) {
    // console.log(state);
    return state.error;
});
var getCurrentCustomerId = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, function (state) {
    // console.log(state);
    return state.selectedCustomerId;
});
var getCurrentCustomer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createSelector"])(getCustomerFeatureState, getCurrentCustomerId, function (state) {
    console.log(state);
    return state.entities[state.selectedCustomerId];
});


/***/ }),

/***/ "./src/app/customers/services/customer.service.ts":
/*!********************************************************!*\
  !*** ./src/app/customers/services/customer.service.ts ***!
  \********************************************************/
/*! exports provided: CustomerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerService", function() { return CustomerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CustomerService = /** @class */ (function () {
    function CustomerService(http) {
        this.http = http;
        this.API_URL = 'http://localhost:4000/customers';
    }
    CustomerService.prototype.getCustomers = function () {
        return this.http.get(this.API_URL);
    };
    CustomerService.prototype.getCustomerById = function (payload) {
        return this.http.get(this.API_URL + "/" + payload);
    };
    CustomerService.prototype.createCustomer = function (payload) {
        return this.http.post(this.API_URL, payload);
    };
    CustomerService.prototype.updateCustomer = function (payload) {
        return this.http.patch(this.API_URL + "/" + payload.id, payload);
    };
    CustomerService.prototype.deleteCustomer = function (payload) {
        return this.http.delete(this.API_URL + "/" + payload);
    };
    CustomerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CustomerService);
    return CustomerService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\learn_angular\angulardeveloper.io\lesson01\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map