import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersModule } from "./customers/customers.module";
import { HomeComponent } from "./components/home/home.component";

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{
		path: 'customers',
		loadChildren: () => CustomersModule
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
