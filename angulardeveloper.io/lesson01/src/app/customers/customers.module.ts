import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { customerReducer } from './reducers/customer.reducer';
import { CustomerEffect } from './effects/customer.effects';

import { CustomerAddComponent } from './components/customer-add/customer-add.component';
import { CustomerEditComponent } from './components/customer-edit/customer-edit.component';
import { CustomerListComponent } from './components/customer-list/customer-list.component';
import { CustomerComponent } from './components/customer/customer.component';

import { CustomersRoutingModule } from './customers-routing.module';

@NgModule({
	declarations: [
		CustomerAddComponent,
		CustomerEditComponent,
		CustomerListComponent,
		CustomerComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		StoreModule.forFeature('customers', customerReducer),
		EffectsModule.forFeature([CustomerEffect]),
		CustomersRoutingModule
	]
})
export class CustomersModule { }
