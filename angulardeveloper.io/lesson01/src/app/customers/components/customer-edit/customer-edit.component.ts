import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from "@ngrx/store";
import { Observable } from 'rxjs';
import { Customer } from './../../models/customers.model';
import * as customerActions from './../../actions/customer.actions';
import * as fromCustomer from "./../../reducers/customer.reducer";

@Component({
	selector: 'app-customer-edit',
	templateUrl: './customer-edit.component.html',
	styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

	customerForm: FormGroup;
	customer$: Observable<Customer>;

	constructor(
		private formBuilder: FormBuilder,
		private store: Store<fromCustomer.AppState>
	) { }

	ngOnInit() {
		this.customerForm = this.formBuilder.group({
			name: ['', Validators.required],
			phone: ['', Validators.required],
			address: ['', Validators.required],
			membership: ['', Validators.required],
			id: null
		})

		this.customer$ = this.store.select(fromCustomer.getCurrentCustomer);

		this.customer$.subscribe(currentCustomer => {
			if (currentCustomer) {
				this.customerForm.patchValue({
					name: currentCustomer.name,
					phone: currentCustomer.phone,
					address: currentCustomer.address,
					membership: currentCustomer.membership,
					id: currentCustomer.id
				});
			}
		});
	}

	updateCustomer(): void {
		if (this.customerForm.valid) {
			const updatedCustomer: Customer = {
				name: this.customerForm.get('name').value,
				phone: this.customerForm.get('phone').value,
				address: this.customerForm.get('address').value,
				membership: this.customerForm.get('membership').value,
				id: this.customerForm.get('id').value
			};

			this.store.dispatch(new customerActions.UpdateCustomer(updatedCustomer))
		}
	}

}
