import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Customer } from './../../models/customers.model';
import * as customerActions from './../../actions/customer.actions';
import * as fromCustomer from "./../../reducers/customer.reducer";

@Component({
	selector: 'app-customer-list',
	templateUrl: './customer-list.component.html',
	styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

	customers$: Observable<Customer[]>;
	error$: Observable<any>;

	constructor(
		private store: Store<fromCustomer.AppState>
	) {}

	ngOnInit() {
		this.store.dispatch(new customerActions.LoadCustomers());
		this.customers$ = this.store.select(fromCustomer.getCustomers);
		this.error$ = this.store.select(fromCustomer.getError);

		this.store.subscribe(state => {
			console.log(state);
		});
	}

	deleteCustomer(customer: Customer): void {
		if (confirm('Are You Sure You want to Delete the User?')) {
			this.store.dispatch(new customerActions.DeleteCustomer(customer.id));
		}
	}

	editCustomer(customer: Customer): void {
		this.store.dispatch(new customerActions.LoadCustomer(customer.id));
	}

}
