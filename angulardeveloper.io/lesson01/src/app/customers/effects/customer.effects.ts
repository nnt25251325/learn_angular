import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { Customer } from './../models/customers.model';
import { CustomerService } from './../services/customer.service';
import * as customerActions from './../actions/customer.actions';

@Injectable()
export class CustomerEffect  {

	constructor(
		private actions$: Actions,
		private customerService: CustomerService
	) {}

	@Effect()
	loadCustomers$: Observable<Action> = this.actions$.pipe(
		ofType<customerActions.LoadCustomers>(
			customerActions.CustomerActionTypes.LOAD_CUSTOMERS
		),
		mergeMap((action: customerActions.LoadCustomers) => {
			return this.customerService.getCustomers().pipe(
				map((customers: Customer[]) => {
					return new customerActions.LoadCustomersSuccess(customers);
				}),
				catchError(err => {
					console.log(err);
					return of(new customerActions.LoadCustomersFail(err));
				})
			);
		})
	);

	@Effect()
	loadCustomer$: Observable<Action> = this.actions$.pipe(
		ofType<customerActions.LoadCustomer>(
			customerActions.CustomerActionTypes.LOAD_CUSTOMER
		),
		mergeMap((action: customerActions.LoadCustomer) => {
			return this.customerService.getCustomerById(action.payload).pipe(
				map((customer: Customer) => {
					return new customerActions.LoadCustomerSuccess(customer);
				}),
				catchError(err => {
					console.log(err);
					return of(new customerActions.LoadCustomerFail(err));
				})
			);
		})
	);

	@Effect()
	createCustomer$: Observable<Action> = this.actions$.pipe(
		ofType<customerActions.CreateCustomer>(
			customerActions.CustomerActionTypes.CREATE_CUSTOMER
		),
		map((action: customerActions.CreateCustomer) => {
			console.log(action);
			return action.payload;
		}),
		mergeMap((customer: Customer) => {
			return this.customerService.createCustomer(customer).pipe(
				map((newCustomer: Customer) => {
					console.log(newCustomer);
					return new customerActions.CreateCustomerSuccess(newCustomer);
				}),
				catchError(err => {
					console.log(err);
					return of(new customerActions.CreateCustomerFail(err));
				})
			);
		})
	);

	@Effect()
	updateCustomer$: Observable<Action> = this.actions$.pipe(
		ofType<customerActions.UpdateCustomer>(
			customerActions.CustomerActionTypes.UPDATE_CUSTOMER
		),
		map((action: customerActions.UpdateCustomer) => {
			console.log(action);
			return action.payload;
		}),
		mergeMap((customer: Customer) => {
			return this.customerService.updateCustomer(customer).pipe(
				map((updateCustomer: Customer) => {
					return new customerActions.UpdateCustomerSuccess({
						id: updateCustomer.id,
						changes: updateCustomer
					});
				}),
				catchError(err => {
					console.log(err);
					return of(new customerActions.UpdateCustomerFail(err));
				})
			);
		})
	);

	@Effect()
	deleteCustomer$: Observable<Action> = this.actions$.pipe(
		ofType<customerActions.DeleteCustomer>(
			customerActions.CustomerActionTypes.DELETE_CUSTOMER
		),
		map((action: customerActions.DeleteCustomer) => {
			console.log(action);
			return action.payload;
		}),
		mergeMap((id: number) => {
			return this.customerService.deleteCustomer(id).pipe(
				map(() => {
					return new customerActions.DeleteCustomerSuccess(id);
				}),
				catchError(err => {
					console.log(err);
					return of(new customerActions.DeleteCustomerFail(err));
				})
			);
		})
	);

}
