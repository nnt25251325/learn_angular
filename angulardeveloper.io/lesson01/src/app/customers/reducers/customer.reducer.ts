import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Customer } from './../models/customers.model';
import * as customerActions from './../actions/customer.actions';
import * as fromRoot from './../../state/app.state';

export interface CustomerState extends EntityState<Customer> {
	selectedCustomerId: number | null,
	loading: boolean,
	loaded: boolean,
	error: any
}

export interface AppState extends fromRoot.AppState {
	customers: CustomerState
}

export const customerAdapter: EntityAdapter<Customer> = createEntityAdapter<Customer>();

export const initialState = customerAdapter.getInitialState({
	selectedCustomerId: null,
	loading: false,
	loaded: false,
	error: null
});

export function customerReducer(state = initialState, action: customerActions.Action): CustomerState {
	switch (action.type) {
		//Load All Customers
		case customerActions.CustomerActionTypes.LOAD_CUSTOMERS_SUCCESS:
			console.log(action, state);
			return customerAdapter.addAll(action.payload, {
				...state,
				loading: false,
				loaded: true,
				error: null
			});

		case customerActions.CustomerActionTypes.LOAD_CUSTOMERS_FAIL:
			console.log(action, state);
			return {
				...state,
				loading: false,
				loaded: false,
				error: action.payload
			};

		//Load One Customer
		case customerActions.CustomerActionTypes.LOAD_CUSTOMER_SUCCESS: {
			console.log(action, state);
			return customerAdapter.addOne(action.payload, {
				...state,
				selectedCustomerId: action.payload.id
			});
		}

		case customerActions.CustomerActionTypes.LOAD_CUSTOMER_FAIL: {
			console.log(action, state);
			return {
				...state,
				error: action.payload
			};
		}

		//Create One Customer
		case customerActions.CustomerActionTypes.CREATE_CUSTOMER_SUCCESS: {
			console.log(action, state);
			return customerAdapter.addOne(action.payload, state);
		}

		case customerActions.CustomerActionTypes.CREATE_CUSTOMER_FAIL: {
			console.log(action, state);
			return {
				...state,
				error: action.payload
			};
		}

		//Update One Customer
		case customerActions.CustomerActionTypes.UPDATE_CUSTOMER_SUCCESS: {
			console.log(action, state);
			return customerAdapter.updateOne(action.payload, state);
		}

		case customerActions.CustomerActionTypes.UPDATE_CUSTOMER_FAIL: {
			console.log(action, state);
			return {
				...state,
				error: action.payload
			};
		}

		//Delete One Customer
		case customerActions.CustomerActionTypes.DELETE_CUSTOMER_SUCCESS: {
			console.log(action, state);
			return customerAdapter.removeOne(action.payload, state);
		}

		case customerActions.CustomerActionTypes.DELETE_CUSTOMER_FAIL: {
			console.log(action, state);
			return {
				...state,
				error: action.payload
			};
		}

		default:
			return state;
	}
}

const getCustomerFeatureState = createFeatureSelector<CustomerState>('customers');

export const getCustomers = createSelector(
	getCustomerFeatureState,
	customerAdapter.getSelectors().selectAll
);

export const getCustomersLoading = createSelector(
	getCustomerFeatureState,
	(state: CustomerState) => {
		// console.log(state);
		return state.loading;
	}
);

export const getCustomersLoaded = createSelector(
	getCustomerFeatureState,
	(state: CustomerState) => {
		// console.log(state);
		return state.loaded;
	}
);

export const getError = createSelector(
	getCustomerFeatureState,
	(state: CustomerState) => {
		// console.log(state);
		return state.error;
	}
);

export const getCurrentCustomerId = createSelector(
	getCustomerFeatureState,
	(state: CustomerState) => {
		// console.log(state);
		return state.selectedCustomerId;
	}
);

export const getCurrentCustomer = createSelector(
	getCustomerFeatureState,
	getCurrentCustomerId,
	state => {
		console.log(state);
		return state.entities[state.selectedCustomerId];
	}
);
