import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './../models/customers.model';

@Injectable({
	providedIn: 'root'
})
export class CustomerService {

	private API_URL: string = 'http://localhost:4000/customers';

	constructor(
		private http: HttpClient
	) { }

	getCustomers(): Observable<Customer[]> {
		return this.http.get<Customer[]>(this.API_URL);
	}

	getCustomerById(payload: number): Observable<Customer> {
		return this.http.get<Customer>(`${this.API_URL}/${payload}`);
	}

	createCustomer(payload: Customer): Observable<Customer> {
		return this.http.post<Customer>(this.API_URL, payload);
	}

	updateCustomer(payload: Customer): Observable<Customer> {
		return this.http.patch<Customer>(`${this.API_URL}/${payload.id}`, payload);
	}

	deleteCustomer(payload: Number): Observable<any> {
		return this.http.delete(`${this.API_URL}/${payload}`);
	}

}
